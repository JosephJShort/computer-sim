/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs;

import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.control.InputState;
import jjs.cs.control.SimulationController;
import jjs.cs.joglview.JoglSimulationView;
import jjs.cs.simulation.SimulationModel;
import jjs.cs.view.SimulationView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class ComputerSimApplication {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(ComputerSimApplication.class);

    private static enum State {

        NEW,
        IDLE,
        SIMULATION_SUSPENDED,
        SIMULATION_RUNNING,
        DISPOSED
    }

    private final AtomicReference<State> state;

    // Resources.
    private final SimulationController simulationController;
    private final SimulationView simulationView;

    // For SIMULATION_SUSPENDED and SIMULATION_RUNNING.
    private SimulationModel simulationModel;
    private InputState inputState;

    private ComputerSimApplication() {
        this.simulationController = new SimulationController();
        this.simulationView = new JoglSimulationView(this);
        this.state = new AtomicReference<>(State.NEW);
    }

    public synchronized void setUp() {

        // Validate state.
        assertState(
                this.state.get(), "setUp() called more than once.", State.NEW);

        // Set up resources.
        this.simulationView.setUp();
        this.simulationController.setUp();

        // Set state to IDLE.
        this.state.set(State.IDLE);
        LOGGER.info("Computer Sim  has been set up and is idle.");
    }

    public synchronized void loadSimulation(
            final SimulationModel simulationModel) {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp(), before tearDown().",
                State.IDLE,
                State.SIMULATION_SUSPENDED,
                State.SIMULATION_RUNNING);

        // Validate parameters.
        SimulationModel simulationModel_dc = simulationModel;
        if (simulationModel_dc == null) {
            throw new NullPointerException(
                    "simulationModel: " + simulationModel_dc);
        }

        // Unload old simulation if present.
        if (stateRef == State.SIMULATION_SUSPENDED
                || stateRef == State.SIMULATION_RUNNING) {
            unloadSimulation();
        }

        // Load given simulation.
        this.simulationModel = simulationModel_dc;
        this.inputState = new InputState();
        this.simulationView.loadModel(simulationModel, inputState);
        this.simulationController.loadSimulation(
                simulationModel, simulationView, inputState);

        // Set state to SIMULATION_SUSPENDED.
        this.state.set(State.SIMULATION_SUSPENDED);
        LOGGER.info("Computer Sim has loaded a model and is suspended. Model: "
                + this.simulationModel + ".");
    }

    public synchronized void unloadSimulation() {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp()"
                + " and loadSimulation(...), before tearDown().",
                State.SIMULATION_SUSPENDED,
                State.SIMULATION_RUNNING);

        if (stateRef == State.SIMULATION_RUNNING) {
            suspendSimulation();
        }

        // Unload.
        this.simulationController.unloadSimulation();
        this.simulationView.unloadModel();
        this.simulationModel = null;
        this.inputState = null;

        // Set state to IDLE.
        this.state.set(State.IDLE);
        LOGGER.info("Computer Sim has unloaded a model and is idle.");
    }

    public synchronized void resumeSimulation() {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp() and loadSimulation(...), before"
                + " tearDown(). Cannot be called again until"
                + " suspendSimulation() is called.",
                State.SIMULATION_SUSPENDED);

        // Resume.
        this.simulationController.resumeSimulation();

        // Set state to SIMULATION_RUNNING.
        this.state.set(State.SIMULATION_RUNNING);
        LOGGER.info("Computer Sim has resumed simulation.");
    }

    public synchronized void suspendSimulation() {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp(), loadSimulation(...) and"
                + " resumeSimulation(), before tearDown(). Cannot be called"
                + " again until suspendRunning() is called.",
                State.SIMULATION_RUNNING);

        // Suspend.
        this.simulationController.suspendSimulation();

        // Set state to SIMULATION_SUSPENDED.
        this.state.set(State.SIMULATION_SUSPENDED);
        LOGGER.info("Computer Sim has suspended simulation.");
    }

    public synchronized void tearDown() {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp()."
                + " Cannot be called more than once.",
                State.IDLE,
                State.SIMULATION_SUSPENDED,
                State.SIMULATION_RUNNING);

        // Unload simulation if present.
        if (stateRef == State.SIMULATION_SUSPENDED
                || stateRef == State.SIMULATION_RUNNING) {
            this.unloadSimulation();
        }

        // Release resources.
        this.simulationController.tearDown();
        this.simulationView.tearDown();

        // Set state to DISPOSED.
        this.state.set(State.DISPOSED);
        LOGGER.info("Computer Sim has been torn down.");

        System.exit(0);
    }

    private static void assertState(
            final State ref,
            final String exceptionMessage,
            final State... states) {
        boolean hasCorrectState = false;
        int i = 0;
        while (!hasCorrectState && i < states.length) {
            if (states[i] == ref) {
                hasCorrectState = true;
            }
            i++;
        }
        if (!hasCorrectState) {
            throw new IllegalStateException(exceptionMessage);
        }
    }

    // Instance control.
    public static ComputerSimApplication getInstance() {
        return SingletonEnforcement.SINGLETON.instance;
    }

    private static enum SingletonEnforcement {

        SINGLETON;

        private final ComputerSimApplication instance;

        private SingletonEnforcement() {
            instance = new ComputerSimApplication();
        }

    }
}
