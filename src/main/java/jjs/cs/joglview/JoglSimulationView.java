/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview;

import java.awt.EventQueue;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.ComputerSimApplication;
import jjs.cs.control.InputState;
import jjs.cs.joglview.swing.GuiVisualisation;
import jjs.cs.joglview.swing.SwingGui;
import jjs.cs.simulation.SimulationModel;
import jjs.cs.view.SimulationView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class JoglSimulationView implements SimulationView {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(JoglSimulationView.class);

    private static enum State {

        NEW,
        NO_MODEL,
        HAS_MODEL,
        DISPOSED
    }

    private final AtomicReference<State> state;
    private final ComputerSimApplication application;
    private final SwingGui swingGui;
    private final SwingGuiWindowListener windowListerner;

    // For HAS_MODEL.
    private GuiVisualisation guiVisualisation;

    public JoglSimulationView(ComputerSimApplication application) {
        this.application = application;
        if (application == null) {
            throw new NullPointerException("application: " + application);
        }
        this.swingGui = new SwingGui(this.application);
        this.windowListerner = new SwingGuiWindowListener();
        this.state = new AtomicReference<>(State.NEW);
    }

    @Override
    public synchronized void setUp() {

        // Validate state.
        assertState(
                this.state.get(), "setUp() called more than once.", State.NEW);

        // Set up resources.
        this.swingGui.setUp();
        try {
            EventQueue.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    swingGui.addWindowListener(windowListerner);
                    swingGui.setVisible(true);
                }
            });
        } catch (InterruptedException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }

        // Set state to NO_MODEL.
        this.state.set(State.NO_MODEL);
    }

    @Override
    public synchronized void loadModel(
            final SimulationModel simulationModel,
            final InputState inputState) {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp(), before tearDown().",
                State.NO_MODEL,
                State.HAS_MODEL);

        // Validate parameters.
        final SimulationModel simulationModel_dc = simulationModel;
        final InputState inputState_dc = inputState;
        if (simulationModel_dc == null || inputState_dc == null) {
            throw new NullPointerException(
                    "simulationModel: " + simulationModel_dc
                    + ", inputState: " + inputState_dc);
        }

        // Unload old simulation if present.
        if (stateRef == State.HAS_MODEL) {
            unloadModel();
        }

        // Load given simulation.
        this.guiVisualisation = new GuiVisualisation(
                simulationModel_dc, inputState_dc);
        this.guiVisualisation.attatch(this.swingGui);

        // Set state to HAS_MODEL.
        this.state.set(State.HAS_MODEL);
    }

    @Override
    public synchronized void unloadModel() {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp()"
                + " and loadSimulation(...), before tearDown().",
                State.HAS_MODEL);

        // Unload.
        this.guiVisualisation.detach();
        this.guiVisualisation = null;

        // Set state to IDLE.
        this.state.set(State.NO_MODEL);
    }

    @Override
    public synchronized void tearDown() {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp()."
                + " Cannot be called more than once.",
                State.NO_MODEL,
                State.HAS_MODEL);

        // Unload simulation if present.
        if (stateRef == State.HAS_MODEL) {
            this.unloadModel();
        }

        // Release resources.
        try {
            EventQueue.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    swingGui.dispose();
                }
            });
        } catch (InterruptedException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }

        // Set state to DISPOSED.
        this.state.set(State.DISPOSED);
        LOGGER.debug(this.getClass().getSimpleName() + " torn down.");
    }

    private static void assertState(
            final State ref,
            final String exceptionMessage,
            final State... states) {
        boolean hasCorrectState = false;
        int i = 0;
        while (!hasCorrectState && i < states.length) {
            if (states[i] == ref) {
                hasCorrectState = true;
            }
            i++;
        }
        if (!hasCorrectState) {
            throw new IllegalStateException(exceptionMessage);
        }
    }

    @Override
    public void update(
            final long deltaNanoseconds, final double interpolation) {
        if (this.state.get() != State.HAS_MODEL) {
            throw new IllegalStateException();
        }
        this.guiVisualisation.update(deltaNanoseconds, interpolation);
    }

    private class SwingGuiWindowListener implements WindowListener {

        @Override
        public void windowOpened(WindowEvent e) {
        }

        @Override
        public void windowClosing(WindowEvent e) {
            new Thread(new Runnable() {

                @Override
                public void run() {
                    application.tearDown();
                }
            }).start();
        }

        @Override
        public void windowClosed(WindowEvent e) {
        }

        @Override
        public void windowIconified(WindowEvent e) {
        }

        @Override
        public void windowDeiconified(WindowEvent e) {
        }

        @Override
        public void windowActivated(WindowEvent e) {
        }

        @Override
        public void windowDeactivated(WindowEvent e) {
        }

    }
}
