/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.opengl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.media.opengl.GL3;
import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.HookFileListener;
import jjs.cs.simulation.RenderHook;
import jjs.cs.simulation.data.WorldPosition;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class OpenGlModelFile {

    private final HookFile hookFile;
    private final HashSet<RenderHook> renderHooks;
    private final HashMap<Integer, OpenGlModelFile.ConvertedHook> idConvertedHooks;
    private final OpenGlTextureFile openGlTextureFile;
    private AtomicBoolean updateConvertedHooks;

    public OpenGlModelFile(final HookFile hookFile) {
        final HookFile hookFile_dc = hookFile;
        if (hookFile_dc == null) {
            throw new NullPointerException("hookFile: " + hookFile_dc);
        }
        this.hookFile = hookFile_dc;
        this.renderHooks
                = hookFile_dc.getHooksOfType(RenderHook.class);
        this.idConvertedHooks = new HashMap<>();
        this.openGlTextureFile = new OpenGlTextureFile();
        this.updateConvertedHooks = new AtomicBoolean(true);
        this.hookFile.addHookFileListener(new HookFileListener<RenderHook>() {

            @Override
            public void hookAdded(RenderHook hook) {
                updateConvertedHooks.set(true);
            }

            @Override
            public void hookRemoved(RenderHook hook) {
                updateConvertedHooks.set(true);
            }
        }, RenderHook.class);
    }

    public void update(GL3 gl3) {
        if (updateConvertedHooks.get()) {
            updateConvertedHooks.set(false);
            final HashSet<Integer> activeIds = new HashSet<>();
            for (RenderHook renderHook : renderHooks) {
                activeIds.add(renderHook.id);

                if (!idConvertedHooks.containsKey(renderHook.id)) {

                    /* If this is a new (unseen) hook, create a representation
                     * and store it on the GPU. */
                    idConvertedHooks.put(
                            renderHook.id,
                            new ConvertedHook(renderHook.position,
                                    OpenGlModel.createOnGpu(
                                            gl3,
                                            openGlTextureFile,
                                            renderHook.model)));
                }
            }
            for (Map.Entry<Integer, ConvertedHook> entry
                    : idConvertedHooks.entrySet()) {
                if (!activeIds.contains(entry.getKey())) {
                    // To do: delete these entries.
                }
            }
        }
    }

    Collection<OpenGlModelFile.ConvertedHook> getConvertedHooks() {
        return idConvertedHooks.values();
    }

    public static class ConvertedHook {

        final WorldPosition worldPosition;
        final OpenGlModel openGlModel;

        private ConvertedHook(
                WorldPosition worldPosition, OpenGlModel openGlModel) {
            this.worldPosition = worldPosition;
            this.openGlModel = openGlModel;
        }

    }
}
