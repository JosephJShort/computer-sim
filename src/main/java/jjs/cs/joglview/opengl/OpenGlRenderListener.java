/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.opengl;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.FloatBuffer;
import javax.media.opengl.GL2;
import javax.media.opengl.GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import org.apache.commons.io.FileUtils;
import jjs.cs.simulation.SimulationModel;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class OpenGlRenderListener implements GLEventListener {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(OpenGlRenderListener.class);

    private int glVertexShader;
    private int glFragmentShader;
    private int glShaderProgram;
    private OpenGlCamera camera;
    private SimulationModel simulationState;
    private OpenGlModelRenderer rendererProcessor;
    private OpenGlCameraProcessor cameraProcessor;

    private Matrix4f modelMatrix;
    public FloatBuffer modelMatrixBuffer;

    private int modelMatrixLocation;
    private int projectionMatrixLocation;
    private int viewMatrixLocation;

    public float interpolation;

    public OpenGlRenderListener(SimulationModel simulationState) {
        this.simulationState = simulationState;
    }

    @Override
    public void init(GLAutoDrawable drawable) {

        // Set up OpenGL context.
        final GL2 gl2 = drawable.getGL().getGL2();
        LOGGER.debug("OpenGL version: " + gl2.glGetString(GL2.GL_VERSION));
        gl2.glViewport(0, 0, drawable.getWidth(), drawable.getHeight());
        gl2.glClearColor(0.7f, 0.7f, 0.9f, 0f);
        gl2.glEnable(GL2.GL_DEPTH_TEST);
        gl2.glEnable(GL2.GL_COLOR_MATERIAL);

        // Create shader program.
        loadShaders(gl2);
        gl2.glUseProgram(glShaderProgram);

        // Create camera.
        camera = new OpenGlCamera();
        this.rendererProcessor = new OpenGlModelRenderer(simulationState);
        this.cameraProcessor = new OpenGlCameraProcessor(camera, simulationState);

        this.projectionMatrixLocation = gl2.glGetUniformLocation(
                this.glShaderProgram, "projectionMatrix");
        this.viewMatrixLocation = gl2.glGetUniformLocation(
                this.glShaderProgram, "viewMatrix");
        this.modelMatrixLocation
                = gl2.glGetUniformLocation(glShaderProgram, "modelMatrix");

        this.modelMatrixBuffer = BufferUtils.createFloatBuffer(16);
        this.modelMatrix = new Matrix4f();

        gl2.glUseProgram(0);
    }

    private void loadShaders(GL2 gl2) {
        // Load shader programs.
        glVertexShader = readGlslToShader(
                gl2, "VertexShader-0.3", GL2.GL_VERTEX_SHADER);
        glFragmentShader = readGlslToShader(
                gl2, "FragmentShader-0.2", GL2.GL_FRAGMENT_SHADER);

        glShaderProgram = gl2.glCreateProgram();
        gl2.glAttachShader(glShaderProgram, glVertexShader);
        gl2.glAttachShader(glShaderProgram, glFragmentShader);

        gl2.glBindAttribLocation(glShaderProgram, 0, "in_Position");
        gl2.glBindAttribLocation(glShaderProgram, 1, "in_Color");
        gl2.glBindAttribLocation(glShaderProgram, 2, "in_TextureCoord");
        gl2.glLinkProgram(glShaderProgram);
        gl2.glValidateProgram(glShaderProgram);
    }

    private static int readGlslToShader(GL2 gl2, String filename, int type) {
        final URI glslFileUri;
        try {
            glslFileUri = OpenGlRenderListener.class
                    .getResource("/glsl/" + filename)
                    .toURI();
        } catch (URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
        LOGGER.debug("Loading shader: " + glslFileUri);
        final String shaderSource;
        try {
            shaderSource = FileUtils.readFileToString(
                    new File(glslFileUri),
                    "UTF-8");
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        if (shaderSource.isEmpty()) {
            throw new IllegalStateException(
                    "Shader source is empty (an empty string). Filename: "
                    + filename + ".");
        }
        final int id = gl2.glCreateShader(type);

        gl2.glShaderSource(
                id, 1, new String[]{shaderSource}, (int[]) null, 0);
        gl2.glCompileShader(id);
        return id;
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        LOGGER.debug("dispose(GLAutoDrawable) called.");
        final GL2 gl2 = drawable.getGL().getGL2();
        gl2.glUseProgram(0);
        gl2.glDetachShader(glShaderProgram, glVertexShader);
        gl2.glDetachShader(glShaderProgram, glFragmentShader);
        gl2.glDeleteShader(glVertexShader);
        gl2.glDeleteShader(glFragmentShader);
        gl2.glDeleteProgram(glShaderProgram);
    }

    @Override
    public void reshape(
            GLAutoDrawable drawable, int x, int y, int width, int height) {
        this.camera.setViewportDimensions(width, height);
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        final GL2 gl2 = drawable.getGL().getGL2();

        // Clear display.
        gl2.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

        // Bind to shader program.
        gl2.glUseProgram(glShaderProgram);

        // Update camera state data, then update camera object.
        cameraProcessor.update(0);
        camera.update(
                gl2,
                glShaderProgram,
                projectionMatrixLocation,
                viewMatrixLocation);

        // Update picking ray.
        simulationState.mouse.rayOrigin.set(camera.getPosition());
        simulationState.mouse.rayDestination.set(camera.getPickingRayDirection(
                simulationState.mouse.posX, simulationState.mouse.posY));

        // Render the frame.
        rendererProcessor.update(gl2, modelMatrixLocation, interpolation);

        //drawPickingRay(gl2);
        // Unbind from shader program.
        gl2.glUseProgram(0);
    }

    private void drawPickingRay(GL2 gl2) {
        gl2.glActiveTexture(GL3.GL_TEXTURE0);
        gl2.glBindTexture(GL3.GL_TEXTURE_2D, 0);

        final Vector3f pickOrigin = simulationState.mouse.rayOrigin;
        final Vector3f pickDirection = simulationState.mouse.rayDestination;
        final float nearPlane = camera.getNearPlane();
        final float farPlane = camera.getFarPlane();

        final float crossRadius = 0.01f;

        final Vector3f near = new Vector3f(pickDirection);
        near.scale((nearPlane + 0.1f) / Math.abs(pickDirection.z));

        final Vector3f far = new Vector3f(pickDirection);
        float factor = farPlane / Math.abs(pickDirection.z);
        far.scale(factor);

        near.translate(pickOrigin.x, pickOrigin.y, pickOrigin.z);
        far.translate(pickOrigin.x, pickOrigin.y, pickOrigin.z);

        modelMatrix.setIdentity();
        modelMatrix.store(modelMatrixBuffer);
        modelMatrixBuffer.flip();
        gl2.glUniformMatrix4fv(
                modelMatrixLocation,
                1,
                false,
                modelMatrixBuffer);
        gl2.glBegin(GL2.GL_LINES);

        // Draw line from near to far (should appear to be a point).
        gl2.glVertex3f(near.x, near.y, near.z);
        gl2.glVertex3f(far.x, far.y, far.z);

        // Draw cross.
        gl2.glVertex3f(near.x, near.y, near.z);
        gl2.glVertex3f(near.x + crossRadius, near.y + crossRadius, near.z);
        gl2.glVertex3f(near.x, near.y, near.z);
        gl2.glVertex3f(near.x + crossRadius, near.y - crossRadius, near.z);
        gl2.glVertex3f(near.x, near.y, near.z);
        gl2.glVertex3f(near.x - crossRadius, near.y - crossRadius, near.z);
        gl2.glVertex3f(near.x, near.y, near.z);
        gl2.glVertex3f(near.x - crossRadius, near.y + crossRadius, near.z);

        // Draw line from near to world origin.
        gl2.glColor3f(1f, 0, 0);
        gl2.glVertex3f(near.x, near.y, near.z);
        gl2.glVertex3f(0, 0, 0);
        gl2.glEnd();
    }
}
