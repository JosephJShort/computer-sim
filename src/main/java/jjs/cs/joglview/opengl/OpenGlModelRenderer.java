/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.opengl;

import java.nio.FloatBuffer;
import javax.media.opengl.GL2;
import javax.media.opengl.GL3;
import jjs.cs.simulation.SimulationModel;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class OpenGlModelRenderer {

    private final SimulationModel simulationState;
    private final Matrix4f modelMatrix;
    public final FloatBuffer modelMatrixBuffer;

    private final OpenGlModelFile openGlModelFile;

    public OpenGlModelRenderer(SimulationModel simulationState) {
        final SimulationModel simulationState_dc = simulationState;
        if (simulationState_dc == null) {
            throw new NullPointerException(
                    "simulationState: " + simulationState_dc);
        }
        this.simulationState = simulationState_dc;

        this.modelMatrixBuffer = BufferUtils.createFloatBuffer(16);
        this.modelMatrix = new Matrix4f();

        this.openGlModelFile = new OpenGlModelFile(simulationState_dc.hookFile);
    }

    public void update(GL2 gl2, int modelMatrixLocation, double interpolation) {
        this.openGlModelFile.update(gl2.getGL3());

        // Unbind any texture before batch.
        gl2.glActiveTexture(GL3.GL_TEXTURE0);
        gl2.glBindTexture(GL3.GL_TEXTURE_2D, 0);
        int boundTexture = 0;

        for (OpenGlModelFile.ConvertedHook hook
                : openGlModelFile.getConvertedHooks()) {

            // Move sprite from model space to world space.
            modelMatrix.setIdentity();
            Matrix4f.translate(hook.worldPosition.position,
                    modelMatrix, modelMatrix);
            Matrix4f.rotate((float) Math.toRadians(hook.worldPosition.angle.z),
                    new Vector3f(0, 0, 1),
                    modelMatrix,
                    modelMatrix);
            Matrix4f.rotate(
                    (float) Math.toRadians(hook.worldPosition.angle.y),
                    new Vector3f(0, 1, 0),
                    modelMatrix,
                    modelMatrix);
            Matrix4f.rotate(
                    (float) Math.toRadians(hook.worldPosition.angle.x),
                    new Vector3f(1, 0, 0),
                    modelMatrix,
                    modelMatrix);
            Matrix4f.scale(hook.worldPosition.scale, modelMatrix, modelMatrix);
            modelMatrix.store(modelMatrixBuffer);
            modelMatrixBuffer.flip();
            gl2.glUniformMatrix4fv(
                    modelMatrixLocation,
                    1,
                    false,
                    modelMatrixBuffer);

            // Bind texture if necessary.
            final int texId = hook.openGlModel.wireframe ? 0 : hook.openGlModel.textureLocation;
            if (texId != boundTexture) {
                gl2.glActiveTexture(GL3.GL_TEXTURE0);
                gl2.glBindTexture(GL3.GL_TEXTURE_2D, texId);
                boundTexture = texId;
            }

            if (hook.openGlModel.wireframe) {
                gl2.glPolygonMode(GL3.GL_FRONT_AND_BACK, GL3.GL_LINE);
            }

            // Bind VAO, VBOs.
            gl2.glBindVertexArray(hook.openGlModel.vaoLocation);
            gl2.glEnableVertexAttribArray(0);
            gl2.glEnableVertexAttribArray(1);
            gl2.glEnableVertexAttribArray(2);
            gl2.glBindBuffer(
                    GL3.GL_ELEMENT_ARRAY_BUFFER,
                    hook.openGlModel.indicesVboLocation);

            // Draw.
            gl2.glDrawElements(
                    GL3.GL_TRIANGLES,
                    hook.openGlModel.indicesLength,
                    GL3.GL_UNSIGNED_BYTE,
                    0);

            // Unbind.
            gl2.glBindBuffer(GL3.GL_ELEMENT_ARRAY_BUFFER, 0);
            gl2.glDisableVertexAttribArray(0);
            gl2.glDisableVertexAttribArray(1);
            gl2.glDisableVertexAttribArray(2);
            gl2.glBindVertexArray(0);

            if (hook.openGlModel.wireframe) {
                gl2.glPolygonMode(GL3.GL_FRONT_AND_BACK, GL3.GL_FILL);
            }
        }
    }
}
