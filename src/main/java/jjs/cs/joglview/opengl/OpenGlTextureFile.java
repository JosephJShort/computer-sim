/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.opengl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;
import javax.media.opengl.GL3;
import org.apache.commons.io.FileUtils;
import org.lwjgl.opengl.GL13;
import org.newdawn.slick.opengl.PNGDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class OpenGlTextureFile {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(OpenGlTextureFile.class);

    private final Map<String, Integer> textureNameIds;
    private final IntBuffer intbuffer = IntBuffer.allocate(1);

    public OpenGlTextureFile() {
        textureNameIds = new HashMap<>();
    }

    public void load(GL3 gl3, String filename) {
        if (gl3 == null || filename == null) {
            throw new NullPointerException(
                    "gl3: " + gl3 + ", filename: " + filename);
        }
        if (!textureNameIds.containsKey(filename)) {
            textureNameIds.put(
                    filename,
                    loadAndBindTexture(gl3, filename, GL13.GL_TEXTURE0));
        }
    }

    public int get(String givenName) {
        final int id;
        if (textureNameIds.containsKey(givenName)) {
            id = textureNameIds.get(givenName);
        } else {
            id = 0;
        }
        return id;
    }

    private int loadAndBindTexture(GL3 gl3, String filename, int textureUnit) {
        final URI textureFileUri;
        try {
            final URL texUrl = OpenGlTextureFile.class
                    .getResource("/textures/" + filename);
            if (texUrl != null) {
                textureFileUri = texUrl.toURI();
            } else {
                textureFileUri = null;
            }
        } catch (URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
        if (textureFileUri == null) {
            throw new RuntimeException(
                    "Could not find texture file: " + filename);
        }
        LOGGER.debug("Loading texture: " + textureFileUri);
        final int textureWidth;
        final int textureHeight;
        final ByteBuffer byteBuffer;

        // Decode.
        try (InputStream is
                = FileUtils.openInputStream(new File(textureFileUri))) {
            final PNGDecoder decoder = new PNGDecoder(is);
            textureWidth = decoder.getWidth();
            textureHeight = decoder.getHeight();
            byteBuffer = ByteBuffer.allocateDirect(
                    4 * textureWidth * textureHeight);
            decoder.decode(byteBuffer, textureWidth * 4, PNGDecoder.RGBA);
            byteBuffer.flip();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        // Send to OpenGL.
        gl3.glGenTextures(1, intbuffer);
        final int id = intbuffer.get(0);

        gl3.glActiveTexture(textureUnit);
        gl3.glBindTexture(GL3.GL_TEXTURE_2D, id);
        gl3.glPixelStorei(GL3.GL_UNPACK_ALIGNMENT, 1);
        gl3.glTexImage2D(
                GL3.GL_TEXTURE_2D,
                0,
                GL3.GL_RGB,
                textureWidth,
                textureHeight,
                0,
                GL3.GL_RGBA,
                GL3.GL_UNSIGNED_BYTE,
                byteBuffer);
        gl3.glGenerateMipmap(GL3.GL_TEXTURE_2D);
        gl3.glTexParameteri(
                GL3.GL_TEXTURE_2D, GL3.GL_TEXTURE_WRAP_S, GL3.GL_REPEAT);
        gl3.glTexParameteri(
                GL3.GL_TEXTURE_2D, GL3.GL_TEXTURE_WRAP_T, GL3.GL_REPEAT);
        gl3.glTexParameteri(
                GL3.GL_TEXTURE_2D,
                GL3.GL_TEXTURE_MAG_FILTER,
                GL3.GL_NEAREST);
        gl3.glTexParameteri(
                GL3.GL_TEXTURE_2D,
                GL3.GL_TEXTURE_MIN_FILTER,
                GL3.GL_LINEAR_MIPMAP_LINEAR);
        return id;
    }
}
