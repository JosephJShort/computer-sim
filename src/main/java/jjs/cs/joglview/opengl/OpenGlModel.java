/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.opengl;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;
import javax.media.opengl.GL3;
import jjs.cs.simulation.data.RenderModel;
import org.lwjgl.BufferUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class OpenGlModel {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(OpenGlModel.class);

    private static final IntBuffer bufferGenIntBuffer = IntBuffer.allocate(1);
    private static final IntBuffer vertexBuffer = IntBuffer.allocate(1);
    private static final IntBuffer indicesVertexBuffer = IntBuffer.allocate(1);

    public final int vaoLocation;
    public final int indicesVboLocation;
    public final int indicesLength;
    public final int textureLocation;
    public final boolean wireframe;
    private boolean active;

    public OpenGlModel(
            int vaoLocation,
            int indicesVboLocation,
            int indicesLength,
            int textureLocation,
            boolean wireframe) {
        this.vaoLocation = vaoLocation;
        this.indicesVboLocation = indicesVboLocation;
        this.indicesLength = indicesLength;
        this.textureLocation = textureLocation;
        this.wireframe = wireframe;
        this.active = true;
    }

    static OpenGlModel createOnGpu(
            final GL3 gl3,
            final OpenGlTextureFile openGlTextureFile,
            final RenderModel model) {
        final GL3 gl3_dc = gl3;
        final OpenGlTextureFile openGlTextureFile_dc = openGlTextureFile;
        final RenderModel model_dc = model;
        if (gl3_dc == null
                || openGlTextureFile_dc == null
                || model_dc == null) {
            throw new NullPointerException(
                    "gl3: " + gl3_dc
                    + ", openGlTextureFile: " + openGlTextureFile_dc
                    + ", model: " + model_dc + ".");
        }

        // Convert data from RenderModel.
        openGlTextureFile_dc.load(gl3, model.textureName);
        final int textureId = openGlTextureFile_dc.get(model.textureName);
        final boolean wireframe = model_dc.wireframe;
        OpenGlVertex[] vertices = convertVertices(model_dc.vertices);
        byte[] indices = convertIndices(model_dc.vertexIndices);

        // Prepare memory buffers.
        final FloatBuffer verticesBuffer = BufferUtils.createFloatBuffer(
                vertices.length * OpenGlVertex.LENGTH);
        ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(indices.length);

        // Load verticesBuffer.
        for (OpenGlVertex vertex : vertices) {
            verticesBuffer.put(vertex.data);
        }
        verticesBuffer.flip();

        // Load indices buffer.
        indicesBuffer.put(indices);
        indicesBuffer.flip();

        // Create VAO.
        gl3_dc.glGenVertexArrays(1, bufferGenIntBuffer);
        final int vertexArrayObject = bufferGenIntBuffer.get(0);

        // Bind to VAO.
        gl3_dc.glBindVertexArray(vertexArrayObject);

        // Create VBO.
        gl3_dc.glGenBuffers(1, vertexBuffer);
        final int vertexBufferObject = vertexBuffer.get(0);

        // Bind to the VBO, send the data, set attribute pointers and unbind.
        gl3_dc.glBindBuffer(GL3.GL_ARRAY_BUFFER, vertexBufferObject);
        gl3_dc.glBufferData(
                GL3.GL_ARRAY_BUFFER,
                verticesBuffer.remaining() << 2,
                verticesBuffer,
                GL3.GL_STREAM_DRAW);
        gl3_dc.glVertexAttribPointer(
                0,
                OpenGlVertex.POSITION_LENGTH,
                GL3.GL_FLOAT,
                false,
                OpenGlVertex.BYTES,
                OpenGlVertex.POSITION_BYTE_OFFSET);
        gl3_dc.glVertexAttribPointer(
                1,
                OpenGlVertex.COLOUR_LENGTH,
                GL3.GL_FLOAT,
                false,
                OpenGlVertex.BYTES,
                OpenGlVertex.COLOUR_BYTE_OFFSET);
        gl3_dc.glVertexAttribPointer(
                2,
                OpenGlVertex.TEXTURE_COORD_LENGTH,
                GL3.GL_FLOAT,
                false,
                OpenGlVertex.BYTES,
                OpenGlVertex.TEXTURE_COORD_BYTE_OFFSET);
        gl3_dc.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
        gl3_dc.glBindVertexArray(0);

        // Create indices VBO.
        gl3_dc.glGenBuffers(1, indicesVertexBuffer);
        final int indicesVertexBufferObject = indicesVertexBuffer.get(0);

        // Bind, send data and unbind.
        gl3_dc.glBindBuffer(
                GL3.GL_ELEMENT_ARRAY_BUFFER, indicesVertexBufferObject);
        gl3_dc.glBufferData(
                GL3.GL_ELEMENT_ARRAY_BUFFER,
                indicesBuffer.remaining() * 4,
                indicesBuffer,
                GL3.GL_STATIC_DRAW);
        gl3_dc.glBindBuffer(GL3.GL_ELEMENT_ARRAY_BUFFER, 0);

        final OpenGlModel openGlModel = new OpenGlModel(
                vertexArrayObject,
                indicesVertexBufferObject,
                indices.length,
                textureId,
                wireframe);
        LOGGER.debug("Created a new model on the GPU. OpenGlModel: "
                + openGlModel + ".");

        return openGlModel;
    }

    private static OpenGlVertex[] convertVertices(List<RenderModel.Vertex> vertices) {
        final int verticesSize = vertices.size();
        final OpenGlVertex[] openGlVertices = new OpenGlVertex[vertices.size()];
        for (int i = 0; i < verticesSize; i++) {
            final RenderModel.Vertex vertex = vertices.get(i);
            openGlVertices[i] = new OpenGlVertex(
                    vertex.positionX,
                    vertex.positionY,
                    vertex.positionZ,
                    1f,
                    vertex.red,
                    vertex.green,
                    vertex.blue,
                    vertex.alpha,
                    vertex.texS,
                    vertex.texT);
        }
        return openGlVertices;
    }

    private static byte[] convertIndices(List<Integer> indices) {
        final int indicesSize = indices.size();
        byte[] convertedIndices = new byte[indices.size()];
        for (int i = 0; i < indicesSize; i++) {
            convertedIndices[i] = indices.get(i).byteValue();
        }
        return convertedIndices;
    }

    public void destroy() {
//        gl3.glBindVertexArray(vertexArrayObject);
//        gl3.glDisableVertexAttribArray(0);
//        gl3.glDisableVertexAttribArray(1);
//        gl3.glBindBuffer(GL3.GL_ARRAY_BUFFER, 0);
//        gl3.glDeleteBuffers(vertexBufferObject);
//        gl3.glBindBuffer(GL3.GL_ELEMENT_ARRAY_BUFFER, 0);
//        gl3.glDeleteBuffers(indicesVertexBufferObject);
//        gl3.glBindVertexArray(0);
//        gl3.glDeleteVertexArrays(vertexArrayObject);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + this.vaoLocation;
        hash = 73 * hash + this.indicesVboLocation;
        hash = 73 * hash + this.indicesLength;
        hash = 73 * hash + this.textureLocation;
        hash = 73 * hash + (this.wireframe ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OpenGlModel other = (OpenGlModel) obj;
        if (this.vaoLocation != other.vaoLocation) {
            return false;
        }
        if (this.indicesVboLocation != other.indicesVboLocation) {
            return false;
        }
        if (this.indicesLength != other.indicesLength) {
            return false;
        }
        if (this.textureLocation != other.textureLocation) {
            return false;
        }
        if (this.wireframe != other.wireframe) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "OpenGlModel{"
                + "vaoLocation=" + vaoLocation
                + ", indicesVboLocation=" + indicesVboLocation
                + ", indicesLength=" + indicesLength
                + ", textureLocation=" + textureLocation
                + ", wireframe=" + wireframe
                + ", active=" + active
                + '}';
    }
}
