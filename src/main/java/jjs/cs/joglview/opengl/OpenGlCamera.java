/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.opengl;

import java.nio.FloatBuffer;
import javax.media.opengl.GL2;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class OpenGlCamera {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(OpenGlCamera.class);

    private final Matrix4f localProjectionMatrix;
    private final Matrix4f localViewMatrix;
    private final FloatBuffer matrixBuffer;
    private final Vector3f position;

    private int viewportWidth;
    private int viewportHeight;
    private float fieldOfView;
    private float aspectRatio;
    private final float nearPlane = 0.1f;
    private final float farPlane = 1000f;
    private float yScale;
    private float xScale;
    private float frustrumLength;

    private boolean updateViewMatrix;
    private boolean updateProjectionMatrix;

    public OpenGlCamera() {
        this(1, 1);
    }

    public OpenGlCamera(int viewportWidth, int viewportHeight) {
        if (viewportWidth <= 0 || viewportHeight <= 0) {
            throw new IllegalArgumentException(
                    "viewportWidth and viewportWidth must be positive."
                    + " viewportWidth: " + viewportWidth
                    + ",  viewportHeight: " + viewportHeight + ".");
        }
        this.localProjectionMatrix = new Matrix4f();
        this.localViewMatrix = new Matrix4f();
        this.matrixBuffer = BufferUtils.createFloatBuffer(16);
        this.position = new Vector3f();
        this.updateViewMatrix = true;
        this.updateProjectionMatrix = true;
        this.viewportWidth = viewportWidth;
        this.viewportHeight = viewportHeight;
        this.fieldOfView = 90;
    }

    public boolean update(
            GL2 gl2,
            int shaderProgramLocation,
            int projectionMatrixLocation,
            int viewMatrixLocation) {
        final GL2 gl2_dc = gl2;

        boolean updated = false;
        if (updateProjectionMatrix) {
            gl2.glUseProgram(shaderProgramLocation);
            updateProjectionMatrix(gl2_dc, projectionMatrixLocation);
            updateProjectionMatrix = false;
            updated = true;
        }
        if (updateViewMatrix) {
            if (!updated) {
                gl2.glUseProgram(shaderProgramLocation);
            }
            updateViewMatrix(gl2_dc, viewMatrixLocation);
            updateViewMatrix = false;
            updated = true;
        }

        return updated;
    }

    private void updateProjectionMatrix(GL2 gl2, int projectionMatrixLocation) {

        // Calculate the dimensions of the frustrum.
        aspectRatio = viewportHeight == 0
                ? 1f : (float) viewportWidth / (float) viewportHeight;
        yScale = (float) (1f / Math.tan((fieldOfView / 2f)
                * (float) (Math.PI / 180d)));
        xScale = yScale / aspectRatio;
        frustrumLength = farPlane - nearPlane;

        localProjectionMatrix.m00 = xScale;
        localProjectionMatrix.m11 = yScale;
        localProjectionMatrix.m22 = -((farPlane + nearPlane) / frustrumLength);
        localProjectionMatrix.m23 = -1f;
        localProjectionMatrix.m32
                = -((2f * nearPlane * farPlane) / frustrumLength);
        localProjectionMatrix.m33 = 0f;

        // Calculate aspect ratio GCD.
        long gcd;
        {
            gcd = viewportWidth;
            long temp = viewportHeight;
            while (temp > 0) {
                long swap = temp;
                temp = gcd % temp;
                gcd = swap;
            }
        }

        // Update on GPU.
        localProjectionMatrix.store(matrixBuffer);
        matrixBuffer.flip();
        gl2.glUniformMatrix4fv(
                projectionMatrixLocation, 1, false, matrixBuffer);

        LOGGER.info(
                "Projection updated: Viewport width = " + viewportWidth + ";"
                + " viewport height = " + viewportHeight + ";"
                + " aspect ratio = " + aspectRatio + " ("
                + (viewportWidth / gcd) + ":" + (viewportHeight / gcd) + ");"
                + " field of view = " + fieldOfView + ".");
    }

    private void updateViewMatrix(GL2 gl2, int viewMatrixLocation) {

        // Translate the view matrix by the negative of the camera position.
        localViewMatrix.setIdentity();
        final Vector3f negatedCameraPos;
        synchronized (this.position) {
            negatedCameraPos = new Vector3f(position);
        }
        negatedCameraPos.negate();
        Matrix4f.translate(negatedCameraPos, localViewMatrix, localViewMatrix);

        // Update on GPU.
        localViewMatrix.store(matrixBuffer);
        matrixBuffer.flip();
        gl2.glUniformMatrix4fv(viewMatrixLocation, 1, false, matrixBuffer);

//        LOGGER.info("View updated: negated camera position = "
//                + negatedCameraPos + ".");
    }

    public Vector3f getPickingRayDirection(final int mouseX, final int mouseY) {
        final float xPos
                = (2.0f * (float) mouseX) / (float) viewportWidth - 1.0f;
        final float yPos
                = (2.0f * (float) mouseY) / (float) viewportHeight - 1.0f;
        final Vector3f rayPosition = new Vector3f(xPos, yPos, 1);

        final Vector4f rayClip
                = new Vector4f(rayPosition.x, rayPosition.y, -1, 1);
        final Matrix4f inverseProjection
                = new Matrix4f(localProjectionMatrix);
        inverseProjection.invert();

        final Vector4f rayEye
                = Matrix4f.transform(inverseProjection, rayClip, null);
        rayEye.z = -1;
        rayEye.w = 0;

        final Matrix4f inverseView = new Matrix4f(localViewMatrix);
        inverseProjection.invert();
        final Vector4f rayWorld = Matrix4f.transform(inverseView, rayEye, null);

        final Vector3f pickingRayDirection
                = new Vector3f(rayWorld.x, rayWorld.y, rayWorld.z);
        pickingRayDirection.normalise();
        return pickingRayDirection;
    }

    public void setViewportDimensions(int viewportWidth, int viewportHeight) {
        if (viewportWidth <= 0 || viewportHeight <= 0) {
            throw new IllegalArgumentException(
                    "viewportWidth and viewportWidth must be positive."
                    + " viewportWidth: " + viewportWidth
                    + ",  viewportHeight: " + viewportHeight + ".");
        }
        this.viewportWidth = viewportWidth;
        this.viewportHeight = viewportHeight;
        this.updateProjectionMatrix = true;
    }

    public void setPosition(float x, float y, float z) {
        synchronized (this.position) {
            this.position.set(x, y, z);
        }
        this.updateViewMatrix = true;
    }

    public void setFieldOfView(float fieldOfView) {
        if (fieldOfView <= 0) {
            throw new IllegalArgumentException(
                    "fieldOfView must positive."
                    + " fieldOfView: " + fieldOfView + ".");
        } else if (fieldOfView > 180) {
            throw new IllegalArgumentException(
                    "fieldOfView cannot be larger than 180f."
                    + " fieldOfView: " + fieldOfView + ".");
        }
        this.fieldOfView = fieldOfView;
        this.updateProjectionMatrix = true;
    }

    public float getAspectRatio() {
        return aspectRatio;
    }

    public float getFieldOfView() {
        return fieldOfView;
    }

    public Vector3f getPosition() {
        return new Vector3f(position);
    }

    public Matrix4f getProjectionMatrix() {
        return new Matrix4f(localProjectionMatrix);
    }

    public Matrix4f getViewMatrix() {
        return new Matrix4f(localViewMatrix);
    }

    public float getNearPlane() {
        return nearPlane;
    }

    public float getFarPlane() {
        return farPlane;
    }

}
