/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.opengl;

import jjs.cs.control.Processor;
import jjs.cs.simulation.SimulationModel;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class OpenGlCameraProcessor extends Processor {

    private static final float[] ZOOM_LEVELS = {
        //        64f,
        //        48f,
        //        32f,
        //        24f,
        16f,
        12f,
        8f,
        6f,
        4f,
        3f,
        2f,
        1.5f,
        1f,
        2f / 3f,
        1f / 2f,
        1f / 3f,
        1f / 4f,
        1f / 6f,
        1f / 8f,
        1f / 12f,
        1f / 16f,
        1f / 24f,
        1f / 32f
    };
    private static final int UNIT_ZOOM_LEVEL_INDEX = 8;
    private static final float DEFAULT_ZOOM = 60f;
    private static final float D_WHEEL_FACTOR = 1f;

    private final OpenGlCamera camera;
    private final SimulationModel.Camera cameraState;
    private final SimulationModel.Mouse mouse;
    private final SimulationModel.Picking picking;

    private boolean updatePosition = false;

    private int zoomLevelIndex = UNIT_ZOOM_LEVEL_INDEX;
    private float zoomLevelIndexInterpolation = zoomLevelIndex;

    private GrabState grabState = GrabState.NOT_GRABBED;
    private float oldPosX = 0;
    private float oldPosY = 0;
    private final Vector3f grabOrigin = new Vector3f();
    private float grabZScale = 0;

    private static enum GrabState {

        NOT_GRABBED,
        GRABBED,
        IGNORE
    }

    public OpenGlCameraProcessor(OpenGlCamera camera, SimulationModel simulationState) {
        super(simulationState);
        this.camera = camera;
        this.cameraState = simulationState.camera;
        this.mouse = simulationState.mouse;
        this.picking = simulationState.picking;
        this.updatePosition = false;

        cameraState.posX = -19.8f;
        cameraState.posY = 45.4f;
        cameraState.posZ = DEFAULT_ZOOM * ZOOM_LEVELS[zoomLevelIndex];
        camera.setPosition(
                cameraState.posX,
                cameraState.posY,
                cameraState.posZ);
    }

    @Override
    public void update(long deltaNanoseconds) {
        updatePosition = false;
        updateMouseWheelZoom();
        updateMouseGrab();
        if (updatePosition) {
            camera.setPosition(
                    cameraState.posX,
                    cameraState.posY,
                    cameraState.posZ);
        }
    }

    private void updateMouseWheelZoom() {
        final double dWheel = mouse.dWheel;
        if (dWheel != 0) {
            mouse.dWheel = 0d;
            zoomLevelIndexInterpolation += (float) dWheel * -1f * D_WHEEL_FACTOR;
            if (zoomLevelIndexInterpolation < 0) {
                zoomLevelIndexInterpolation = 0;
            } else if (zoomLevelIndexInterpolation > ZOOM_LEVELS.length - 1) {
                zoomLevelIndexInterpolation = ZOOM_LEVELS.length - 1;
            }
            final int newZoomLevelIndex
                    = Math.round(zoomLevelIndexInterpolation);
            if (newZoomLevelIndex != zoomLevelIndex) {
                zoomLevelIndex = newZoomLevelIndex;
                cameraState.posZ = DEFAULT_ZOOM * ZOOM_LEVELS[zoomLevelIndex];
                updatePosition = true;
            }
        }
    }

    private void updateMouseGrab() {

        // Update grabState.
        if (mouse.leftButtonDown
                && grabState == GrabState.NOT_GRABBED) {
            if (picking.somethingHovered
                    || simulationState.gui.mouseOverSomething) {
                grabState = GrabState.IGNORE;
            } else {
                grabState = GrabState.GRABBED;
                grabOrigin.set(mouse.rayOrigin);
                grabZScale = -grabOrigin.z / mouse.rayDestination.z;
                oldPosX = grabOrigin.x + mouse.rayDestination.x * grabZScale;
                oldPosY = grabOrigin.y + mouse.rayDestination.y * grabZScale;
            }
        } else if (!mouse.leftButtonDown) {
            grabState = GrabState.NOT_GRABBED;
        }

        // Move the camera if grabbed.
        if (grabState == GrabState.GRABBED) {
            grabZScale = -grabOrigin.z / mouse.rayDestination.z;
            final float newPosX
                    = grabOrigin.x + mouse.rayDestination.x * grabZScale;
            final float newPosY
                    = grabOrigin.y + mouse.rayDestination.y * grabZScale;
            cameraState.posX += oldPosX - newPosX;
            cameraState.posY += oldPosY - newPosY;
            oldPosX = newPosX;
            oldPosY = newPosY;
            cameraState.grabbed = true;
            updatePosition = true;
        } else {
            cameraState.grabbed = false;
        }
    }
}
