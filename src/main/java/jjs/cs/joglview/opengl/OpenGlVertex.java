/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.opengl;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class OpenGlVertex {

    public static final int BYTES_PER_FLOAT = 4;

    // Lengths.
    public static final int POSITION_LENGTH = 4;
    public static final int COLOUR_LENGTH = 4;
    public static final int TEXTURE_COORD_LENGTH = 2;

    // Bytes.
    public static final int POSITION_BYTES
            = POSITION_LENGTH * BYTES_PER_FLOAT;
    public static final int COLOUR_BYTES
            = COLOUR_LENGTH * BYTES_PER_FLOAT;
    public static final int TEXTURE_COORD_BYTES
            = TEXTURE_COORD_LENGTH * BYTES_PER_FLOAT;

    // Byte offsets.
    public static final int POSITION_BYTE_OFFSET = 0;
    public static final int COLOUR_BYTE_OFFSET
            = POSITION_BYTE_OFFSET + POSITION_BYTES;
    public static final int TEXTURE_COORD_BYTE_OFFSET
            = COLOUR_BYTE_OFFSET + COLOUR_BYTES;

    // Total length and bytes.
    public static final int LENGTH
            = POSITION_LENGTH
            + COLOUR_LENGTH
            + TEXTURE_COORD_LENGTH;
    public static final int BYTES
            = POSITION_BYTES
            + COLOUR_BYTES
            + TEXTURE_COORD_BYTES;

    // (xyzw_rgba_st)
    public final float[] data;

    public OpenGlVertex(
            float posX,
            float posY,
            float posZ,
            float posW,
            float red,
            float green,
            float blue,
            float alpha,
            float texS,
            float texT) {
        this.data = new float[]{
            posX,
            posY,
            posZ,
            posW,
            red,
            green,
            blue,
            alpha,
            texS,
            texT};
    }
}
