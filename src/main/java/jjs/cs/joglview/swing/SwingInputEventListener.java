/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.swing;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import jjs.cs.simulation.SimulationModel;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class SwingInputEventListener implements
        KeyListener,
        MouseListener,
        MouseMotionListener,
        MouseWheelListener {

    private final SimulationModel simulationState;

    public SwingInputEventListener(SimulationModel simulationState) {
        this.simulationState = simulationState;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //
    }

    @Override
    public void mousePressed(MouseEvent e) {
        final int button = e.getButton();
        if (button == MouseEvent.BUTTON1) {
            simulationState.mouse.leftButtonDown = true;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        final int button = e.getButton();
        if (button == MouseEvent.BUTTON1) {
            simulationState.mouse.leftButtonDown = false;
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //System.out.println("e = " + e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        simulationState.mouse.posX = e.getX();
        simulationState.mouse.posY = e.getComponent().getSize().height - e.getY();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        simulationState.mouse.posX = e.getX();
        simulationState.mouse.posY = e.getComponent().getSize().height - e.getY();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        simulationState.mouse.dWheel = e.getPreciseWheelRotation();
    }

}
