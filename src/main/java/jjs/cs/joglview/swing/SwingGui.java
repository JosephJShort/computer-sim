/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.swing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import jjs.cs.ComputerSimApplication;
import jjs.cs.simcom.SimCom;
import jjs.cs.simulation.SimulationModel;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class SwingGui extends JFrame {

    static {
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException |
                InstantiationException |
                IllegalAccessException |
                UnsupportedLookAndFeelException ex) {
            throw new RuntimeException(ex);
        }
        JFrame.setDefaultLookAndFeelDecorated(true);
    }

    private static final long serialVersionUID = -1l;

    private final ComputerSimApplication computerSimApplication;

    // Menu components.
    private MenuListener menuListener;
    private JMenuBar menuBar;
    private JMenu menuFile;
    private JMenuItem menuFileOpen;
    private JMenuItem menuFileClose;
    private JMenuItem menuFileQuit;
    private JMenu menuHelp;

    JDesktopPane desktopPane;
    JPanel bottomPanel;

    private GuiVisualisation guiVisualisation;

    public SwingGui(ComputerSimApplication computerSimApplication) {
        this.computerSimApplication = computerSimApplication;
    }

    public synchronized void setUp() {

        this.setTitle("Computer Sim");
        // Menu components.
        this.menuListener = new MenuListener();
        this.menuBar = new JMenuBar();
        this.menuFile = new JMenu("File");
        this.menuFileOpen = new JMenuItem("Open");
        this.menuFileOpen.addActionListener(menuListener);
        this.menuFileClose = new JMenuItem("Close");
        this.menuFileClose.addActionListener(menuListener);
        this.menuFileClose.setEnabled(false);
        this.menuFileQuit = new JMenuItem("Exit");
        this.menuFileQuit.addActionListener(menuListener);
        this.menuHelp = new JMenu("About");

        this.menuFile.setEnabled(false);
        this.menuHelp.setEnabled(false);
        this.menuFile.add(menuFileOpen);
        this.menuFile.add(menuFileClose);
        this.menuFile.add(menuFileQuit);

        this.menuBar.add(menuFile);
        this.menuBar.add(menuHelp);

        desktopPane = new JDesktopPane();

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        desktopPane.setMinimumSize(new java.awt.Dimension(500, 300));
        desktopPane.setPreferredSize(new java.awt.Dimension(1000, 600));
        desktopPane.setBackground(Color.DARK_GRAY);

        getContentPane().add(desktopPane, java.awt.BorderLayout.CENTER);

        // Stats panel.
        bottomPanel = new JPanel();
        bottomPanel.setLayout(
                new BoxLayout(bottomPanel, BoxLayout.LINE_AXIS));
        bottomPanel.setBorder(
                BorderFactory.createMatteBorder(
                        1, 0, 0, 0,
                        UIManager.getDefaults()
                        .getColor("MenuBar.shadow")));
        getContentPane().add(bottomPanel, java.awt.BorderLayout.SOUTH);

        setJMenuBar(menuBar);
        pack();

    }

    synchronized boolean hasGuiVisualisationAttatched() {
        return guiVisualisation != null;
    }

    synchronized void setGuiVisualisation(final GuiVisualisation guiVisualisation) {
        final GuiVisualisation guiVisualisation_dc = guiVisualisation;
        if (guiVisualisation_dc == null) {
            throw new NullPointerException(
                    "guiVisualisation: " + guiVisualisation_dc);
        }
        if (hasGuiVisualisationAttatched()) {
            throw new IllegalStateException();
        }
        this.guiVisualisation = guiVisualisation_dc;
    }

    synchronized void unsetGuiVisualisation() {
        if (!hasGuiVisualisationAttatched()) {
            throw new IllegalStateException();
        }
        this.guiVisualisation = null;
    }

    private class MenuListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            switch (e.getActionCommand()) {
                case "Open":
                    menuFileClose.setEnabled(true);
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            SimulationModel simulationModel
                                    = new SimCom();
                            computerSimApplication.loadSimulation(
                                    simulationModel);
                            computerSimApplication.resumeSimulation();
                        }
                    }).start();
                    break;
                case "Close":
                    menuFileClose.setEnabled(false);
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            computerSimApplication.unloadSimulation();
                        }
                    }).start();
                    break;
                case "Exit":
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            computerSimApplication.tearDown();
                        }
                    }).start();
                    break;
            }
        }
    }
}
