/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.swing;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.table.DefaultTableModel;
import jjs.cs.control.HookProcessor;
import jjs.cs.control.hooks.CurrentInspectableHook;
import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.HookFileListener;
import jjs.cs.simulation.SimulationModel;
import jjs.cs.simulation.data.Inspectable;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
class GuiInspectableFile extends HookProcessor<CurrentInspectableHook> {

    private final HookFile hookFile;
    private final HookListener hookListener;
    final HashMap<CurrentInspectableHook, SwingInspectable> converted;
    private final JDesktopPane desktopPane;
    private final InsFrameListener insLis;

    public GuiInspectableFile(
            SimulationModel simulationState,
            JDesktopPane desktopPane) {
        super(simulationState, CurrentInspectableHook.class);
        this.hookFile = simulationState.hookFile;
        this.hookListener = new HookListener();
        this.desktopPane = desktopPane;
        this.converted = new HashMap<>();
        this.insLis = new InsFrameListener();
        for (CurrentInspectableHook currentInspectableHook : processorHooks) {
            addConvert(currentInspectableHook);
        }
        this.hookFile.addHookFileListener(
                hookListener, CurrentInspectableHook.class);
    }

    private void addConvert(CurrentInspectableHook hook) {
        if (!converted.containsKey(hook)) {
            final SwingInspectable si = new SwingInspectable(hook.inspectable);
            si.addToDesktop(desktopPane);
            converted.put(hook, si);
        }
    }

    private void removeConvert(CurrentInspectableHook hook) {
        if (converted.containsKey(hook)) {
            final SwingInspectable si = converted.get(hook);
            si.removeFromDesktop(desktopPane);
        }
    }

    @Override
    public void update(long deltaNanoseconds) {
        synchronized (converted) {
            for (SwingInspectable swingInspectable : converted.values()) {
                swingInspectable.update();
            }
        }
    }

    public void dispose() {
        this.hookFile.removeHookFileListener(
                hookListener, CurrentInspectableHook.class);
        for (final SwingInspectable swingInspectable : converted.values()) {
            try {
                EventQueue.invokeAndWait(new Runnable() {

                    @Override
                    public void run() {
                        swingInspectable.removeFromDesktop(desktopPane);
                    }
                });
            } catch (InterruptedException | InvocationTargetException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private class SwingInspectable extends JInternalFrame {

        private static final long serialVersionUID = -1l;

        private final JScrollPane scrollPane;
        private final JTable table;
        private final DefaultTableModel model;
        private final Inspectable inspectable;
        private final Map<String, ?> properties;

        public SwingInspectable(Inspectable inspectable) {
            this.inspectable = inspectable;

            // Initialise self.
            this.setFrameIcon(null);
            this.scrollPane = new JScrollPane();
            this.table = new JTable();
            this.model = new DefaultTableModel(0, 2);
            this.table.setModel(model);
            this.table.setDragEnabled(false);
            this.table.setColumnSelectionAllowed(false);
            this.table.setRowSelectionAllowed(false);
            this.table.setCellSelectionEnabled(false);
            this.table.setDragEnabled(false);
            this.table.setTableHeader(null);
            this.properties = inspectable.getProperties();
            this.setClosable(true);
            this.setIconifiable(true);
            this.setMaximizable(true);
            this.setResizable(true);
            this.setTitle("Inspecting: " + inspectable.getInspectableName());
            this.scrollPane.setViewportView(this.table);
            this.getContentPane().add(this.scrollPane, BorderLayout.CENTER);
            this.pack();

            this.addInternalFrameListener(insLis);
        }

        public void addToDesktop(JDesktopPane desktopPane) {
            this.setVisible(true);
            desktopPane.add(this);
            this.setSize(230, 170);
            desktopPane.setLayer(this, 1);
        }

        public void update() {
            this.inspectable.updateProperties();
            final int rowCount = this.properties.size();
            this.model.setRowCount(rowCount);
            int i = 0;
            for (Map.Entry<String, ?> entry
                    : this.properties.entrySet()) {
                this.model.setValueAt(entry.getKey(), i, 0);
                this.model.setValueAt(entry.getValue(), i, 1);
                i++;
            }
            this.model.fireTableDataChanged();
        }

        public void removeFromDesktop(JDesktopPane desktopPane) {
            this.setVisible(false);
            desktopPane.remove(this);
            desktopPane.revalidate();
        }
    }

    private class HookListener
            implements HookFileListener<CurrentInspectableHook> {

        @Override
        public void hookAdded(CurrentInspectableHook hook) {
            addConvert(hook);
        }

        @Override
        public void hookRemoved(CurrentInspectableHook hook) {
            removeConvert(hook);
        }

    }

    private class InsFrameListener implements InternalFrameListener {

        @Override
        public void internalFrameOpened(InternalFrameEvent e) {
        }

        @Override
        @SuppressWarnings("unchecked")
        public void internalFrameClosing(InternalFrameEvent e) {
            synchronized (converted) {
                // Todo: Make this less bad.
                for (Map.Entry<CurrentInspectableHook, SwingInspectable> entry
                        : converted.entrySet()) {
                    if (entry.getValue().equals((SwingInspectable) e.getInternalFrame())) {
                        converted.remove(entry.getKey());
                        hookFile.removeHook(entry.getKey(), CurrentInspectableHook.class);
                        break;
                    }
                }
            }
        }

        @Override
        public void internalFrameClosed(InternalFrameEvent e) {
        }

        @Override
        public void internalFrameIconified(InternalFrameEvent e) {
        }

        @Override
        public void internalFrameDeiconified(InternalFrameEvent e) {
        }

        @Override
        public void internalFrameActivated(InternalFrameEvent e) {
        }

        @Override
        public void internalFrameDeactivated(InternalFrameEvent e) {
        }
    }
}
