/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.swing;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import jjs.cs.control.hooks.StringInputHook;
import jjs.cs.control.hooks.StringOutputHook;
import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.SimulationModel;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class ConsoleProcessor {

    protected final SimulationModel simulationState;
    private final HookFile hookFile;
    private final JDesktopPane desktopPane;
    private final ConsoleFrame consoleFrame;
    private final LinkedList<String> inputFifo;
    protected final HashSet<StringOutputHook> outputHooks;
    protected final HashSet<StringInputHook> inputHooks;

    public ConsoleProcessor(
            SimulationModel simulationState,
            JDesktopPane desktopPaneR) {
        final SimulationModel simulationState_dc = simulationState;
        if (simulationState_dc == null) {
            throw new NullPointerException(
                    "simulationState: " + simulationState_dc);
        }
        this.simulationState = simulationState_dc;
        this.outputHooks = simulationState.hookFile
                .getHooksOfType(StringOutputHook.class);
        this.inputHooks = simulationState.hookFile
                .getHooksOfType(StringInputHook.class);
        this.hookFile = simulationState.hookFile;
        this.desktopPane = desktopPaneR;
        this.consoleFrame = new ConsoleFrame();
        this.inputFifo = new LinkedList<>();
        try {
            EventQueue.invokeAndWait(new Runnable() {

                @Override
                public void run() {

                    consoleFrame.setVisible(true);
                    desktopPane.add(consoleFrame);
                    consoleFrame.setSize(230, 170);
                    consoleFrame.setBounds(0, 20, 240, 160);
                    desktopPane.setLayer(consoleFrame, 1);
                    desktopPane.revalidate();
                }
            });
        } catch (InterruptedException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }

        this.consoleFrame.sendButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                addToFifo();
            }
        });
        this.consoleFrame.textIn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                addToFifo();
            }
        });
    }

    private void addToFifo() {
        inputFifo.offerFirst(consoleFrame.textIn.getText());
        consoleFrame.textIn.setText("");

    }

    public void update(long deltaNanoseconds) {
        for (StringOutputHook stringOutputHook : outputHooks) {
            if (stringOutputHook.newString.get()) {
                this.consoleFrame.textOut.append(
                        stringOutputHook.stringRef.get() + '\n');
                stringOutputHook.deactivate();
            }
        }
        if (!inputFifo.isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder();
            while (!inputFifo.isEmpty()) {
                stringBuilder.append(inputFifo.removeLast());
            }
            this.consoleFrame.textOut.append(stringBuilder.toString() + '\n');
            for (StringInputHook stringInputHook : inputHooks) {
                stringInputHook.stringRef.set(stringBuilder.toString());
                stringInputHook.activate();
            }
        }
    }

    public void dispose() {
        this.consoleFrame.dispose();
    }

    private class ConsoleFrame extends JInternalFrame {

        private static final long serialVersionUID = -1l;

        private JButton sendButton;
        private JPanel bottomPanel;
        private JScrollPane scrollPane;
        private JTextArea textOut;
        private JTextField textIn;

        /**
         * Creates new form TestConsoleFrame
         */
        public ConsoleFrame() {
            initComponents();
        }

        private void initComponents() {

            scrollPane = new JScrollPane();
            textOut = new JTextArea();
            bottomPanel = new JPanel();
            textIn = new JTextField();
            sendButton = new JButton();

            setIconifiable(true);
            setMaximizable(true);
            setResizable(true);
            setTitle("Console");
            this.setFrameIcon(null);

            scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

            textOut.setEditable(false);
            textOut.setColumns(20);
            textOut.setLineWrap(true);
            textOut.setRows(5);
            scrollPane.setViewportView(textOut);

            getContentPane().add(scrollPane, BorderLayout.CENTER);

            bottomPanel.setLayout(new BorderLayout());
            bottomPanel.add(textIn, BorderLayout.CENTER);

            sendButton.setText("Input");
            bottomPanel.add(sendButton, BorderLayout.LINE_END);

            getContentPane().add(bottomPanel, BorderLayout.PAGE_END);

            pack();
        }
    }
}
