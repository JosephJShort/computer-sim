/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.joglview.swing;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import jjs.cs.control.InputState;
import jjs.cs.control.hooks.CurrentInspectableHook;
import jjs.cs.joglview.LightweightOpenGlCanvas;
import jjs.cs.joglview.opengl.OpenGlRenderListener;
import jjs.cs.simulation.SimulationModel;
import jjs.cs.simulation.data.Entity;
import jjs.cs.simulation.data.Inspectable;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class GuiVisualisation {

    private static enum State {

        NEW,
        ATTACHED,
        OLD
    }

    private final AtomicReference<State> state;
    private final SimulationModel simulationModel;
    private final InputState inputState;

    // For ATTATCHED.
    private SwingGui swingGui;
    private JDesktopPane desktopPane;
    private JPanel statsPanel;
    private GuiInspectableFile inspectableFile;
    private ConsoleProcessor consoleProcessor;
    private JLabel labelFps;
    private JLabel labelTime;
    private JLabel labelInspectables;
    private JSeparator sep1;
    private JSeparator sep2;
    private TestHtmlViewer htmlViewer;

    private Set<CurrentInspectableHook> insHooks;

    private GLProfile glprofile;
    private GLCapabilities glcapabilities;
    private LightweightOpenGlCanvas openGlCanvas;
    private OpenGlRenderListener renderListener;

    private CanvasResizeListener canvasResizeListener;
    private SwingInputEventListener inputListener;
    private ContextMenuListener contextMenuListener;

    public GuiVisualisation(
            final SimulationModel simulationModel,
            final InputState inputState) {
        this.simulationModel = simulationModel;
        this.inputState = inputState;
        this.state = new AtomicReference<>(State.NEW);
    }

    public synchronized void attatch(final SwingGui swingGui) {

        // Parameter validation.
        this.swingGui = swingGui;
        if (this.swingGui == null) {
            throw new NullPointerException("swingGui: " + this.swingGui);
        }
        // State validation.
        final State stateRef = state.get();
        if (stateRef != State.NEW) {
            throw new IllegalStateException(
                    "Can't attach, has already been used.");
        } else if (this.swingGui.hasGuiVisualisationAttatched()) {
            throw new IllegalStateException(
                    "Can't attach, swingGui already has one attached.");
        }

        // Attatch this visualisation to the GUI.
        swingGui.setTitle("Computer Sim - A RISC computer");
        this.desktopPane = swingGui.desktopPane;
        this.statsPanel = swingGui.bottomPanel;
        this.insHooks = this.simulationModel.hookFile.getHooksOfType(
                CurrentInspectableHook.class);

        // Create OpenGL resources.
        this.glprofile = GLProfile.getDefault();
        this.glcapabilities = new GLCapabilities(glprofile);
        this.openGlCanvas = new LightweightOpenGlCanvas(glcapabilities);
        this.renderListener = new OpenGlRenderListener(simulationModel);
        this.openGlCanvas.setBackground(Color.DARK_GRAY);
        try {
            EventQueue.invokeAndWait(new Runnable() {
                @Override
                public void run() {

                    inputListener = new SwingInputEventListener(simulationModel);
                    openGlCanvas.addKeyListener(inputListener);
                    openGlCanvas.addMouseListener(inputListener);
                    openGlCanvas.addMouseMotionListener(inputListener);
                    openGlCanvas.addMouseWheelListener(inputListener);
                    openGlCanvas.setLayout(null);
                    openGlCanvas.setVisible(true);
                    desktopPane.add(openGlCanvas);

                    canvasResizeListener
                            = new CanvasResizeListener(openGlCanvas);
                    desktopPane.addComponentListener(canvasResizeListener);
                    desktopPane.validate();
                    //  java.awt.Container
                    //  lightweightCanvas.
                    openGlCanvas.setSize(desktopPane.getSize());
                    contextMenuListener = new ContextMenuListener();
                    openGlCanvas.addMouseListener(contextMenuListener);
                    swingGui.validate();
                    openGlCanvas.addGLEventListener(renderListener);

                    // Stats bar.
                    statsPanel.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 0, 0, javax.swing.UIManager.getDefaults().getColor("MenuBar.shadow")));
                    statsPanel.setLayout(new javax.swing.BoxLayout(statsPanel, javax.swing.BoxLayout.LINE_AXIS));

                    labelFps = new JLabel();
                    labelFps.setFont(new java.awt.Font("Tahoma", 0, 10));
                    labelFps.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
                    statsPanel.add(labelFps);

                    labelTime = new JLabel();
                    labelTime.setFont(new java.awt.Font("Tahoma", 0, 10));
                    labelTime.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
                    statsPanel.add(labelTime);

                    sep1 = new JSeparator();
                    sep1.setOrientation(javax.swing.SwingConstants.VERTICAL);
                    statsPanel.add(sep1);

                    labelInspectables = new JLabel();
                    labelInspectables.setFont(new java.awt.Font("Tahoma", 0, 10));
                    labelInspectables.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
                    statsPanel.add(labelInspectables);

                    sep2 = new JSeparator();
                    sep2.setOrientation(javax.swing.SwingConstants.VERTICAL);
                    statsPanel.add(sep2);

                    statsPanel.revalidate();
                }
            });
        } catch (InterruptedException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }

        this.htmlViewer = new TestHtmlViewer();
        this.htmlViewer.setBounds(0, 180, 240, 400);
        this.htmlViewer.setVisible(true);
        this.htmlViewer.setTitle("Welcome");
        this.desktopPane.add(htmlViewer);
        this.desktopPane.setLayer(htmlViewer, 1);

        this.inspectableFile
                = new GuiInspectableFile(simulationModel, desktopPane);
        this.consoleProcessor
                = new ConsoleProcessor(simulationModel, desktopPane);

        // State transition.
        this.state.set(State.ATTACHED);
    }

    public synchronized void detach() {

        // State validation.
        final State stateRef = state.get();
        if (stateRef != State.ATTACHED) {
            throw new IllegalStateException("Not attached.");
        }

        // Detach this visualisation to the GUI.
        swingGui.setTitle("Computer Sim");
        this.inspectableFile.dispose();
        this.htmlViewer.dispose();
        this.consoleProcessor.dispose();
        try {
            EventQueue.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    desktopPane.removeComponentListener(canvasResizeListener);
                    openGlCanvas.setVisible(false);
                    desktopPane.remove(openGlCanvas);
                    statsPanel.removeAll();
                    statsPanel.validate();
                    desktopPane.validate();
                    swingGui.validate();
                    desktopPane.repaint(50L);
                    openGlCanvas.disposeGLEventListener(renderListener, true);
                    openGlCanvas.destroy();
                }
            });
        } catch (InterruptedException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }

        this.swingGui = null;
        this.desktopPane = null;
        this.statsPanel = null;
        this.inspectableFile = null;
        this.htmlViewer = null;
        this.consoleProcessor = null;

        this.glprofile = null;
        this.glcapabilities = null;
        this.openGlCanvas = null;
        this.renderListener = null;

        this.canvasResizeListener = null;
        this.inputListener = null;
        this.contextMenuListener = null;

        // State transition.
        this.state.set(State.OLD);
    }

    public void update(
            final long deltaNanoseconds, final double interpolation) {
        this.renderListener.interpolation = (float) interpolation;
        this.openGlCanvas.display();
        this.inspectableFile.update(deltaNanoseconds);
        this.consoleProcessor.update(deltaNanoseconds);
        this.labelFps.setText(String.format("FPS: % 2.2f",
                this.simulationModel.runtimeStatistics.ewmaFramesPerSecond));
        this.labelTime.setText(String.format("Time scale: % 2.2f",
                this.simulationModel.runtimeStatistics.ewmaTimescale));
        this.labelInspectables.setText("Inspecting: "
                + this.simulationModel.hookFile.getHooksOfType(
                        CurrentInspectableHook.class).size());
    }

    private static class CanvasResizeListener implements ComponentListener {

        private final LightweightOpenGlCanvas canvas;

        public CanvasResizeListener(LightweightOpenGlCanvas canvas) {
            this.canvas = canvas;
        }

        @Override
        public void componentResized(ComponentEvent e) {
            canvas.setSize(e.getComponent().getSize());
        }

        @Override
        public void componentMoved(ComponentEvent e) {
        }

        @Override
        public void componentShown(ComponentEvent e) {
        }

        @Override
        public void componentHidden(ComponentEvent e) {
        }

    }

    private class ContextMenuListener extends MouseAdapter {

        private JPopupMenu jPopupMenu;

        public ContextMenuListener() {
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (e.isPopupTrigger()) {
                final Entity entity
                        = simulationModel.picking.hovered;
                final Inspectable inspectable
                        = simulationModel.picking.hoveredInspectable;
                if (entity != null && inspectable != null) {
                    this.jPopupMenu = new JPopupMenu();
                    final JMenuItem addInspectableButton
                            = new JMenuItem("Inspect: "
                                    + inspectable.getInspectableName());
                    addInspectableButton.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            simulationModel.hookFile.addHook(
                                    new CurrentInspectableHook(
                                            inspectable,
                                            simulationModel.hookFile.newId()),
                                    CurrentInspectableHook.class);
                        }
                    });
                    this.jPopupMenu.add(addInspectableButton);
                    jPopupMenu.show(e.getComponent(),
                            e.getX(), e.getY());
                }
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            mousePressed(e);
        }

    }
}
