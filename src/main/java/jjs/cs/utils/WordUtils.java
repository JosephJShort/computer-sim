/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.utils;

import java.nio.ByteBuffer;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class WordUtils {

    // Nonistantiable.
    private WordUtils() {
    }

    public static byte[] toBytes(final long word) {
        return ByteBuffer.allocate(8).putLong(word).array();
    }

    public static long fromBytes(final byte[] bytes) {
        if (bytes.length != 8) {
            throw new IllegalArgumentException(
                    "bytes must be 8 byte long. bytes.length: " + bytes.length);
        }
        return ByteBuffer.wrap(bytes).getLong();
    }

    public static long longLowerHalf(final long word) {
        return longField(word, 0, 31);
    }

    public static long longUpperHalf(final long word) {
        return longField(word, 32, 63);
    }

    public static long longField(long word, int start, int end) {
        return (word << (63 - end)) >>> (start + 63 - end);
    }

    public static int intField(int word, int start, int end) {
        return (word << (31 - end)) >>> (start + 31 - end);
    }

    public static int intLowerHalf(int word) {
        return intField(word, 0, 15);
    }

    public static int intUpperHalf(int word) {
        return intField(word, 16, 31);
    }

    public static byte[] intToBytes(int word) {
        return ByteBuffer.allocate(4).putInt(word).array();
    }

    public static long signedField(long word, int start, int end) {
        return (word << (63 - end)) >> (start + 63 - end);
    }
}
