/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simulation;

import jjs.cs.simulation.data.Entity;
import jjs.cs.simulation.data.Inspectable;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class SimulationModel {

    public final HookFile hookFile;
    public final Mouse mouse;
    public final Display display;
    public final Camera camera;
    public final RuntimeStatistics runtimeStatistics;
    public final Inspection inspection;
    public final Gui gui;
    public final Picking picking;

    public SimulationModel(HookFile hookFile) {
        this.hookFile = hookFile;
        this.mouse = new Mouse();
        this.display = new Display();
        this.camera = new Camera();
        this.runtimeStatistics = new RuntimeStatistics();
        this.inspection = new Inspection();
        this.gui = new Gui();
        this.picking = new Picking();
    }

    public static class Mouse {

        public final Vector3f rayOrigin = new Vector3f();
        public final Vector3f rayDestination = new Vector3f();
        public int posX = 0;
        public int posY = 0;
        public int deltaPosX = 0;
        public int deltaPosY = 0;
        public boolean leftButtonDown = false;
        public boolean leftButtonChanged = false;
        public double dWheel = 0;
    }

    public static class Display {

        public int width;
        public int height;
    }

    public static class Camera {

        public float posX = 0;
        public float posY = 0;
        public float posZ = -5;
        public boolean grabbed = false;
    }

    public static class RuntimeStatistics {

        public double ewmaFramesPerSecond = 0;
        public double ewmaTimescale = 0;
    }

    public static class Inspection {

        public Inspectable inspectable = null;
        public boolean inspectableChanged = false;
    }

    public static class Gui {

        public boolean mouseOverSomething = false;
    }

    public static class Picking {

        public RenderHook selectionBox = null;
        public RenderHook hoverBox = null;
        public boolean somethingHovered = false;
        public boolean somethingSelected = false;
        public Entity selection = null;
        public Entity hovered = null;
        public Inspectable hoveredInspectable = null;
    }
}
