/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simulation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class HookFile {

    private final HashMap<
            Class<? extends Hook>, HashSet<? extends Hook>> typeHooks;
    private final transient HashMap<
            Class<? extends Hook>, HashSet<
            HookFileListener<?>>> typeHookFileListeners;
    private final AtomicInteger nextId;

    public HookFile() {
        this.typeHooks = new HashMap<>();
        this.typeHookFileListeners = new HashMap<>();
        this.nextId = new AtomicInteger(1);
    }

    @SuppressWarnings("unchecked")
    public <T extends Hook> boolean addHook(
            final T hook, final Class<T> type) {

        // Parameter validation using defensive pointer copies.
        final T hook_dc = hook;
        final Class<T> type_dc = type;
        if (hook_dc == null || type_dc == null) {
            throw new NullPointerException(
                    "hook: " + hook_dc + ", type: " + type_dc);
        }

        // Return value.
        final boolean added;

        // Create or retrieve the correct hash set.
        final HashSet<T> hooks;
        if (!typeHooks.containsKey(type_dc)) {
            hooks = new HashSet<>();
            typeHooks.put(type_dc, hooks);
        } else {
            hooks = (HashSet<T>) typeHooks.get(type_dc);
        }

        // Add the hook.
        added = hooks.add(hook_dc);

        // Invoke methods on the HookFileListeners if necessary.
        if (added && typeHookFileListeners.containsKey(type_dc)) {
            for (HookFileListener<?> hookFileListener
                    : typeHookFileListeners.get(type_dc)) {
                ((HookFileListener<T>) hookFileListener).hookAdded(hook_dc);
            }
        }

        return added;
    }

    @SuppressWarnings("unchecked")
    public <T extends Hook> boolean removeHook(
            final T hook, final Class<T> type) {

        // Parameter validation using defensive pointer copies.
        final T hook_dc = hook;
        final Class<T> type_dc = type;
        if (hook_dc == null || type_dc == null) {
            throw new NullPointerException(
                    "hook_dc: " + hook_dc + ", type_dc: " + type_dc);
        }

        // Remove the hook if present.
        final boolean removed = typeHooks.containsKey(type_dc)
                ? typeHooks.get(type_dc).remove(hook_dc) : false;

        // Invoke methods on the HookFileListeners if necessary.
        if (removed && typeHookFileListeners.containsKey(type_dc)) {
            for (HookFileListener<?> hookFileListener
                    : typeHookFileListeners.get(type_dc)) {
                ((HookFileListener<T>) hookFileListener).hookRemoved(hook_dc);
            }
        }

        return removed;
    }

    @SuppressWarnings("unchecked")
    public <T extends Hook> HashSet<T> getHooksOfType(final Class<T> type) {

        // Parameter validation using defensive pointer copies.
        final Class<T> type_dc = type;
        if (type_dc == null) {
            throw new NullPointerException("type_dc: " + type_dc);
        }

        // Retrieve set if it exists; other or create empty set.
        final HashSet<T> hooks;
        if (typeHooks.containsKey(type_dc)) {
            hooks = (HashSet<T>) typeHooks.get(type_dc);
        } else {
            hooks = new HashSet<>();
        }

        return hooks;
    }

    public <T extends Hook> boolean addHookFileListener(
            final HookFileListener<T> hookFileListener,
            final Class<T> hookType) {

        // Parameter validation using defensive pointer copies.
        final HookFileListener<T> hookFileListener_dc = hookFileListener;
        final Class<T> hookType_dc = hookType;
        if (hookFileListener_dc == null || hookType_dc == null) {
            throw new NullPointerException(
                    "hookFileListener: " + hookFileListener_dc
                    + ", hookType: " + hookType_dc);
        }

        // Create or retrieve the correct list.
        final HashSet<HookFileListener<?>> listeners;
        if (!typeHookFileListeners.containsKey(hookType_dc)) {
            listeners = new HashSet<>();
            typeHookFileListeners.put(hookType_dc, listeners);
        } else {
            listeners = typeHookFileListeners.get(hookType_dc);
        }

        // Add the passed listener to the list if it doesnt already exist.
        return listeners.add(hookFileListener_dc);
    }

    public <T extends Hook> boolean removeHookFileListener(
            final HookFileListener<T> hookFileListener,
            final Class<T> hookType) {

        // Parameter validation using defensive pointer copies.
        final HookFileListener<T> hookFileListener_dc = hookFileListener;
        final Class<T> hookType_dc = hookType;
        if (hookFileListener_dc == null || hookType_dc == null) {
            throw new NullPointerException(
                    "hookFileListener: " + hookFileListener_dc
                    + ", hookType: " + hookType_dc);
        }

        // Remove the passed listener from the list if it exists.
        return typeHookFileListeners.containsKey(hookType_dc)
                ? typeHookFileListeners.get(hookType_dc)
                .remove(hookFileListener_dc)
                : false;
    }

    public int newId() {
        return nextId.getAndIncrement();
    }

    @Override
    public String toString() {
        return "HookFile{" + "typeHooks=" + typeHooks
                + ", typeHookFileListeners=" + typeHookFileListeners + '}';
    }

}
