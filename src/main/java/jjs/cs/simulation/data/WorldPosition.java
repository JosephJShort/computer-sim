/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simulation.data;

import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class WorldPosition {

    public final Vector3f position;
    public final Vector3f angle;
    public final Vector3f scale;

    public WorldPosition(
            float positionX,
            float positionY,
            float positionZ,
            float rotationX,
            float rotationY,
            float rotationZ,
            float scaleX,
            float scaleY,
            float scaleZ) {
        this.position = new Vector3f(positionX, positionY, positionZ);
        this.angle = new Vector3f(rotationX, rotationY, rotationZ);
        this.scale = new Vector3f(scaleX, scaleY, scaleZ);
    }

    public WorldPosition(Vector3f position, Vector3f angle, Vector3f scale) {
        this.position = position;
        this.angle = angle;
        this.scale = scale;
    }

    @Override
    public String toString() {
        return "Position{"
                + "position=" + position
                + ", angle=" + angle
                + ", scale=" + scale + '}';
    }
}
