/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simulation.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Immutable.
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class RenderModel {

    public final List<Vertex> vertices;
    public final List<Integer> vertexIndices;
    public final String textureName;
    public final boolean wireframe;

    public RenderModel(
            final List<Vertex> vertices,
            final List<Integer> vertexIndices,
            final String textureName,
            final boolean wireframe) {
        final List<Vertex> vertices_dc = vertices;
        final List<Integer> vertexIndices_dc = vertexIndices;
        final String textureName_dc = textureName;

        // Null check.
        {
            final ArrayList<String> nullParameters = new ArrayList<>();
            if (vertices_dc == null) {
                nullParameters.add("List<Vertex> vertices");
            }
            if (vertexIndices_dc == null) {
                nullParameters.add("List<Integer> vertexIndices");
            }
            if (textureName_dc == null) {
                nullParameters.add("String textureName");
            }
            if (!nullParameters.isEmpty()) {
                throw new NullPointerException(
                        "The following parameters were illegally null: "
                        + nullParameters + ".");
            }
        }
        if (!validate(vertices, vertexIndices)) {
            throw new IllegalArgumentException("Invalid combination of"
                    + " List<Vertex> vertices and List<Integer> vertexIndices"
                    + " passed. Vertices: " + vertices_dc + ", vertexIndices: "
                    + vertexIndices_dc + ".");
        }
        this.vertices = Collections.unmodifiableList(
                new ArrayList<>(vertices_dc));
        this.vertexIndices = Collections.unmodifiableList(
                new ArrayList<>(vertexIndices_dc));
        this.textureName = textureName_dc;
        this.wireframe = wireframe;
    }

    private static boolean validate(
            final List<Vertex> vertices, final List<Integer> vertexIndices) {
        final boolean isValid;
        if (vertices.isEmpty()
                || vertexIndices.isEmpty()
                || vertexIndices.size() % 3 != 0) {
            isValid = false;
        } else {
            boolean foundInvalidIndex = false;
            {
                final int verticesSize = vertices.size();
                final Iterator<Integer> it = vertexIndices.iterator();
                while (!foundInvalidIndex && it.hasNext()) {
                    final int next = it.next();
                    if (next < 0 || next >= verticesSize) {
                        foundInvalidIndex = true;
                    }
                }
            }
            isValid = !foundInvalidIndex;
        }
        return isValid;
    }

    public static class Vertex {

        public final float positionX;
        public final float positionY;
        public final float positionZ;
        public final float red;
        public final float green;
        public final float blue;
        public final float alpha;
        public final float texS;
        public final float texT;

        public Vertex(
                float posX,
                float posY,
                float posZ,
                float red,
                float green,
                float blue,
                float alpha,
                float texS,
                float texT) {
            if (red < 0 || red > 1
                    || green < 0 || green > 1
                    || blue < 0 || blue > 1
                    || alpha < 0 || alpha > 1) {
                throw new IllegalArgumentException("red, green, blue and"
                        + " alpha must all be between 0 and 1. Red: "
                        + red + ", green: " + green + ", blue: " + blue
                        + ", alpha: " + alpha + ".");
            }
            this.positionX = posX;
            this.positionY = posY;
            this.positionZ = posZ;
            this.red = red;
            this.green = green;
            this.blue = blue;
            this.alpha = alpha;
            this.texS = texS;
            this.texT = texT;
        }
    }
}
