/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simulation.data;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class BoxRenderModel extends RenderModel {

    private static final List<RenderModel.Vertex> vertices;
    private static final List<Integer> vertexIndices;

    static {
        final float pos = +0.5f;
        final float neg = -0.5f;
        vertices = Arrays.asList(
                // Face 1 (front).
                new RenderModel.Vertex(neg, neg, pos, 1f, 1f, 1f, 1f, 0f, .8f),
                new RenderModel.Vertex(pos, neg, pos, 1f, 1f, 1f, 1f, 1f, .8f),
                new RenderModel.Vertex(pos, pos, pos, 1f, 1f, 1f, 1f, 1f, 0f),
                new RenderModel.Vertex(neg, pos, pos, 1f, 1f, 1f, 1f, 0f, 0f),
                // Face 2 (top).
                new RenderModel.Vertex(neg, pos, pos, 1f, 1f, 1f, 1f, 1f, .8f),
                new RenderModel.Vertex(pos, pos, pos, 1f, 1f, 1f, 1f, 0f, .8f),
                new RenderModel.Vertex(pos, pos, neg, 1f, 1f, 1f, 1f, 0f, 1f),
                new RenderModel.Vertex(neg, pos, neg, 1f, 1f, 1f, 1f, 1f, 1f),
                // Face 3 (back).
                new RenderModel.Vertex(neg, pos, neg, 1f, 1f, 1f, 1f, 1f, 0f),
                new RenderModel.Vertex(neg, neg, neg, 1f, 1f, 1f, 1f, 1f, .8f),
                new RenderModel.Vertex(pos, neg, neg, 1f, 1f, 1f, 1f, 0f, .8f),
                new RenderModel.Vertex(pos, pos, neg, 1f, 1f, 1f, 1f, 0f, 0f),
                // Face 4 (bottom).
                new RenderModel.Vertex(neg, neg, neg, 1f, 1f, 1f, 1f, 1f, 1f),
                new RenderModel.Vertex(pos, neg, neg, 1f, 1f, 1f, 1f, 0f, 1f),
                new RenderModel.Vertex(pos, neg, pos, 1f, 1f, 1f, 1f, 0f, .8f),
                new RenderModel.Vertex(neg, neg, pos, 1f, 1f, 1f, 1f, 1f, .8f),
                // Face 5 (left).
                new RenderModel.Vertex(neg, neg, neg, 1f, 1f, 1f, 1f, 0f, 1f),
                new RenderModel.Vertex(neg, neg, pos, 1f, 1f, 1f, 1f, 0f, .8f),
                new RenderModel.Vertex(neg, pos, pos, 1f, 1f, 1f, 1f, 1f, .8f),
                new RenderModel.Vertex(neg, pos, neg, 1f, 1f, 1f, 1f, 1f, 1f),
                // Face 6 (right).
                new RenderModel.Vertex(pos, neg, pos, 1f, 1f, 1f, 1f, 0f, .8f),
                new RenderModel.Vertex(pos, neg, neg, 1f, 1f, 1f, 1f, 0f, 1f),
                new RenderModel.Vertex(pos, pos, neg, 1f, 1f, 1f, 1f, 1f, 1f),
                new RenderModel.Vertex(pos, pos, pos, 1f, 1f, 1f, 1f, 1f, .8f)
        );

        vertexIndices = Arrays.asList(
                // Face 1 (front).
                0, 1, 2,
                2, 3, 0,
                // Face 2 (top).
                4, 5, 6,
                6, 7, 4,
                // Face 3 (back).
                8, 9, 10,
                10, 11, 8,
                // Face 4 (bottom).
                12, 13, 14,
                14, 15, 12,
                // Face 5 (left).
                16, 17, 18,
                18, 19, 16,
                // Face 6 (right).
                20, 21, 22,
                22, 23, 20);
    }

    public BoxRenderModel(String textureName, boolean wireframe) {
        super(vertices, vertexIndices, textureName, wireframe);
    }

}
