/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.control;

import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class InputState {

    public final Mouse mouse;
    public final Display display;
    public final Camera camera;

    public InputState() {
        this.mouse = new Mouse();
        this.display = new Display();
        this.camera = new Camera();
    }

    public static class Mouse {

        public final Vector3f rayOrigin = new Vector3f();
        public final Vector3f rayDestination = new Vector3f();
        public int posX = 0;
        public int posY = 0;
        public int deltaPosX = 0;
        public int deltaPosY = 0;
        public boolean leftButtonDown = false;
        public boolean leftButtonChanged = false;
        public int dWheel = 0;
    }

    public static class Display {

        public int width;
        public int height;
    }

    public static class Camera {

        public float posX = 0;
        public float posY = 0;
        public float posZ = -5;
        public boolean grabbed = false;
    }
}
