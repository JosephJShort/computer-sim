/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.control;

import java.util.ArrayList;
import jjs.cs.simulation.Hook;
import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.RenderHook;
import jjs.cs.simulation.SimulationModel;
import jjs.cs.simulation.data.BoxRenderModel;
import jjs.cs.simulation.data.Entity;
import jjs.cs.simulation.data.Inspectable;
import jjs.cs.simulation.data.WorldPosition;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class PickingProcessor extends HookProcessor<PickingProcessor.H> {

    private final HookFile hookFile;
    private final SimulationModel.Picking picking;
    private final SimulationModel.Mouse mouse;

    private final WorldPosition selectionBoxPosition;
    private final WorldPosition hoverBoxPosition;

    private final ArrayList<H> hoverWorkingList;
    private H hoverWorkingReference;

    public PickingProcessor(
            SimulationModel simulationState) {
        super(simulationState, H.class);
        this.hookFile = simulationState.hookFile;
        this.picking = simulationState.picking;
        this.mouse = simulationState.mouse;

        this.hoverWorkingList = new ArrayList<>();

        // Set up sprites.
        selectionBoxPosition = new WorldPosition(
                new Vector3f(0, 0, 0),
                new Vector3f(0, 0, 0),
                new Vector3f(0, 0, 0));
        hoverBoxPosition = new WorldPosition(
                new Vector3f(0, 0, 0),
                new Vector3f(0, 0, 0),
                new Vector3f(0, 0, 0));
        picking.selectionBox = new RenderHook(
                hookFile.newId(),
                selectionBoxPosition,
                new BoxRenderModel("white.png", true));
        picking.hoverBox = new RenderHook(
                hookFile.newId(),
                hoverBoxPosition,
                new BoxRenderModel("white.png", true));
        hookFile.addHook(picking.selectionBox, RenderHook.class);
        hookFile.addHook(picking.hoverBox, RenderHook.class);
    }

    @Override
    public void update(long deltaNanoseconds) {
        if (!simulationState.gui.mouseOverSomething) {
            findHovers();
            updateHover();
            updateSelection();
        }
    }

    private void findHovers() {
        final Vector3f pickingRayOrigin = mouse.rayOrigin;
        final Vector3f pickingRayDirection = mouse.rayDestination;
        final Vector3f pickingRayDirectionFractional = new Vector3f(
                1.0f / pickingRayDirection.x,
                1.0f / pickingRayDirection.y,
                1.0f / pickingRayDirection.z);

        // Draw hover box over closest entity.
        hoverWorkingList.clear();
        hoverWorkingReference = null;
        int i = 0;
        float smallestT = Float.MAX_VALUE;
        int smallestTindex = -1;

        for (H h : processorHooks) {
            boolean intersects;

            // Todo this doesn't account for rotation.
            Vector3f aabbMin = new Vector3f(
                    -h.worldPosition.scale.x / 2f,
                    -h.worldPosition.scale.y / 2f,
                    -h.worldPosition.scale.z / 2f);
            Vector3f aabbMax = new Vector3f(
                    h.worldPosition.scale.x / 2f,
                    h.worldPosition.scale.y / 2f,
                    h.worldPosition.scale.z / 2f);
            float t1 = (h.worldPosition.position.x
                    + aabbMin.x - pickingRayOrigin.x)
                    * pickingRayDirectionFractional.x;
            float t2 = (h.worldPosition.position.x
                    + aabbMax.x - pickingRayOrigin.x)
                    * pickingRayDirectionFractional.x;
            float t3 = (h.worldPosition.position.y
                    + aabbMin.y - pickingRayOrigin.y)
                    * pickingRayDirectionFractional.y;
            float t4 = (h.worldPosition.position.y
                    + aabbMax.y - pickingRayOrigin.y)
                    * pickingRayDirectionFractional.y;
            float t5 = (h.worldPosition.position.z
                    + aabbMin.z - pickingRayOrigin.z)
                    * pickingRayDirectionFractional.z;
            float t6 = (h.worldPosition.position.z
                    + aabbMax.z - pickingRayOrigin.z)
                    * pickingRayDirectionFractional.z;
            float tmin = Math.max(Math.max(
                    Math.min(t1, t2),
                    Math.min(t3, t4)),
                    Math.min(t5, t6));
            float tmax = Math.min(Math.min(
                    Math.max(t1, t2),
                    Math.max(t3, t4)),
                    Math.max(t5, t6));

            // Set distance t.
            float t;
            if (tmax < 0) {
                t = tmax;
                intersects = false;
            } else if (tmin > tmax) {
                t = tmax;
                intersects = false;
            } else {
                t = tmin;
                intersects = true;
            }

            // Update working list.
            if (intersects) {
                if (t < smallestT) {
                    smallestT = t;
                    smallestTindex = i;
                }
                hoverWorkingList.add(h);
                i++;
            }
        }

        // Update working reference.
        hoverWorkingReference = smallestTindex != -1
                ? hoverWorkingList.get(smallestTindex) : null;
    }

    private void updateHover() {
        if (hoverWorkingReference != null) {
            picking.somethingHovered = true;
            picking.hovered = hoverWorkingReference.entity;
            picking.hoveredInspectable = hoverWorkingReference.inspectable;
            hoverBoxPosition.position.x
                    = hoverWorkingReference.worldPosition.position.x;
            hoverBoxPosition.position.y
                    = hoverWorkingReference.worldPosition.position.y;
            hoverBoxPosition.position.z
                    = hoverWorkingReference.worldPosition.position.z;
            hoverBoxPosition.scale.x
                    = hoverWorkingReference.worldPosition.scale.x + 0.1f;
            hoverBoxPosition.scale.y
                    = hoverWorkingReference.worldPosition.scale.y + 0.1f;
            hoverBoxPosition.scale.z
                    = hoverWorkingReference.worldPosition.scale.z + 0.1f;
            hoverBoxPosition.angle.x
                    = hoverWorkingReference.worldPosition.angle.x;
            hoverBoxPosition.angle.y
                    = hoverWorkingReference.worldPosition.angle.y;
            hoverBoxPosition.angle.z
                    = hoverWorkingReference.worldPosition.angle.z;
        } else {
            picking.somethingHovered = false;
            picking.hovered = null;
            picking.hoveredInspectable = null;
            hoverBoxPosition.position.x = 0f;
            hoverBoxPosition.position.y = 0f;
            hoverBoxPosition.position.z = 0f;
            hoverBoxPosition.scale.x = 0f;
            hoverBoxPosition.scale.y = 0f;
            hoverBoxPosition.scale.z = 0f;
            hoverBoxPosition.angle.x = 0f;
            hoverBoxPosition.angle.y = 0f;
            hoverBoxPosition.angle.z = 0f;
        }
    }

    private void updateSelection() {
        final boolean click
                = mouse.leftButtonDown;
        if (click) {
            if (hoverWorkingReference == null) {

                // Clicked on nothing.
                picking.selection = null;
                picking.somethingSelected = false;
                selectionBoxPosition.position.x = 0f;
                selectionBoxPosition.position.y = 0f;
                selectionBoxPosition.position.z = 0f;
                selectionBoxPosition.scale.x = 0f;
                selectionBoxPosition.scale.y = 0f;
                selectionBoxPosition.scale.z = 0f;
                selectionBoxPosition.angle.x = 0f;
                selectionBoxPosition.angle.y = 0f;
                selectionBoxPosition.angle.z = 0f;
            } else if (hoverWorkingReference.entity != picking.selection) {

                // Clicked on a different entity.
                picking.selection = hoverWorkingReference.entity;
                if (hoverWorkingReference.inspectable != null) {
                    simulationState.inspection.inspectable
                            = hoverWorkingReference.inspectable;
                    simulationState.inspection.inspectableChanged = true;
                }
                picking.somethingSelected = true;
                selectionBoxPosition.position.x
                        = hoverWorkingReference.worldPosition.position.x;
                selectionBoxPosition.position.y
                        = hoverWorkingReference.worldPosition.position.y;
                selectionBoxPosition.position.z
                        = hoverWorkingReference.worldPosition.position.z;
                selectionBoxPosition.scale.x
                        = hoverWorkingReference.worldPosition.scale.x + 0.1f;
                selectionBoxPosition.scale.y
                        = hoverWorkingReference.worldPosition.scale.y + 0.1f;
                selectionBoxPosition.scale.z
                        = hoverWorkingReference.worldPosition.scale.z + 0.1f;
                selectionBoxPosition.angle.x
                        = hoverWorkingReference.worldPosition.angle.x;
                selectionBoxPosition.angle.y
                        = hoverWorkingReference.worldPosition.angle.y;
                selectionBoxPosition.angle.z
                        = hoverWorkingReference.worldPosition.angle.z;
            }
        }
    }

    public static class H extends Hook {

        private final Entity entity;
        private final WorldPosition worldPosition;
        private final Inspectable inspectable;

        public H(
                int id,
                Entity entity,
                WorldPosition worldPosition,
                Inspectable inspectable) {
            super(id);
            this.entity = entity;
            this.worldPosition = worldPosition;
            this.inspectable = inspectable;
        }

        public H(int id, Entity entity, WorldPosition worldPosition) {
            this(id, entity, worldPosition, null);
        }
    }
}
