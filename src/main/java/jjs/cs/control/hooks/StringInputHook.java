/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.control.hooks;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.simulation.Hook;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class StringInputHook extends Hook {

    private final AtomicBoolean active;
    public final AtomicReference<String> stringRef;

    public StringInputHook(int id, AtomicReference<String> stringRef) {
        super(id);
        this.stringRef = stringRef;
        this.active = new AtomicBoolean(false);
    }

    public boolean isActive() {
        return this.active.get();
    }

    public void activate() {
        this.active.set(true);
    }

    public void deactivate() {
        this.active.set(false);
    }
}
