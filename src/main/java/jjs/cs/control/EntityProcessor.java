/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.control;

import jjs.cs.simulation.Hook;
import jjs.cs.simulation.SimulationModel;
import jjs.cs.simulation.data.Entity;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class EntityProcessor extends HookProcessor<EntityProcessor.EntityHook> {

    public EntityProcessor(SimulationModel simulationState) {
        super(simulationState, EntityHook.class);
    }

    @Override
    public void update(long deltaNanoseconds) {
        for (EntityHook entHook : processorHooks) {
            entHook.e.observe();
        }
        for (EntityHook entHook : processorHooks) {
            entHook.e.update(deltaNanoseconds);
        }
    }

    public static class EntityHook extends Hook {

        private final Entity e;

        public EntityHook(int id, Entity e) {
            super(id);
            this.e = e;
        }
    }
}
