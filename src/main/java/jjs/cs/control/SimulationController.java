/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.control;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.simulation.SimulationModel;
import jjs.cs.view.SimulationView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class SimulationController {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(SimulationController.class);

    private static enum State {

        NEW,
        IDLE,
        SIMULATION_SUSPENDED,
        SIMULATION_RUNNING,
        DISPOSED
    }

    final long UPDATES_PER_SECOND = 50;
    final long FRAMES_PER_SECOND = 30;

    final long NS_BETWEEN_UPDATES = 1_000_000_000 / UPDATES_PER_SECOND;
    final long NS_BETWEEN_FRAMES = 1_000_000_000 / FRAMES_PER_SECOND;
    final int MAX_FRAME_SKIPS = (int) FRAMES_PER_SECOND / 2;

    private final AtomicReference<State> state;

    // For SIMULATION_SUSPENDED and SIMULATION_RUNNING.
    private SimulationModel simulationModel;
    private InputState inputState;
    private SimulationView simulationView;
    private SimulationRunnable simulationRunnable;
    private Thread simulationThread;

    public SimulationController() {
        this.state = new AtomicReference<>(State.NEW);
    }

    public synchronized void setUp() {

        // Validate state.
        assertState(
                this.state.get(), "setUp() called more than once.", State.NEW);

        // Set state to IDLE.
        this.state.set(State.IDLE);
    }

    public synchronized void loadSimulation(
            final SimulationModel simulationModel,
            final SimulationView simulationView,
            final InputState inputState) {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp(), before tearDown().",
                State.IDLE,
                State.SIMULATION_SUSPENDED,
                State.SIMULATION_RUNNING);

        // Validate parameters.
        final SimulationModel simulationModel_dc = simulationModel;
        final SimulationView simulationView_dc = simulationView;
        final InputState inputState_dc = inputState;
        if (simulationModel_dc == null
                || simulationView_dc == null
                || inputState_dc == null) {
            throw new NullPointerException(
                    "simulationModel: " + simulationModel_dc
                    + ", simulationView: " + simulationView_dc
                    + ", inputState: " + inputState_dc);
        }

        // Unload old simulation if present.
        if (stateRef == State.SIMULATION_SUSPENDED
                || stateRef == State.SIMULATION_RUNNING) {
            unloadSimulation();
        }

        // Load given simulation.
        this.simulationModel = simulationModel_dc;
        this.inputState = inputState_dc;
        this.simulationView = simulationView_dc;
        this.simulationRunnable = new SimulationRunnable(
                this.simulationModel,
                this.simulationView,
                this.inputState);
        this.simulationThread = new Thread(this.simulationRunnable);
        this.simulationThread.start();

        // Set state to SIMULATION_SUSPENDED.
        this.state.set(State.SIMULATION_SUSPENDED);
    }

    public synchronized void unloadSimulation() {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp()"
                + " and loadSimulation(...), before tearDown().",
                State.SIMULATION_SUSPENDED,
                State.SIMULATION_RUNNING);

        // Unload.
        if (stateRef == State.SIMULATION_RUNNING) {
            this.suspendSimulation();
        }
        this.simulationRunnable.exit();
        try {
            this.simulationThread.join();
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        this.simulationModel = null;
        this.simulationView = null;
        this.inputState = null;
        this.simulationRunnable = null;
        this.simulationThread = null;

        // Set state to IDLE.
        this.state.set(State.IDLE);
    }

    public synchronized void resumeSimulation() {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp() and loadSimulation(...), before"
                + " tearDown(). Cannot be called again until"
                + " suspendSimulation() is called.",
                State.SIMULATION_SUSPENDED);

        // Resume.
        this.simulationRunnable.resume();

        // Set state to SIMULATION_RUNNING.
        this.state.set(State.SIMULATION_RUNNING);
    }

    public synchronized void suspendSimulation() {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp(), loadSimulation(...) and"
                + " resumeSimulation(), before tearDown(). Cannot be called"
                + " again until suspendRunning() is called.",
                State.SIMULATION_RUNNING);

        // Suspend.
        this.simulationRunnable.suspend();

        // Set state to SIMULATION_SUSPENDED.
        this.state.set(State.SIMULATION_SUSPENDED);
    }

    public synchronized void tearDown() {

        // Validate state.
        final State stateRef = this.state.get();
        assertState(
                stateRef,
                "Must be called after setUp()."
                + " Cannot be called more than once.",
                State.IDLE,
                State.SIMULATION_SUSPENDED,
                State.SIMULATION_RUNNING);

        // Unload simulation if present.
        if (stateRef == State.SIMULATION_SUSPENDED
                || stateRef == State.SIMULATION_RUNNING) {
            this.unloadSimulation();
        }

        // Release resources.
        // ...
        // Set state to DISPOSED.
        this.state.set(State.DISPOSED);
    }

    private static void assertState(
            final State ref,
            final String exceptionMessage,
            final State... states) {
        boolean hasCorrectState = false;
        int i = 0;
        while (!hasCorrectState && i < states.length) {
            if (states[i] == ref) {
                hasCorrectState = true;
            }
            i++;
        }
        if (!hasCorrectState) {
            throw new IllegalStateException(exceptionMessage);
        }
    }

    private class SimulationRunnable implements Runnable {

        private final SimulationModel simulationModel;
        private final SimulationView simulationView;
        private final InputState inputState;

        // Subject processors.
        private final EntityProcessor entityProcessor;
        private final PickingProcessor pickingProcessor;

        private boolean suspend;
        private final AtomicBoolean exit;

        private SimulationRunnable(
                SimulationModel simulationModel,
                SimulationView simulationView,
                InputState inputState) {
            this.simulationModel = simulationModel;
            this.simulationView = simulationView;
            this.inputState = inputState;
            if (this.simulationModel == null || this.simulationView == null) {
                throw new NullPointerException(
                        "simulationModel: " + this.simulationModel
                        + ", simulationView: " + this.simulationView);
            }

            this.entityProcessor = new EntityProcessor(this.simulationModel);
            this.pickingProcessor = new PickingProcessor(this.simulationModel);
            this.suspend = true;
            this.exit = new AtomicBoolean(false);
        }

        @Override
        @SuppressWarnings("SleepWhileInLoop")
        public void run() {
            while (!exit.get()) {

                LOGGER.debug("SimulationRunnable - suspended.");

                // If suspended, use wait().
                synchronized (this) {
                    while (suspend) {
                        try {
                            wait();
                        } catch (InterruptedException ex) {
                            throw new RuntimeException(ex);
                        }
                    }
                }

                LOGGER.debug("SimulationRunnable - resumed.");

                // If resumed, run the simulation.
                // (Re)set timing variables.
                long nextUpdateTime = System.nanoTime();
                long updateLagAbsoluteNs = 0;
                long updateLagDifferenceNs;
                double timeScale;
                double timeScaleEma = 1;
                final double timeScaleEmaAlpha
                        = Math.exp((1d - (double) NS_BETWEEN_UPDATES)
                                / (0.5d * 1_000_000_000d));
                long nextFrameTime = System.nanoTime();
                int framesSkipped;
                long lastFrameTime = System.nanoTime();
                long frameNsGap;
                double frameNsGapEma = NS_BETWEEN_FRAMES;
                final double frameNsGapEmaAlpha
                        = Math.exp((1d - (double) NS_BETWEEN_FRAMES)
                                / (0.2d * 1_000_000_000d));
                double framesPerSecond;
                boolean sleep;
                long sleepNs;
                double interpolation;
                long lag;

                while (!suspend) {

                    // Update subject in an inner loop (can skip frames).
                    framesSkipped = 0;
                    while (!suspend
                            && System.nanoTime() >= nextUpdateTime
                            && framesSkipped < MAX_FRAME_SKIPS) {

                        // Update timing variables.
                        lag = System.nanoTime() - nextUpdateTime;
                        updateLagDifferenceNs = lag - updateLagAbsoluteNs;
                        updateLagAbsoluteNs = lag;
                        timeScale = (double) NS_BETWEEN_UPDATES
                                / (double) (NS_BETWEEN_UPDATES + System.nanoTime()
                                - nextUpdateTime);
                        timeScaleEma = timeScale + timeScaleEmaAlpha
                                * (timeScaleEma - timeScale);
                        this.simulationModel.runtimeStatistics.ewmaTimescale = timeScaleEma;
                        nextUpdateTime += NS_BETWEEN_UPDATES;
                        framesSkipped++;

                        // Update subject.
                        entityProcessor.update(NS_BETWEEN_UPDATES);
                    }

                    // Update input.
                    pickingProcessor.update(NS_BETWEEN_FRAMES);

                    // Update frame (draw all entities).
                    if (System.nanoTime() >= nextFrameTime) {

                        // Calculate interpolation.
                        interpolation
                                = (double) (System.nanoTime()
                                + NS_BETWEEN_UPDATES - nextUpdateTime)
                                / (double) NS_BETWEEN_UPDATES;

                        // Update display.
                        simulationView.update(NS_BETWEEN_UPDATES, interpolation);

                        // Calculate statistics.
                        frameNsGap = System.nanoTime() - lastFrameTime;
                        frameNsGapEma
                                = System.nanoTime()
                                - lastFrameTime + frameNsGapEmaAlpha
                                * (frameNsGapEma - frameNsGap);
                        framesPerSecond = 1_000_000_000d / frameNsGapEma;
                        this.simulationModel.runtimeStatistics.ewmaFramesPerSecond
                                = framesPerSecond;
                        lastFrameTime = System.nanoTime();
                        nextFrameTime = System.nanoTime() + NS_BETWEEN_FRAMES;
                    }

                    // Sleep in the spare time.
                    sleep = true;
                    while (!suspend && sleep) {
                        sleepNs = nextUpdateTime < nextFrameTime
                                ? nextUpdateTime - System.nanoTime()
                                : nextFrameTime - System.nanoTime();
                        if (sleepNs >= 1_000l) {
                            try {
                                Thread.sleep(0, 900);
                            } catch (InterruptedException ex) {
                                throw new RuntimeException(ex);
                            }
                        } else {
                            sleep = false;
                        }
                    }
                }

            }
        }

        private void suspend() {
            this.suspend = true;
        }

        private void resume() {
            synchronized (this) {
                this.suspend = false;
                notify();
            }
        }

        private void exit() {
            this.exit.set(true);
            this.suspend = true;
        }
    }
}
