/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.control;

import jjs.cs.simulation.SimulationModel;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class InspectableProcessor extends Processor {

    private final SimulationModel.Inspection inspection;
    // private final OldSwingGui comp2dGui;

    public InspectableProcessor(
            SimulationModel simulationState
    // OldSwingGui comp2dGui
    ) {
        super(simulationState);
        this.inspection = simulationState.inspection;
        // this.comp2dGui = comp2dGui;
    }

    @Override
    public void update(long deltaNanoseconds) {
        if (this.inspection.inspectableChanged) {
            //this.comp2dGui.changeInspectable(this.inspection.inspectable);
            this.inspection.inspectableChanged = false;
        }
        if (this.inspection.inspectable != null) {
            this.inspection.inspectable.updateProperties();
            //this.comp2dGui.updateInspectable();
        }
    }
}
