/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.control.EntityProcessor;
import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.RenderHook;
import jjs.cs.simulation.data.BoxRenderModel;
import jjs.cs.simulation.data.Entity;
import jjs.cs.simulation.data.Inspectable;
import jjs.cs.simulation.data.RenderModel;
import jjs.cs.simulation.data.WorldPosition;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class ParallelBusWire implements Entity, Inspectable {

    public static final float SINGLE_CHANNEL_WIDTH = 0.08f;

    private final int channels;
    protected final long bitMask;
    protected final Terminus terminusA;
    protected final Terminus terminusB;
    private boolean collision;

    protected final ArrayList<SegmentModel> segmentModels;

    private final Map<String, Object> properties;
    private final AtomicReference<String> prop_word;

    protected long word;

    private final String name;

    public ParallelBusWire(int channels, String name) {
        if (channels < 1 || channels > 64) {
            throw new IllegalArgumentException(
                    "Must have 1 <= C <= 64 channels. C = " + channels);
        }
        this.channels = channels;
        this.bitMask = ((long) Math.pow(2, channels)) - 1;
        this.terminusA = new Terminus();
        this.terminusB = new Terminus();
        this.collision = false;
        this.segmentModels = new ArrayList<>();

        this.properties = new TreeMap<>();
        this.properties.put("Channels", String.valueOf(channels));
        this.prop_word = new AtomicReference<>();
        this.properties.put("Word", this.prop_word);
        this.name = name;
    }

    @Override
    public void addHooks(HookFile hookFile) {
        hookFile.addHook(
                new EntityProcessor.EntityHook(
                        hookFile.newId(), this),
                EntityProcessor.EntityHook.class);

        if (this.terminusA.pbwConnector != null
                && this.terminusB.pbwConnector != null) {

            final WorldPosition aWp
                    = this.terminusA.pbwConnector.getWorldPosition();
            final WorldPosition bWp
                    = this.terminusB.pbwConnector.getWorldPosition();
            createSegs(aWp, bWp, hookFile);
        }
    }

    @Override
    public void observe() {
        terminusA.observe();
        terminusB.observe();
    }

    @Override
    public void update(long deltaNanoseconds) {
        final long collidedWord;
        if (!terminusA.observed_connectorCharged
                && !terminusB.observed_connectorCharged) {
            collidedWord = 0L;
            collision = false;
        } else if (!terminusA.observed_connectorCharged
                && terminusB.observed_connectorCharged) {
            collidedWord = terminusB.observerd_connectorWord;
            collision = false;
        } else if (terminusA.observed_connectorCharged
                && !terminusB.observed_connectorCharged) {
            collidedWord = terminusA.observerd_connectorWord;
            collision = false;
        } else {
            collidedWord
                    = terminusA.observerd_connectorWord
                    ^ terminusB.observerd_connectorWord;
            collision = true;
        }
        word = collidedWord & bitMask;
    }

    public int getChannels() {
        return channels;
    }

    public Terminus getTerminusA() {
        return terminusA;
    }

    public Terminus getTerminusB() {
        return terminusB;
    }

    public boolean hadCollision() {
        return collision;
    }

    @Override
    public String getInspectableName() {
        return name + " bus";
    }

    @Override
    public Map<String, Object> getProperties() {
        return properties;
    }

    @Override
    public void updateProperties() {
        this.prop_word.set(String.valueOf(word));
    }

    public class Terminus {

        protected PbwConnector pbwConnector;
        protected boolean observed_connectorCharged;
        protected long observerd_connectorWord;

        protected Terminus() {
            this.pbwConnector = null;
            this.observed_connectorCharged = false;
            this.observerd_connectorWord = 0L;
        }

        public long getWord() {
            return word;
        }

        public int getChannels() {
            return channels;
        }

        protected void observe() {
            if (this.pbwConnector == null) {
                this.observed_connectorCharged = false;
                this.observerd_connectorWord = 0L;
            } else {
                this.observed_connectorCharged = this.pbwConnector.isCharged();
                this.observerd_connectorWord = this.pbwConnector.getWord();
            }
        }

        public void connect(PbwConnector pbwConnector) {
            if (this.pbwConnector != null) {
                throw new IllegalStateException("Already connected");
            }
            this.pbwConnector = pbwConnector;
        }
    }

    protected class SegmentModel {

        private final WorldPosition wp;
        private final RenderModel rm;
        private RenderHook renderHook;

        public SegmentModel(WorldPosition wp, RenderModel rm) {
            this.wp = wp;
            this.rm = rm;
        }

        public void addHooks(HookFile hookFile) {
            this.renderHook = new RenderHook(hookFile.newId(), wp, rm);
            hookFile.addHook(this.renderHook, RenderHook.class);
        }
    }

    protected void createSegs(
            final WorldPosition aWp,
            final WorldPosition bWp,
            HookFile hookFile) {

        final float xDistance = aWp.position.x - bWp.position.x;
        final float yDistance = aWp.position.y - bWp.position.y;
        final float midXPos = aWp.position.x - xDistance / 2f;
        final float midYPos = aWp.position.y - yDistance / 2f;

        {
            final SegmentModel midSegment = new SegmentModel(
                    new WorldPosition(
                            midXPos, midYPos,
                            SimCom.COMPONENT_POS_Z - (SimCom.COMPONENT_DEPTH / 3f),
                            0, 0, 90,
                            yDistance, SINGLE_CHANNEL_WIDTH * channels,
                            SimCom.COMPONENT_DEPTH / 3f),
                    new BoxRenderModel("simcom_bus.png", false));
            midSegment.addHooks(hookFile);
            this.segmentModels.add(midSegment);
        }
        {
            final SegmentModel aSegment = new SegmentModel(
                    new WorldPosition(
                            aWp.position.x - xDistance / 4f, aWp.position.y,
                            SimCom.COMPONENT_POS_Z - (SimCom.COMPONENT_DEPTH / 3f),
                            0, 0, 0,
                            xDistance / 2f, SINGLE_CHANNEL_WIDTH * channels,
                            SimCom.COMPONENT_DEPTH / 3f),
                    new BoxRenderModel("simcom_bus.png", false));
            aSegment.addHooks(hookFile);
            this.segmentModels.add(aSegment);

        }
        {
            final SegmentModel aSegment = new SegmentModel(
                    new WorldPosition(
                            aWp.position.x - (xDistance / 4f) * 3f,
                            bWp.position.y, SimCom.COMPONENT_POS_Z
                            - (SimCom.COMPONENT_DEPTH / 3f),
                            0, 0, 0,
                            xDistance / 2f, SINGLE_CHANNEL_WIDTH * channels,
                            SimCom.COMPONENT_DEPTH / 3f),
                    new BoxRenderModel("simcom_bus.png", false));
            aSegment.addHooks(hookFile);
            this.segmentModels.add(aSegment);

        }
    }
}
