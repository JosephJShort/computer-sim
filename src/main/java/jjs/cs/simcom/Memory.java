/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.simulation.HookFile;
import jjs.cs.utils.WordUtils;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class Memory extends BasicInspectableEntity {

    public static final int SIZE_IN_BYTES = 1_000_000; // 1MB.

    public static final int CONTROL_CHANNELS = 2;

    // Connections to other entities.
    private final PbwConnector inputAddress;
    private final PbwConnector inputData;
    private final PbwConnector output;
    private final PbwConnector control;

    // Observations.
    private long obs_inputAddress;
    private long obs_inputData;
    private long obs_output;
    private long obs_control;
    private ControlWord obs_controlAction;

    // State.
    final byte[] bytes;

    // Properties.
    private final AtomicReference<String> prop_control;
    private final AtomicReference<String> prop_controlAction;

    public static enum ControlWord {

        NOTHING(0b00),
        STORE(0b01),
        RETRIEVE(0b10),
        STORE_BYTE(0b11);
        public final long word;

        private ControlWord(long word) {
            this.word = word;
        }

    }

    public Memory(
            float posX,
            float posY) {
        super(
                posX,
                posY,
                SimCom.COMPONENT_POS_Z,
                SimCom.GRID_STEP * 14f,
                SimCom.GRID_STEP * 4f,
                SimCom.COMPONENT_DEPTH,
                "simcom_memory.png");

        this.bytes = new byte[SIZE_IN_BYTES];

        this.inputAddress = PbwConnector.createAttatched(
                worldPosition, false, true, 7, 3,
                SimCom.WORD_LENGTH, "Input address");
        this.inputData = PbwConnector.createAttatched(
                worldPosition, false, true, 7, 5,
                SimCom.WORD_LENGTH, "Input data");
        this.output = PbwConnector.createAttatched(
                worldPosition, false, false, 2, 1,
                SimCom.WORD_LENGTH, "Output");
        this.control = PbwConnector.createAttatched(
                worldPosition, true, false, 2, 1, CONTROL_CHANNELS, "Control");

        // Observations.
        this.obs_inputAddress = 0l;
        this.obs_inputData = 0l;
        this.obs_output = 0l;
        this.obs_control = 0l;
        this.obs_controlAction = ControlWord.NOTHING;

        // Properties.
        this.prop_control = new AtomicReference<>();
        this.prop_controlAction = new AtomicReference<>();
        this.properties.put("!1. Control signal", prop_control);
        this.properties.put("!2. Control action", prop_controlAction);
        this.properties.putAll(this.inputAddress.getProperties());
        this.properties.putAll(this.inputData.getProperties());
        this.properties.putAll(this.output.getProperties());
        this.properties.putAll(this.control.getProperties());
    }

    @Override
    public void addHooks(HookFile hookFile) {
        super.addHooks(hookFile);
        this.inputAddress.addHooks(hookFile);
        this.inputData.addHooks(hookFile);
        this.output.addHooks(hookFile);
        this.control.addHooks(hookFile);
    }

    @Override
    public void observe() {
        this.obs_inputAddress = this.inputAddress.getWord();
        this.obs_inputData = this.inputData.getWord();
        this.obs_control = this.control.getWord();
        this.obs_controlAction = this.obs_control < ControlWord.values().length
                ? ControlWord.values()[(int) this.obs_control] : null;
    }

    @Override
    public void update(long deltaNanoseconds) {
        output.removeCharge();
        if (obs_controlAction == ControlWord.STORE) {
            storeWord((int) obs_inputAddress, (int) obs_inputData);
        } else if (obs_controlAction == ControlWord.RETRIEVE) {
            output.charge(retrieveWord((int) obs_inputAddress));
        } else if (obs_controlAction == ControlWord.STORE_BYTE) {
            storeByte((int) obs_inputAddress, WordUtils.toBytes(obs_inputData)[7]);
        }
    }

    public void connect(
            ParallelBusWire.Terminus t_inputAddress,
            ParallelBusWire.Terminus t_inputData,
            ParallelBusWire.Terminus t_output,
            ParallelBusWire.Terminus t_control) {
        this.inputAddress.connect(t_inputAddress);
        this.inputData.connect(t_inputData);
        this.output.connect(t_output);
        this.control.connect(t_control);

    }

    @Override
    public void updateProperties() {
        this.inputAddress.updateProperties();
        this.inputData.updateProperties();
        this.output.updateProperties();
        this.control.updateProperties();
        this.prop_control.set(String.valueOf(obs_control));
        this.prop_controlAction.set(obs_controlAction == null
                ? "[UNKNOWN]" : String.valueOf(obs_controlAction));
    }

    public void storeWord(int location, int word) {
        byte[] wordBytes
                = ByteBuffer.allocate(4).putInt(word).array();
        System.arraycopy(wordBytes, 0, bytes, location, 4);
    }

    public int retrieveWord(int location) {
        byte[] wordBytes = new byte[4];
        for (int i = 0; i < wordBytes.length; i++) {
            wordBytes[i] = bytes[location + i];
        }
        ByteBuffer byteBuffer = ByteBuffer.wrap(wordBytes);
        return byteBuffer.getInt();
    }

    public void storeByte(int location, byte b) {
        bytes[location] = b;
    }

    public byte retrieveByte(int location) {
        return bytes[location];
    }

    public void wipe() {
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = 0;
        }
    }
}
