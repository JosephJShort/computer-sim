/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.RenderHook;
import jjs.cs.simulation.data.BoxRenderModel;
import jjs.cs.simulation.data.RenderModel;
import jjs.cs.simulation.data.WorldPosition;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class PrintedCircuitBoard {

    private final WorldPosition worldPosition;
    private final RenderModel model;
    private RenderHook renderHook;

    public PrintedCircuitBoard(
            float posX, float posY, float width, float height) {
        this.worldPosition = new WorldPosition(
                new Vector3f(posX, posY,
                        SimCom.COMPONENT_POS_Z - SimCom.COMPONENT_DEPTH),
                new Vector3f(0, 0, 0),
                new Vector3f(width, height, SimCom.COMPONENT_DEPTH));
        this.model = new BoxRenderModel("simcom_pcb.png", false);
    }

    public void addHooks(HookFile hookFile) {
        renderHook = new RenderHook(
                hookFile.newId(), worldPosition, model);
        hookFile.addHook(renderHook, RenderHook.class);
    }
}
