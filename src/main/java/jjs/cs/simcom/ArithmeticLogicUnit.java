/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.simulation.HookFile;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class ArithmeticLogicUnit extends BasicInspectableEntity {

    public static final int CONTROL_CHANNELS = 10;

    private final PbwConnector inputA;
    private final PbwConnector inputB;
    private final PbwConnector output;
    private final PbwConnector control;

    private long obs_inputA;
    private long obs_inputB;
    private long obs_output;
    private long obs_control;
    private ControlWord obs_controlAction;

    private final AtomicReference<String> property_controlSignal;
    private final AtomicReference<String> property_controlAction;

    private long result = 0;

    public ArithmeticLogicUnit(
            float posX,
            float posY) {
        super(
                posX, posY, SimCom.COMPONENT_POS_Z,
                4f * SimCom.GRID_STEP,
                4f * SimCom.GRID_STEP,
                SimCom.COMPONENT_DEPTH,
                "simcom_alu.png");

        this.inputA = PbwConnector.createAttatched(
                worldPosition, true, false, 4, 1, 32, "Input A");
        this.inputB = PbwConnector.createAttatched(
                worldPosition, true, false, 4, 3, 32, "Input B");
        this.output = PbwConnector.createAttatched(
                worldPosition, true, true, 2, 1, 32, "Output");
        this.control = PbwConnector.createAttatched(
                worldPosition, false, true, 2, 1, CONTROL_CHANNELS, "Control");

        this.property_controlSignal = new AtomicReference<>();
        this.property_controlAction = new AtomicReference<>();
        this.properties.put("1. Control signal", this.property_controlSignal);
        this.properties.put("2. Control action", this.property_controlAction);
        this.properties.putAll(this.inputA.getProperties());
        this.properties.putAll(this.inputB.getProperties());
        this.properties.putAll(this.output.getProperties());

        this.obs_control = 0l;
        this.obs_controlAction = ControlWord.NOTHING;
    }

    public void connect(
            ParallelBusWire.Terminus inputA,
            ParallelBusWire.Terminus inputB,
            ParallelBusWire.Terminus output,
            ParallelBusWire.Terminus control) {

        this.inputA.connect(inputA);
        this.inputB.connect(inputB);
        this.output.connect(output);
        this.control.connect(control);
    }

    @Override
    public void addHooks(HookFile hookFile) {
        super.addHooks(hookFile);
        inputA.addHooks(hookFile);
        inputB.addHooks(hookFile);
        output.addHooks(hookFile);
        control.addHooks(hookFile);
    }

    @Override
    public void observe() {
        obs_inputA = inputA.getWord();
        obs_inputB = inputB.getWord();
        obs_output = output.getWord();
        obs_control = control.getWord();
        obs_controlAction = obs_control < ControlWord.values().length
                ? ControlWord.values()[(int) obs_control] : null;
    }

    @Override
    public void update(long deltaNanoseconds) {
        if (obs_controlAction != null
                && obs_controlAction != ControlWord.NOTHING) {
            if (obs_controlAction != ControlWord.REPEAT) {
                result = obs_controlAction.operation(obs_inputA, obs_inputB);
            }
            this.output.charge(result);
        } else {
            this.output.removeCharge();
        }
    }

    @Override
    public void updateProperties() {
        this.inputA.updateProperties();
        this.inputB.updateProperties();
        this.output.updateProperties();
        this.property_controlSignal.set(String.valueOf(obs_control));
        this.property_controlAction.set(obs_controlAction == null ? "[UNKNOWN]"
                : String.valueOf(obs_controlAction));
    }

    public enum ControlWord {

        NOTHING {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return 0l;
                    }

                },
        PLUS {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA + operandB;
                    }
                },
        MINUS {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA - operandB;
                    }
                },
        AND {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA & operandB;
                    }
                },
        INCLUSIVE_OR {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA | operandB;
                    }
                },
        EXCLUSIVE_OR {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA ^ operandB;
                    }
                },
        SET_EQUAL_TO {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA == operandB ? 1 : 0;
                    }
                },
        SET_NOT_EQUAL_TO {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA != operandB ? 1 : 0;
                    }
                },
        SET_LESS_THAN {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA < operandB ? 1 : 0;
                    }
                },
        SET_GREATER_THAN {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA > operandB ? 0 : 1;
                    }
                },
        LEFT_SHIFT {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA << operandB;
                    }
                },
        RIGHT_SHIFT {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA >> operandB;
                    }
                },
        RIGHT_SHIFT_SIGNED {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA >>> operandB;
                    }
                },
        PASS_INPUT_1 {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandA;
                    }
                },
        PASS_INPUT_2 {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return operandB;
                    }
                },
        REPEAT {

                    @Override
                    protected long operation(long operandA, long operandB) {
                        return 0;
                    }
                };

        protected abstract long operation(long operandA, long operandB);

        public long word() {
            return ordinal();
        }
    }
}
