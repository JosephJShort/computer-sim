/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.control.hooks.StringInputHook;
import jjs.cs.control.hooks.StringOutputHook;
import jjs.cs.simulation.HookFile;
import jjs.cs.utils.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class HardwiredControlUnit
        extends BasicInspectableEntity implements ControlUnit {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(HardwiredControlUnit.class);

    private static final int TICKS_PER_STEP = 10;

    private static enum SyscallState {

        READ_CODE,
        READ_A0,
        READ_A1,
        READ_A2,
        READ_A3;
    }

    private final PbwConnector input;
    private final PbwConnector output1;
    private final PbwConnector output2;
    private final PbwConnector control_mdu;
    private final PbwConnector control_register_hi;
    private final PbwConnector control_register_lo;
    private final PbwConnector control_alu;
    private final PbwConnector control_registerFileRw;
    private final PbwConnector control_registerFileSel;
    private final PbwConnector control_register_a;
    private final PbwConnector control_register_b;
    private final PbwConnector control_register_c;
    private final PbwConnector control_register_temp;
    private final PbwConnector control_register_pc;
    private final PbwConnector control_register_mar;
    private final PbwConnector control_mdrMultiplexer;
    private final PbwConnector control_register_mdr;
    private final PbwConnector control_memory;
    private final PbwConnector control_register_instruction;

    private final Memory debugMemoryRef;
    private final Register debugCRef;

    private State state;
    private State nextState;
    private Step[] stateSteps;
    private int stateCurrentStep;
    private int stateIdleTicks;

    private final AtomicReference<String> stringOutput;
    private StringOutputHook stringOutputHook;
    private final AtomicReference<String> stringInput;
    private StringInputHook stringInputHook;

    private final AtomicReference<String> property_operation;
    private final AtomicReference<String> property_operationStep;
    private final AtomicReference<String> property_instruction;

    private long obs_instructionWord = 0l;
    private long obs_output1 = 0;
    private long obs_output2 = 0;
    private long instruction;
    private long immed;
    private long offset;
    private int rs;
    private int rt;
    private boolean exit = false;
    private SyscallState syscallState = null;
    private int systemCallCode = 0;
    private int a0 = 0;
    private int a1 = 0;
    private int a2 = 0;
    private int a3 = 0;

    private boolean readingNumber = false;
    private int number = 0;

    public HardwiredControlUnit(
            float posX,
            float posY,
            Memory debugMemoryRef,
            Register debugCRef) {
        super(
                posX,
                posY,
                SimCom.COMPONENT_POS_Z,
                SimCom.GRID_STEP * 4f,
                SimCom.GRID_STEP * 12f,
                SimCom.COMPONENT_DEPTH,
                "simcom_hcu.png");

        this.debugMemoryRef = debugMemoryRef;
        this.debugCRef = debugCRef;

        this.input = PbwConnector.createAttatched(
                worldPosition, false, false, 3, 1, 32, "Input");
        this.output1 = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 18, 32, "Output 1");
        this.output2 = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 20, 32, "Output 2");
        this.control_mdu = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 16,
                MultiplyDivideUnit.CONTROL_SIGNAL_CHANNELS, "Control MDU");
        this.control_register_hi = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 15,
                Register.CONTROL_CHANNELS, "Control Reg Hi");
        this.control_register_lo = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 14,
                Register.CONTROL_CHANNELS, "Control Reg Lo");
        this.control_alu = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 13,
                ArithmeticLogicUnit.CONTROL_CHANNELS, "Control ALU");
        this.control_registerFileRw = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 12,
                RegisterFile.CONTROL_RW_CHANNELS, "Control Register File Rw");
        this.control_registerFileSel = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 11,
                RegisterFile.CONTROL_SEL_CHANNELS, "Control Register File Sel");
        this.control_register_a = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 10,
                Register.CONTROL_CHANNELS, "Control Reg A");
        this.control_register_b = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 9,
                Register.CONTROL_CHANNELS, "Control Reg B");
        this.control_register_c = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 8,
                Register.CONTROL_CHANNELS, "Control Reg C");
        this.control_register_temp = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 7,
                Register.CONTROL_CHANNELS, "Control Reg Temp");
        this.control_register_pc = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 6,
                Register.CONTROL_CHANNELS, "Control Reg PC");
        this.control_register_mar = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 5,
                Register.CONTROL_CHANNELS, "Control Reg MAR");
        this.control_mdrMultiplexer = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 4,
                Multiplexer.CONTROL_CHANNELS, "Control MDR mpx");
        this.control_register_mdr = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 3,
                Register.CONTROL_CHANNELS, "Control Reg MDR");
        this.control_memory = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 2,
                Memory.CONTROL_CHANNELS, "Control Memory");
        this.control_register_instruction = PbwConnector.createAttatched(
                worldPosition, true, true, 21, 1,
                Register.CONTROL_CHANNELS, "Control Reg Instruction");

        this.state = GenericState.IF1;
        this.stateSteps = this.state.getSteps();
        this.stateCurrentStep = 0;
        this.stateIdleTicks = 0;

        this.property_operation = new AtomicReference<>();
        this.property_operationStep = new AtomicReference<>();
        this.property_instruction = new AtomicReference<>();
        this.properties.put("1. Operation", this.property_operation);
        this.properties.put("2. Op. step", this.property_operationStep);
        this.properties.put("3. Instruction", this.property_instruction);
        this.properties.putAll(input.getProperties());
        this.properties.putAll(output1.getProperties());
        this.properties.putAll(output2.getProperties());

        this.stringOutput = new AtomicReference<>();
        this.stringInput = new AtomicReference<>();

    }

    @Override
    public void addHooks(HookFile hookFile) {
        super.addHooks(hookFile);
        this.input.addHooks(hookFile);
        this.output1.addHooks(hookFile);
        this.output2.addHooks(hookFile);
        this.control_mdu.addHooks(hookFile);
        this.control_register_hi.addHooks(hookFile);
        this.control_register_lo.addHooks(hookFile);
        this.control_alu.addHooks(hookFile);
        this.control_registerFileRw.addHooks(hookFile);
        this.control_registerFileSel.addHooks(hookFile);
        this.control_register_a.addHooks(hookFile);
        this.control_register_b.addHooks(hookFile);
        this.control_register_c.addHooks(hookFile);
        this.control_register_temp.addHooks(hookFile);
        this.control_register_pc.addHooks(hookFile);
        this.control_register_mar.addHooks(hookFile);
        this.control_mdrMultiplexer.addHooks(hookFile);
        this.control_register_mdr.addHooks(hookFile);
        this.control_memory.addHooks(hookFile);
        this.control_register_instruction.addHooks(hookFile);
        this.stringOutputHook
                = new StringOutputHook(hookFile.newId(), stringOutput);
        hookFile.addHook(this.stringOutputHook, StringOutputHook.class);
        this.stringInputHook
                = new StringInputHook(hookFile.newId(), stringInput);
        hookFile.addHook(this.stringInputHook, StringInputHook.class);
    }

    public boolean exit() {
        return exit;
    }

    public void reset() {
        this.nextState = GenericState.WAIT_FOR_PROGRAM;
        this.state = GenericState.WAIT_FOR_PROGRAM;
        this.stateCurrentStep = 0;

        this.stateIdleTicks = 0;
        this.stateSteps = this.nextState.getSteps();
        if (exit) {
            exit = false;
        }
    }

    public void connect(
            ParallelBusWire.Terminus t_input,
            ParallelBusWire.Terminus t_output1,
            ParallelBusWire.Terminus t_output2,
            ParallelBusWire.Terminus t_control_mdu,
            ParallelBusWire.Terminus t_control_register_hi,
            ParallelBusWire.Terminus t_control_register_lo,
            ParallelBusWire.Terminus t_control_alu,
            ParallelBusWire.Terminus t_control_registerFileRw,
            ParallelBusWire.Terminus t_control_registerFileSel,
            ParallelBusWire.Terminus t_control_register_a,
            ParallelBusWire.Terminus t_control_register_b,
            ParallelBusWire.Terminus t_control_register_c,
            ParallelBusWire.Terminus t_control_register_temp,
            ParallelBusWire.Terminus t_control_register_pc,
            ParallelBusWire.Terminus t_control_register_mar,
            ParallelBusWire.Terminus t_control_mdrMultiplexer,
            ParallelBusWire.Terminus t_control_register_mdr,
            ParallelBusWire.Terminus t_control_memory,
            ParallelBusWire.Terminus t_control_register_instruction) {
        this.input.connect(t_input);
        this.output1.connect(t_output1);
        this.output2.connect(t_output2);
        this.control_mdu.connect(t_control_mdu);
        this.control_register_hi.connect(t_control_register_hi);
        this.control_register_lo.connect(t_control_register_lo);
        this.control_alu.connect(t_control_alu);
        this.control_registerFileRw.connect(t_control_registerFileRw);
        this.control_registerFileSel.connect(t_control_registerFileSel);
        this.control_register_a.connect(t_control_register_a);
        this.control_register_b.connect(t_control_register_b);
        this.control_register_c.connect(t_control_register_c);
        this.control_register_temp.connect(t_control_register_temp);
        this.control_register_pc.connect(t_control_register_pc);
        this.control_register_mar.connect(t_control_register_mar);
        this.control_mdrMultiplexer.connect(t_control_mdrMultiplexer);
        this.control_register_mdr.connect(t_control_register_mdr);
        this.control_memory.connect(t_control_memory);
        this.control_register_instruction.connect(t_control_register_instruction);
    }

    @Override
    public void observe() {
        this.obs_instructionWord = this.input.getWord();
        this.obs_output1 = this.output1.getWord();
        this.obs_output2 = this.output2.getWord();
    }

    @Override
    public void update(long deltaNanoseconds) {
        if (stringInputHook.isActive()) {
            final String str = stringInput.get();
            if (str.matches("in [0-9]+")) {
                this.readingNumber = true;
                this.number = Integer.parseInt(str.substring(3));
            }
            stringInputHook.deactivate();
        }

        if (stateIdleTicks == 0) {
            stateIdleTicks = TICKS_PER_STEP - 1;

            // Read from input1 if there's a sys call happening.
            if (syscallState != null) {
                if (syscallState == SyscallState.READ_CODE) {
                    this.systemCallCode = (int) obs_output1;
                } else if (syscallState == SyscallState.READ_A0) {
                    this.a0 = (int) obs_output1;
                }
                if (syscallState == SyscallState.READ_A1) {
                    this.a1 = (int) obs_output1;
                }
                if (syscallState == SyscallState.READ_A2) {
                    this.a2 = (int) obs_output1;
                }
                if (syscallState == SyscallState.READ_A3) {
                    this.a3 = (int) obs_output1;
                }
            }

            // Clear signals from previous step.
            this.input.removeCharge();
            this.output1.removeCharge();
            this.output2.removeCharge();
            this.control_mdu.removeCharge();
            this.control_register_hi.removeCharge();
            this.control_register_lo.removeCharge();
            this.control_alu.removeCharge();
            this.control_registerFileRw.removeCharge();
            this.control_registerFileSel.removeCharge();
            this.control_register_a.removeCharge();
            this.control_register_b.removeCharge();
            this.control_register_c.removeCharge();
            this.control_register_temp.removeCharge();
            this.control_register_pc.removeCharge();
            this.control_register_mar.removeCharge();
            this.control_mdrMultiplexer.removeCharge();
            this.control_register_mdr.removeCharge();
            this.control_memory.removeCharge();
            //this.control_register_instruction.removeCharge();

            // Execute step.
            if (stateCurrentStep >= stateSteps.length) {

                // State transition.
                state = nextState;
                LOGGER.debug(String.valueOf(state));
                stateSteps = state.getSteps();
                stateCurrentStep = 0;
            }
            stateSteps[stateCurrentStep].execute(this);
            stateCurrentStep++;
            readingNumber = false;
        } else {

            // Idle for this tick.
            stateIdleTicks--;
        }
    }

    @Override
    public Map<String, Object> getProperties() {
        return properties;
    }

    @Override
    public void updateProperties() {

        this.input.updateProperties();
        this.output1.updateProperties();
        this.output2.updateProperties();
        this.property_operation.set(this.state.toString());
        this.property_operationStep.set(
                this.stateCurrentStep + "/" + this.stateSteps.length);
        this.property_instruction.set(Long.toHexString(obs_instructionWord));
    }

    @Override
    public String getInspectableName() {
        return inspectableName;
    }

    private static interface State {

        public abstract Step[] getSteps();
    }

    private static interface Step {

        public abstract void execute(HardwiredControlUnit hcu);
    }

    private enum GenericState implements State {

        WAIT_FOR_PROGRAM(new Step[]{
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    // "pc" gets 0
                    hcu.output1.charge(0);
                    hcu.control_register_pc.charge(
                            Register.ControlWord.READ_INPUT.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PASS_INPUT_1.word());
                    // State transition.
                    hcu.nextState = IF1;
                }
            }
        }),
        EXIT(new Step[]{
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.exit = true;
                }
            }}),
        IF1(new Step[]{
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // "mar" gets value of "pc".
                    hcu.control_register_pc.charge(
                            Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PASS_INPUT_1.word());
                    hcu.control_register_mar.charge(
                            Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    // IR gets getWord from memory (using address in MAR).
                    hcu.control_register_mar.charge(
                            Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_memory.charge(Memory.ControlWord.RETRIEVE.word);
                    hcu.control_register_instruction.charge(
                            Register.ControlWord.READ_INPUT.word);

                    // State transition.
                    hcu.nextState = IF2;
                }
            }}),
        IF2(new Step[]{
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // "Instruction" reads input from memory bus.
                    final boolean memoryReady = true;
                    if (memoryReady) {
                        // "Instruction" writes word to instruction bus.
                        hcu.control_register_instruction.charge(
                                Register.ControlWord.WRITE_OUTPUT_A.word);

                        // State transition.
                        hcu.nextState = ID;

                    } else {
                        hcu.nextState = IF2;
                    }
                }
            }}),
        ID(new Step[]{
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.instruction = hcu.obs_instructionWord;

                    // "B" get word at [rs] (the 2nd 5 bits) from the register file.
                    hcu.rs = WordUtils.intField((int) hcu.instruction, 21, 25);
                    hcu.control_registerFileSel.charge(hcu.rs);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.OUTPUT_A.word);
                    hcu.control_register_a.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // "B" get word at [rt] (the 3rd 5 bits) from the register file.
                    hcu.rt = WordUtils.intField((int) hcu.instruction, 16, 20);
                    hcu.control_registerFileSel.charge(hcu.rt);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.OUTPUT_B.word);
                    hcu.control_register_b.charge(Register.ControlWord.READ_INPUT.word);

                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    // ALU adds "PC" + 4.
                    hcu.control_register_pc.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.output2.charge(4);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PLUS.word());
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    // "PC" gets "PC + 4".
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.REPEAT.word());
                    hcu.control_register_pc.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // immed is 16 sign bits concat field 0...15
                    int immediate = (WordUtils.intField((int) hcu.instruction, 15, 15)) == 1 ? -1 : 0;
                    immediate <<= 16;
                    immediate |= (WordUtils.intField((int) hcu.instruction, 0, 15));
                    hcu.immed = immediate;
                    // offset is 14 sign bits concat field 0...15 concat two 0s
                    int offset = (WordUtils.intField((int) hcu.instruction, 15, 15)) == 1 ? -1 : 0;
                    offset <<= 14;
                    offset |= (WordUtils.intField((int) hcu.instruction, 0, 15));
                    offset <<= 2;
                    hcu.offset = offset;

                    // set new state
                    int firstOpcode = WordUtils.intField((int) hcu.instruction, 26, 31);
                    int secondOpcode = WordUtils.intField((int) hcu.instruction, 0, 5);
                    LOGGER.debug("Loaded instruction: "
                            + Long.toHexString(hcu.instruction)
                            + " first op: "
                            + Integer.toHexString(firstOpcode)
                            + ", second op: "
                            + Integer.toHexString(secondOpcode) + ".");
                    if (firstOpcode == 0) {
                        if (secondOpcode == 0x00) {
                            hcu.nextState = WAIT_FOR_PROGRAM;
                        } else if (secondOpcode == 0x08) {
                            hcu.nextState = BranchInstructionState.jr_EX;
                        } else if (secondOpcode == 0x0C) {
                            // syscall
                            hcu.nextState = MiscellaneousState.syscall_EX;
                        } else if (secondOpcode == 0x10) {
                            hcu.nextState = MiscellaneousState.mfhi_EX;
                        } else if (secondOpcode == 0x12) {
                            hcu.nextState = MiscellaneousState.mflo_EX;
                        } else if (secondOpcode == 0x18) {
                            hcu.nextState = AluInstructionState.mult_EX;
                        } else if (secondOpcode == 0x1B) {
                            hcu.nextState = AluInstructionState.divu_EX;
                        } else if (secondOpcode == 0x20 || secondOpcode == 0x21) {
                            hcu.nextState = AluInstructionState.add_EX;
                        } else if (secondOpcode == 0x22) {
                            hcu.nextState = AluInstructionState.sub_EX;
                        } else if (secondOpcode == 0x2A) {
                            hcu.nextState = AluInstructionState.slt_EX;
                        } else {
                            throw new UnsupportedOperationException(
                                    "Instruction with unknown opcode: "
                                    + firstOpcode + ", " + secondOpcode);
                        }
                    } else if (firstOpcode == 2) {
                        hcu.nextState = BranchInstructionState.j_EX;
                    } else if (firstOpcode == 3) {
                        hcu.nextState = BranchInstructionState.jal_EX;
                    } else if (firstOpcode == 4) {
                        hcu.nextState = BranchInstructionState.beq_EX1;
                    } else if (firstOpcode == 5) {
                        hcu.nextState = BranchInstructionState.bne_EX1;
                    } else if (firstOpcode == 6) {
                        hcu.nextState = BranchInstructionState.blez_EX1;
                    } else if (firstOpcode == 7) {
                        hcu.nextState = BranchInstructionState.bgtz_EX1;
                    } else if (firstOpcode == 8 || firstOpcode == 9) {
                        hcu.nextState = AluInstructionState.addiu_EX;
                    } else if (firstOpcode == 0xd) {
                        hcu.nextState = AluInstructionState.ori_EX;
                    } else if (firstOpcode == 0xf) {
                        hcu.nextState = LoadStoreInstructionState.lui_EX;
                    } else if (firstOpcode == 0x23) {
                        hcu.nextState = LoadStoreInstructionState.lw_EX;
                    } else if (firstOpcode == 0x24) {
                        hcu.nextState = LoadStoreInstructionState.lb_EX;
                    } else if (firstOpcode == 0x28) {
                        hcu.nextState = LoadStoreInstructionState.sb_EX1;
                    } else if (firstOpcode == 0x2b) {
                        hcu.nextState = LoadStoreInstructionState.sw_EX1;
                    } else {
                        throw new IllegalStateException(
                                "Instruction with unknown opcode: " + firstOpcode);
                    }
                }
            }});

        private final Step[] steps;

        private GenericState(Step[] steps) {
            this.steps = steps;
        }

        @Override
        public Step[] getSteps() {
            return steps;
        }
    }

    private enum AluInstructionState implements State {

        add_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // c gets a + immediate
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_b.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PLUS.word());
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = add_WB;

                }
            }}),
        add_WB(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // rd gets c
                    hcu.control_registerFileSel.charge(
                            WordUtils.intField((int) hcu.instruction, 11, 15));
                    hcu.control_register_c.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;
                }
            }}),
        slt_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // c gets a + immediate
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_b.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.SET_LESS_THAN.word());
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = add_WB;
                }
            }}),
        addiu_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // c gets a + immediate
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.output2.charge(hcu.immed);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PLUS.word());
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = addiu_WB;
                }
            }}),
        addiu_WB(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // rd gets c
                    hcu.control_registerFileSel.charge(hcu.rt);
                    hcu.control_register_c.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;
                }
            }}),
        ori_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // c gets a | immediate
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.output2.charge(hcu.immed);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.INCLUSIVE_OR.word());
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = ori_WB;
                }
            }}),
        ori_WB(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // rd gets c
                    hcu.control_registerFileSel.charge(hcu.rt);
                    hcu.control_register_c.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;
                }
            }}),
        sub_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // c gets a + immediate
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_b.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.MINUS.word());
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = add_WB;
                }
            }}),
        divu_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_b.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_mdu.charge(MultiplyDivideUnit.ControlWord.DIVIDE.word());
                    hcu.control_register_hi.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.control_register_lo.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;
                }
            }}),
        mult_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_b.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_mdu.charge(MultiplyDivideUnit.ControlWord.MULTIPLY.word());
                    hcu.control_register_hi.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.control_register_lo.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;

                }
            }});

        private final Step[] steps;

        private AluInstructionState(Step[] steps) {
            this.steps = steps;
        }

        @Override
        public Step[] getSteps() {
            return steps;
        }
    }

    private enum LoadStoreInstructionState implements State {

        lui_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    int lowerHalfWord = WordUtils.intLowerHalf((int) hcu.immed);
                    hcu.output1.charge(lowerHalfWord);
                    hcu.output2.charge(16);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.LEFT_SHIFT.word());
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = lui_WB;
                }
            }}),
        lui_WB(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    hcu.control_registerFileSel.charge(hcu.rt);
                    hcu.control_register_c.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;
                }
            }}),
        lw_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.output2.charge(hcu.immed);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PLUS.word());
                    hcu.control_register_mar.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = lw_MEM;
                }
            }}),
        lw_MEM(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    hcu.control_register_mar.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_memory.charge(Memory.ControlWord.RETRIEVE.word);
                    hcu.nextState = lw_WB;
                }
            }}),
        lw_WB(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    hcu.output1.charge(hcu.instruction);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PASS_INPUT_1.word());
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_c.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_registerFileSel.charge(hcu.rt);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;
                }
            }}),
        lb_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.output2.charge(hcu.immed);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PLUS.word());
                    hcu.control_register_mar.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = lb_MEM;
                }
            }}),
        lb_MEM(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    hcu.control_register_mar.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_memory.charge(Memory.ControlWord.RETRIEVE.word);
                    hcu.nextState = lb_WB;
                }
            }}),
        lb_WB(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // read the val from IR
                    hcu.control_register_instruction.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {

                    hcu.control_register_instruction.charge(Register.ControlWord.WRITE_OUTPUT_A.word);

                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    //
                    hcu.output1.charge(
                            WordUtils.intToBytes((int) hcu.obs_instructionWord)[0]);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PASS_INPUT_1.word());
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_registerFileSel.charge(hcu.rt);
                    hcu.control_register_c.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;
                }
            }}),
        sw_EX1(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.output2.charge(hcu.immed);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PLUS.word());
                    hcu.control_register_mar.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = sw_EX2;
                }
            }}),
        sw_EX2(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_b.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PASS_INPUT_2.word());
                    hcu.control_mdrMultiplexer.charge(Multiplexer.ControlWord.USE_INPUT_A.word);
                    hcu.control_register_mdr.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = sw_MEM;
                }
            }}),
        sw_MEM(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_mar.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_mdr.charge(Register.ControlWord.WRITE_OUTPUT_B.word);
                    hcu.control_memory.charge(Memory.ControlWord.STORE.word);
                    hcu.nextState = GenericState.IF1;
                }
            }}),
        sb_EX1(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.output2.charge(hcu.immed);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PLUS.word());
                    hcu.control_register_mar.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = sb_EX2;
                }
            }}),
        sb_EX2(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_b.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PASS_INPUT_2.word());
                    hcu.control_mdrMultiplexer.charge(Multiplexer.ControlWord.USE_INPUT_A.word);
                    hcu.control_register_mdr.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = sb_MEM;
                }
            }}),
        sb_MEM(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_mar.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_mdr.charge(Register.ControlWord.WRITE_OUTPUT_B.word);
                    hcu.control_memory.charge(Memory.ControlWord.STORE_BYTE.word);
                    hcu.nextState = GenericState.IF1;

                }
            }});

        private final Step[] steps;

        private LoadStoreInstructionState(Step[] steps) {
            this.steps = steps;
        }

        @Override
        public Step[] getSteps() {
            return steps;
        }
    }

    private enum BranchInstructionState implements State {

        bgtz_EX1(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.output2.charge(0);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.SET_GREATER_THAN.word());
                    hcu.control_register_temp.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = bgtz_EX2;
                }
            }}),
        bgtz_EX2(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.output1.charge(hcu.immed);
                    hcu.control_register_temp.charge(Register.ControlWord.WRITE_OUTPUT_B.word);
                    hcu.control_mdu.charge(MultiplyDivideUnit.ControlWord.MULTIPLY.word());
                    hcu.control_register_lo.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    hcu.control_register_lo.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_temp.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = bgtz_EX3;
                }
            }}),
        bgtz_EX3(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.output1.charge(4);
                    hcu.control_register_temp.charge(Register.ControlWord.WRITE_OUTPUT_B.word);
                    hcu.control_mdu.charge(MultiplyDivideUnit.ControlWord.MULTIPLY.word());
                    hcu.control_register_lo.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_lo.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_temp.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = bgtz_EX4;
                }
            }}),
        bgtz_EX4(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_pc.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_temp.charge(Register.ControlWord.WRITE_OUTPUT_B.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PLUS.word());
                }
            },
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.REPEAT.word());
                    hcu.control_register_pc.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;

                }
            }}),
        beq_EX1(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_b.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.SET_EQUAL_TO.word());
                    hcu.control_register_temp.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = bgtz_EX2;
                }
            }}),
        bne_EX1(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_b.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.SET_NOT_EQUAL_TO.word());
                    hcu.control_register_temp.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = bgtz_EX2;
                }
            }}),
        blez_EX1(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.output2.charge(1);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.SET_LESS_THAN.word());
                    hcu.control_register_temp.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = bgtz_EX2;
                }
            }}),
        j_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.output1.charge(WordUtils.intField((int) hcu.instruction, 0, 25));
                    hcu.output2.charge(4);
                    hcu.control_mdu.charge(MultiplyDivideUnit.ControlWord.MULTIPLY.word());
                    hcu.control_register_lo.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_lo.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_pc.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;
                }
            }}),
        jr_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    //hcu.output2.charge(new Word(4));
                    //hcu.mdu.control_mduOp(MultiplyDivideUnit.MDUOp.MULTIPLY);
                    //                hcu.lo.control_read();
                    //                hcu.lo.control_write(0);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PASS_INPUT_1.word());
                    hcu.control_register_pc.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;

                }
            }}),
        jal_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_pc.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_alu.charge(ArithmeticLogicUnit.ControlWord.PASS_INPUT_1.word());
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_c.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_registerFileSel.charge(31);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.READ_INPUT.word);
                    hcu.nextState = j_EX;
                }
            }});

        private final Step[] steps;

        private BranchInstructionState(Step[] steps) {
            this.steps = steps;
        }

        @Override
        public Step[] getSteps() {
            return steps;
        }
    }

    private enum MiscellaneousState implements State {

        mfhi_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_hi.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_c.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_registerFileSel.charge(WordUtils.intField((int) hcu.instruction, 11, 15));
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;
                }
            }}),
        mflo_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_lo.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_register_c.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {
                    hcu.control_register_c.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    hcu.control_registerFileSel.charge(WordUtils.intField((int) hcu.instruction, 11, 15));
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.READ_INPUT.word);
                    hcu.nextState = GenericState.IF1;

                }
            }}),
        syscall_EX(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // "a" gets RF[2].
                    hcu.control_registerFileSel.charge(2);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.OUTPUT_A.word);
                    hcu.control_register_a.charge(Register.ControlWord.READ_INPUT.word);
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    // "a" writes.
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    // systemCallCode value from "a".
                    hcu.systemCallCode = (int) hcu.output1.getWord();
                    hcu.syscallState = SyscallState.READ_CODE;
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // "a" gets RF[4]
                    hcu.control_registerFileSel.charge(4);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.OUTPUT_A.word);
                    hcu.control_register_a.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.syscallState = null;
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    // "a" writes.
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    // a0 value from "A".
                    hcu.syscallState = SyscallState.READ_A0;
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // "a" gets RF[5].
                    hcu.control_registerFileSel.charge(5);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.OUTPUT_A.word);
                    hcu.control_register_a.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.syscallState = null;
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    // "a" writes.
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    // a1 value from "a".
                    hcu.syscallState = SyscallState.READ_A1;
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // "a" gets RF[6].
                    hcu.control_registerFileSel.charge(6);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.OUTPUT_A.word);
                    hcu.control_register_a.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.syscallState = null;
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    // "a" writes.
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    // a2 value from "a".
                    hcu.syscallState = SyscallState.READ_A2;
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // "a" gets RF[7].
                    hcu.control_registerFileSel.charge(7);
                    hcu.control_registerFileRw.charge(RegisterFile.ControlWord.OUTPUT_A.word);
                    hcu.control_register_a.charge(Register.ControlWord.READ_INPUT.word);
                    hcu.syscallState = null;
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    // "a" writes.
                    hcu.control_register_a.charge(Register.ControlWord.WRITE_OUTPUT_A.word);
                    // a2 value from "a".
                    hcu.syscallState = SyscallState.READ_A3;
                }
            },
            new Step() {

                @Override
                public void execute(HardwiredControlUnit hcu) {
                    LOGGER.debug("Syscall with code: "
                            + hcu.systemCallCode
                            + " (" + hcu.a0 + ", " + hcu.a1 + ", "
                            + hcu.a2 + ", " + hcu.a3 + ").");
                    hcu.syscallState = null;

                    if (hcu.systemCallCode == 1) {
                        System.out.print(hcu.a0);
                        hcu.nextState = GenericState.IF1;
                    } else if (hcu.systemCallCode == 4) {
                        int i = 0;
                        boolean nullTerminated = false;
                        StringBuilder sb = new StringBuilder("[SimCom - sys call] sout \"");

                        while (!nullTerminated) {
                            int address = hcu.a0 + i;
                            int subaddress = address - (2 * (address % 4)) + 3;
                            int character = hcu.debugMemoryRef.retrieveByte(subaddress);
                            if (character == 0) {
                                nullTerminated = true;
                            } else {
                                sb.append((char) character);
                                i++;
                            }
                        }
                        sb.append("\"");
                        hcu.stringOutput.set(sb.toString());
                        hcu.stringOutputHook.activate();
                        hcu.nextState = GenericState.IF1;
                    } else if (hcu.systemCallCode == 5) {
                        hcu.nextState = MiscellaneousState.READ_INPUT;
                    } else if (hcu.systemCallCode == 10) {
                        hcu.nextState = GenericState.EXIT;
                        hcu.stringOutput.set("[SimCom - sys call] Reached end of program.");
                        hcu.stringOutputHook.activate();
                    } else {
                        throw new IllegalStateException(
                                "Syscall with code " + hcu.systemCallCode);
                    }
                }
            }}),
        READ_INPUT(new Step[]{
            new Step() {
                @Override
                public void execute(HardwiredControlUnit hcu) {

                    // "a" gets RF[2].
                    if (hcu.readingNumber) {
                        hcu.debugCRef.setWord(hcu.number);
                        hcu.control_register_c.charge(RegisterFile.ControlWord.OUTPUT_A.word);
                        hcu.control_registerFileSel.charge(2);
                        hcu.control_registerFileRw.charge(RegisterFile.ControlWord.READ_INPUT.word);
                        hcu.nextState = GenericState.IF1;
                    } else {
                        hcu.nextState = READ_INPUT;
                    }
                }
            }});

        private final Step[] steps;

        private MiscellaneousState(Step[] steps) {
            this.steps = steps;
        }

        @Override
        public Step[] getSteps() {
            return steps;
        }
    }
}
