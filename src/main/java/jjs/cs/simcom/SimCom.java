/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.io.File;
import java.net.URISyntaxException;
import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.control.EntityProcessor;
import jjs.cs.control.hooks.StringInputHook;
import jjs.cs.control.hooks.StringOutputHook;
import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.SimulationModel;
import jjs.cs.simulation.data.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class SimCom extends SimulationModel {

    private static final Logger LOGGER
            = LoggerFactory.getLogger(SimCom.class);

    private static final long serialVersionUID = -1l;

    public static final int WORD_LENGTH = 32;

    public static final float COMPONENT_DEPTH = 1.5f;
    public static final float COMPONENT_POS_Z = 0.5f;

    public static final float GRID_STEP = 6;

    private Executable debugEx;

    // Circuit board.
    private final PrintedCircuitBoard pcb;

    // Data buses.
    private final MultiConPbw bus_s1;
    private final MultiConPbw bus_s2;
    private final ParallelBusWire bus_mduHi;
    private final ParallelBusWire bus_mduLo;
    private final MultiConPbw bus_dest;
    private final ParallelBusWire bus_registerFileToA;
    private final ParallelBusWire bus_registerFileToB;
    private final ParallelBusWire bus_cToRegisterFile;
    private final ParallelBusWire bus_marToMemory;
    private final ParallelBusWire bus_mdrMultiplexerToMdr;
    private final ParallelBusWire bus_mdrToMemory;
    private final MultiConPbw bus_mem;
    private final ParallelBusWire bus_instruction;

    // Control buses.
    private final ParallelBusWire control_mdu;
    private final RegisterControlPbw control_register_hi;
    private final RegisterControlPbw control_register_lo;
    private final ParallelBusWire control_alu;
    private final ParallelBusWire control_registerFileRw;
    private final ParallelBusWire control_registerFileSel;
    private final RegisterControlPbw control_register_a;
    private final RegisterControlPbw control_register_b;
    private final RegisterControlPbw control_register_c;
    private final RegisterControlPbw control_register_temp;
    private final RegisterControlPbw control_register_pc;
    private final RegisterControlPbw control_register_mar;
    private final RegisterControlPbw control_register_mdr;
    private final ParallelBusWire control_mdrMultiplexer;
    private final ParallelBusWire control_memory;
    private final RegisterControlPbw control_register_instruction;

    // Other components.
    private final HardwiredControlUnit controlUnit;
    private final MultiplyDivideUnit mdu;
    private final Register register_hi;
    private final Register register_lo;
    private final ArithmeticLogicUnit alu;
    private final RegisterFile registerFile;
    private final Register register_a;
    private final Register register_b;
    private final Register register_c;
    private final Register register_temp;
    private final Register register_pc;
    private final Register register_mar;
    private final Multiplexer mdrMultiplexer;
    private final Register register_mdr;
    private final Memory memory;
    private final Register register_instruction;

    // Input.
    private final InputReader inputReader;
    private Executable currentEx;

    public SimCom() {
        super(new HookFile());

        this.pcb = new PrintedCircuitBoard(
                .5f * GRID_STEP, 0f * GRID_STEP,
                21f * GRID_STEP, 32f * GRID_STEP);
        this.pcb.addHooks(hookFile);

        // Initialise data buses.
        this.bus_s1 = new MultiConPbw(32, "S1", -1.5f * GRID_STEP, 3.5f * GRID_STEP, true, 23f * GRID_STEP);
        this.bus_s2 = new MultiConPbw(32, "S2", -3.5f * GRID_STEP, 3.5f * GRID_STEP, true, 23f * GRID_STEP);
        this.bus_mduHi = new ParallelBusWire(32, "MduHi");
        this.bus_mduLo = new ParallelBusWire(32, "MduLo");
        this.bus_dest = new MultiConPbw(32, "Dest", 9.5f * GRID_STEP, 4f * GRID_STEP, true, 22f * GRID_STEP);
        this.bus_registerFileToA = new ParallelBusWire(32, "RfToA");
        this.bus_registerFileToB = new ParallelBusWire(32, "RfToB");
        this.bus_cToRegisterFile = new ParallelBusWire(32, "cToRf");
        this.bus_marToMemory = new ParallelBusWire(32, "marToM");
        this.bus_mdrMultiplexerToMdr = new ParallelBusWire(32, "MpxMdr");
        this.bus_mdrToMemory = new ParallelBusWire(32, "aasdd");
        this.bus_mem = new MultiConPbw(32, "Mem", 1f * GRID_STEP, -15f * GRID_STEP, false, 18f * GRID_STEP);
        this.bus_instruction = new ParallelBusWire(32, "Instruction");

        // Initialise other components.
        // Multiply divide unit (MDU).
        this.control_mdu = new ParallelBusWire(
                MultiplyDivideUnit.CONTROL_SIGNAL_CHANNELS, "control_mdu");
        this.mdu = new MultiplyDivideUnit(3f * GRID_STEP, 13f * GRID_STEP);
        this.mdu.addHooks(hookFile);
        this.mdu.connect(
                bus_s1.newTerminus(),
                bus_s2.newTerminus(),
                bus_mduHi.getTerminusA(),
                bus_mduLo.getTerminusA(),
                control_mdu.getTerminusB());

        // Register "hi".
        this.control_register_hi = new RegisterControlPbw("control_register_hi");
        this.register_hi = new Register(
                7f * GRID_STEP, 14.5f * GRID_STEP, "Hi", true);
        this.register_hi.addHooks(hookFile);
        this.register_hi.connect(
                this.bus_mduHi.getTerminusB(),
                this.bus_dest.getTerminusA(),
                null,
                this.control_register_hi.getTerminusB());

        // Register "lo".
        this.control_register_lo = new RegisterControlPbw("control_register_lo");
        this.register_lo = new Register(
                7f * GRID_STEP, 11.5f * GRID_STEP, "Lo", true);
        this.register_lo.addHooks(hookFile);
        this.register_lo.connect(
                this.bus_mduLo.getTerminusB(),
                this.bus_dest.newTerminus(),
                null,
                this.control_register_lo.getTerminusB());

        // Arithmetic logic unit (ALU).
        this.control_alu = new ParallelBusWire(
                ArithmeticLogicUnit.CONTROL_CHANNELS, "control_alu");
        this.alu = new ArithmeticLogicUnit(
                3f * GRID_STEP,
                8f * GRID_STEP);
        this.alu.addHooks(hookFile);
        this.alu.connect(
                this.bus_s1.newTerminus(),
                this.bus_s2.newTerminus(),
                this.bus_dest.newTerminus(),
                this.control_alu.getTerminusB());

        // Register file.
        this.control_registerFileRw
                = new ParallelBusWire(RegisterFile.CONTROL_RW_CHANNELS,
                        "control_registerFileRw");
        this.control_registerFileSel
                = new ParallelBusWire(RegisterFile.CONTROL_SEL_CHANNELS,
                        "control_registerFileSel");
        this.registerFile = new RegisterFile(
                4f * GRID_STEP, 2.5f * GRID_STEP);
        this.registerFile.connect(
                this.bus_cToRegisterFile.getTerminusB(),
                this.bus_registerFileToA.getTerminusA(),
                this.bus_registerFileToB.getTerminusA(),
                this.control_registerFileRw.getTerminusB(),
                this.control_registerFileSel.getTerminusB());
        this.registerFile.addHooks(hookFile);

        // Register "a".
        this.control_register_a = new RegisterControlPbw("control_register_a");
        this.register_a = new Register(
                1f * GRID_STEP, 3.5f * GRID_STEP, "A", false);
        this.register_a.addHooks(hookFile);
        this.register_a.connect(
                // Input
                this.bus_registerFileToA.getTerminusB(),
                // Outputs
                this.bus_s1.newTerminus(),
                null,
                // Control
                this.control_register_a.getTerminusB());

        // Register "b".
        this.control_register_b = new RegisterControlPbw("control_register_b");
        this.register_b = new Register(
                1f * GRID_STEP, 1.5f * GRID_STEP, "B", false);
        this.register_b.addHooks(hookFile);
        this.register_b.connect(
                // Input
                this.bus_registerFileToB.getTerminusB(),
                // Outputs
                this.bus_s2.newTerminus(),
                null,
                // Control
                this.control_register_b.getTerminusB());

        // Register "c".
        this.control_register_c = new RegisterControlPbw("control_register_c");
        this.register_c = new Register(
                7f * GRID_STEP, 2.5f * GRID_STEP, "C", false);
        this.register_c.addHooks(hookFile);
        this.register_c.connect(
                // Input
                this.bus_dest.newTerminus(),
                // Outputs
                this.bus_cToRegisterFile.getTerminusA(),
                null,
                // Control
                this.control_register_c.getTerminusB());

        // Register "temp".
        this.control_register_temp = new RegisterControlPbw(
                "control_register_temp");
        this.register_temp = new Register(
                2f * GRID_STEP, -2.5f * GRID_STEP, "temp", false);
        this.register_temp.addHooks(hookFile);
        this.register_temp.connect(
                // Input
                this.bus_dest.newTerminus(),
                // Outputs
                this.bus_s1.newTerminus(),
                this.bus_s2.newTerminus(),
                // Control
                this.control_register_temp.getTerminusB());

        // Register "pc".
        this.control_register_pc = new RegisterControlPbw(
                "control_register_pc");
        this.register_pc = new Register(
                2f * GRID_STEP, -4.5f * GRID_STEP, "pc", false);
        this.register_pc.addHooks(hookFile);
        this.register_pc.connect(
                // Input
                this.bus_dest.newTerminus(),
                // Outputs
                this.bus_s1.newTerminus(),
                this.bus_s2.newTerminus(),
                // Control
                this.control_register_pc.getTerminusB());

        // Register "mar".
        this.control_register_mar = new RegisterControlPbw(
                "control_register_mar");
        register_mar = new Register(
                2f * GRID_STEP, -6.5f * GRID_STEP, "mar", false);
        register_mar.addHooks(hookFile);
        this.register_mar.connect(
                // Input
                this.bus_dest.newTerminus(),
                // Outputs
                this.bus_s1.newTerminus(),
                this.bus_s2.newTerminus(),
                // Control
                this.control_register_mar.getTerminusB());

        // Multiplexer (used with "mdr" register).
        this.control_mdrMultiplexer = new ParallelBusWire(
                Multiplexer.CONTROL_CHANNELS, "control_mdrMultiplexer");
        this.mdrMultiplexer = new Multiplexer(
                6f * GRID_STEP, -7.5f * GRID_STEP);
        this.mdrMultiplexer.addHooks(hookFile);
        this.mdrMultiplexer.connect(
                this.bus_dest.newTerminus(),
                this.bus_mem.newTerminus(),
                this.bus_mdrMultiplexerToMdr.getTerminusA(),
                this.control_mdrMultiplexer.getTerminusB());

        // Register "mdr".
        this.control_register_mdr = new RegisterControlPbw(
                "control_register_mdr");
        this.register_mdr = new Register(
                2f * GRID_STEP, -8.5f * GRID_STEP, "mdr", false);
        this.register_mdr.addHooks(hookFile);
        this.register_mdr.connect(
                this.bus_mdrMultiplexerToMdr.getTerminusB(),
                this.bus_s1.newTerminus(),
                this.bus_s2.newTerminus(),
                this.control_register_mdr.getTerminusB());

        // Main memory.
        this.control_memory = new ParallelBusWire(
                Memory.CONTROL_CHANNELS, "control_memory");
        this.memory = new Memory(1f * GRID_STEP, -12f * GRID_STEP);
        this.memory.addHooks(hookFile);
        this.memory.connect(
                this.bus_s1.newTerminus(),
                this.bus_s2.newTerminus(),
                this.bus_mem.newTerminus(),
                this.control_memory.getTerminusB());

        // Register "instruction".
        this.control_register_instruction = new RegisterControlPbw(
                "control_register_instruction");
        this.register_instruction = new Register(
                -7f * GRID_STEP, 1.5f * GRID_STEP, "instruction", true);
        this.register_instruction.addHooks(hookFile);
        this.register_instruction.connect(
                this.bus_mem.newTerminus(),
                this.bus_instruction.getTerminusA(),
                null,
                this.control_register_instruction.getTerminusB());

        // Control unit.
        this.controlUnit = new HardwiredControlUnit(
                -7f * GRID_STEP, 9f * GRID_STEP,
                memory, register_c);
        this.controlUnit.addHooks(hookFile);
        this.controlUnit.connect(
                bus_instruction.getTerminusB(),
                bus_s1.newTerminus(),
                bus_s2.newTerminus(),
                control_mdu.getTerminusA(),
                control_register_hi.getTerminusA(),
                control_register_lo.getTerminusA(),
                control_alu.getTerminusA(),
                control_registerFileRw.getTerminusA(),
                control_registerFileSel.getTerminusA(),
                control_register_a.getTerminusA(),
                control_register_b.getTerminusA(),
                control_register_c.getTerminusA(),
                control_register_temp.getTerminusA(),
                control_register_pc.getTerminusA(),
                control_register_mar.getTerminusA(),
                control_mdrMultiplexer.getTerminusA(),
                control_register_mdr.getTerminusA(),
                control_memory.getTerminusA(),
                control_register_instruction.getTerminusA());

        // Add bus hooks.
        this.bus_s1.addHooks(hookFile);
        this.bus_s2.addHooks(hookFile);
        this.bus_mduHi.addHooks(hookFile);
        this.bus_mduLo.addHooks(hookFile);
        this.bus_dest.addHooks(hookFile);
        this.bus_registerFileToA.addHooks(hookFile);
        this.bus_registerFileToB.addHooks(hookFile);
        this.bus_cToRegisterFile.addHooks(hookFile);
        this.bus_marToMemory.addHooks(hookFile);
        this.bus_mdrMultiplexerToMdr.addHooks(hookFile);
        this.bus_mdrToMemory.addHooks(hookFile);
        this.bus_mem.addHooks(hookFile);
        this.bus_instruction.addHooks(hookFile);

        this.control_mdu.addHooks(hookFile);
        this.control_register_hi.addHooks(hookFile);
        this.control_register_lo.addHooks(hookFile);
        this.control_alu.addHooks(hookFile);
        this.control_registerFileRw.addHooks(hookFile);
        this.control_registerFileSel.addHooks(hookFile);
        this.control_register_a.addHooks(hookFile);
        this.control_register_b.addHooks(hookFile);
        this.control_register_c.addHooks(hookFile);
        this.control_register_temp.addHooks(hookFile);
        this.control_register_pc.addHooks(hookFile);
        this.control_register_mar.addHooks(hookFile);
        this.control_mdrMultiplexer.addHooks(hookFile);
        this.control_register_mdr.addHooks(hookFile);
        this.control_memory.addHooks(hookFile);
        this.control_register_instruction.addHooks(hookFile);

        this.inputReader = new InputReader();
        this.inputReader.addHooks(hookFile);
    }

    private void loadProg(String filename) throws URISyntaxException {
        wipe();
        this.debugEx = Executable.load(new File(this.getClass().getResource("/programs/" + filename).toURI()));

        LOGGER.info("Parsing program.");
        boolean moreLines = true;
        int i = 0;
        int memAddr = 0;
        while (moreLines) {
            final int word = this.debugEx.getWord(i++);
            LOGGER.info("line " + i + ": " + Integer.toHexString(word));
            memory.storeWord(memAddr, word);
            memAddr += 4;
            if (word == 0) {
                moreLines = false;
            }
        }
    }

    private void wipe() {
        memory.wipe();
        registerFile.wipe();
        register_pc.setWord(0);
        controlUnit.reset();
    }

    private class InputReader implements Entity {

        private final AtomicReference<String> inputStringRef;
        private final AtomicReference<String> outputStringRef;
        private StringInputHook inputHook;
        private StringOutputHook outputHook;
        private EntityProcessor.EntityHook entHook;

        public InputReader() {
            this.inputStringRef = new AtomicReference<>();
            this.outputStringRef = new AtomicReference<>();
        }

        @Override
        public void addHooks(HookFile hookFile) {
            this.inputHook = new StringInputHook(hookFile.newId(), inputStringRef);
            this.outputHook = new StringOutputHook(hookFile.newId(), outputStringRef);
            this.entHook = new EntityProcessor.EntityHook(hookFile.newId(), this);
            hookFile.addHook(inputHook, StringInputHook.class);
            hookFile.addHook(outputHook, StringOutputHook.class);
            hookFile.addHook(entHook, EntityProcessor.EntityHook.class);
        }

        @Override
        public void observe() {
        }

        @Override
        public void update(long deltaNanoseconds) {
            if (controlUnit.exit()) {
                memory.wipe();
                registerFile.wipe();
                register_pc.setWord(0);
                controlUnit.reset();
            }

            if (this.inputHook.isActive()) {
                final String input = this.inputStringRef.get();
                if (input.matches("run .+")) {
                    final String fileName = input.substring(4);
                    String output = "[SimCom] Loading \"" + fileName + "\" ... ";
                    try {

                        loadProg(fileName);
                    } catch (Exception ex) {
                        output += "failed (ex: " + ex + ").";
                    }
                    this.outputStringRef.set(output);
                    this.outputHook.activate();
                }
                this.inputHook.deactivate();
            }
        }

    }
}
