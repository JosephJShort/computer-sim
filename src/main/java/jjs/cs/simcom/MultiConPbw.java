/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.util.ArrayList;
import jjs.cs.control.EntityProcessor;
import jjs.cs.control.PickingProcessor;
import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.data.BoxRenderModel;
import jjs.cs.simulation.data.RenderModel;
import jjs.cs.simulation.data.WorldPosition;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class MultiConPbw extends ParallelBusWire {

    private final ArrayList<Terminus> termini;

    private final WorldPosition worldPosition;
    private final RenderModel renderModel;

    private final boolean rotated;
    private final float startX;
    private final float startY;
    private final float endX;
    private final float endY;

    public MultiConPbw(
            int channels, String name,
            float posX,
            float posY, boolean rotated, float length) {
        super(channels, name);

        this.termini = new ArrayList<>();
        this.termini.add(terminusA);
        this.termini.add(terminusB);

        this.rotated = rotated;
        this.worldPosition = new WorldPosition(
                posX, posY, SimCom.COMPONENT_POS_Z,
                0, 0, 0,
                rotated ? ParallelBusWire.SINGLE_CHANNEL_WIDTH * ((float) channels) : length,
                rotated ? length : ParallelBusWire.SINGLE_CHANNEL_WIDTH * ((float) channels),
                SimCom.COMPONENT_DEPTH / 2f);
        this.renderModel = new BoxRenderModel("simcom_bus.png", false);
        this.startX = (rotated ? posY : posX) - length / 2f;
        this.endX = (rotated ? posY : posX) + length / 2f;
        this.startY = (rotated ? posX : posY) - length / 2f;
        this.endY = (rotated ? posX : posY) + length / 2f;
    }

    public Terminus newTerminus() {
        final Terminus instance;
        instance = new Terminus();
        this.termini.add(instance);
        return instance;
    }

    @Override
    public void update(long deltaNanoseconds) {
        long collidedWord = 0;
        for (Terminus terminus : termini) {
            if (terminus.observed_connectorCharged) {
                collidedWord |= terminus.observerd_connectorWord;
            }
        }
        word = collidedWord & bitMask;
    }

    @Override
    public void observe() {
        for (Terminus terminus : termini) {
            terminus.observe();
        }
    }

    @Override
    public void addHooks(HookFile hookFile) {
        hookFile.addHook(
                new EntityProcessor.EntityHook(
                        hookFile.newId(), this),
                EntityProcessor.EntityHook.class);

        {
            final SegmentModel mainSeg
                    = new SegmentModel(worldPosition, renderModel);
            mainSeg.addHooks(hookFile);
            this.segmentModels.add(mainSeg);

            final PickingProcessor.H pickingHook = new PickingProcessor.H(
                    hookFile.newId(), this, worldPosition, this);
            hookFile.addHook(pickingHook, PickingProcessor.H.class);
        }

        final float xPos = this.worldPosition.position.x;
        final float yPos = this.worldPosition.position.y;
        for (Terminus terminus : termini) {
            if (terminus.pbwConnector != null) {
                final WorldPosition origin
                        = terminus.pbwConnector.getWorldPosition();
                final WorldPosition dest;
                if (rotated) {
                    dest = new WorldPosition(
                            xPos,
                            origin.position.y < startY
                            ? startY : origin.position.y > endY
                            ? endY : origin.position.y,
                            SimCom.COMPONENT_POS_Z,
                            0, 0, 0,
                            0, 0, 0);
                } else {
                    dest = new WorldPosition(
                            origin.position.x < startX
                            ? startX : origin.position.x > endX
                            ? endX : origin.position.x,
                            yPos,
                            SimCom.COMPONENT_POS_Z,
                            0, 0, 0,
                            0, 0, 0);

                }
                createSegs(origin, dest, hookFile);
            }
        }
    }
}
