/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.simulation.HookFile;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class Register extends BasicInspectableEntity {

    public static final int CONTROL_CHANNELS = 2;

    public static enum ControlWord {

        NOTHING(0b00),
        READ_INPUT(0b01),
        WRITE_OUTPUT_A(0b10),
        WRITE_OUTPUT_B(0b11);
        final long word;

        private ControlWord(long word) {
            this.word = word;
        }

    }

    // Connections to other entities.
    private final PbwConnector input;
    private final PbwConnector outputA;
    private final PbwConnector outputB;
    private final PbwConnector control;

    // State.
    private long word;

    // Observation variables.
    private long observed_input = 0;
    private long observed_outputA = 0;
    private long observed_outputB = 0;
    private long observed_control = 0;
    private ControlWord observed_controlAction = ControlWord.NOTHING;

    private final String name;

    private final AtomicReference<String> prop_control;
    private final AtomicReference<String> prop_controlAction;
    private final AtomicReference<String> prop_word;

    public Register(float posX, float posY, String name, boolean flipConnectors) {
        super(
                posX,
                posY,
                SimCom.COMPONENT_POS_Z,
                2 * SimCom.GRID_STEP,
                1 * SimCom.GRID_STEP,
                SimCom.COMPONENT_DEPTH,
                "simcom_register.png");

        this.input = PbwConnector.createAttatched(
                worldPosition, true, !flipConnectors, 2, 1, 32, "Input");
        this.outputA = PbwConnector.createAttatched(
                worldPosition, true, flipConnectors, 4, 1, 32, "Output A");
        this.outputB = PbwConnector.createAttatched(
                worldPosition, true, flipConnectors, 4, 3, 32, "Output B");
        this.control = PbwConnector.createAttatched(
                worldPosition, false, true, 2, 1, CONTROL_CHANNELS, "Control");
        this.name = name;

        // Default state.
        this.word = 0;

        // Default observation.
        this.observed_input = 0;
        this.observed_outputA = 0;
        this.observed_outputB = 0;
        this.observed_control = 0;

        // Properties.
        this.prop_control = new AtomicReference<>();
        this.prop_controlAction = new AtomicReference<>();
        this.prop_word = new AtomicReference<>();
        this.properties.put("!0. Control signal", prop_control);
        this.properties.put("!1. Control action", prop_controlAction);
        this.properties.put("!2. Word", prop_word);
        this.properties.putAll(this.input.getProperties());
        this.properties.putAll(this.outputA.getProperties());
        this.properties.putAll(this.outputB.getProperties());
        this.properties.putAll(this.control.getProperties());
    }

    public void connect(
            ParallelBusWire.Terminus t_input,
            ParallelBusWire.Terminus t_outputA,
            ParallelBusWire.Terminus t_outputB,
            ParallelBusWire.Terminus t_control
    ) {
        this.input.connect(t_input);
        this.outputA.connect(t_outputA);
        if (t_outputB != null) {
            this.outputB.connect(t_outputB);
        }
        this.control.connect(t_control);
    }

    @Override
    public void addHooks(HookFile hookFile) {
        super.addHooks(hookFile);
        this.input.addHooks(hookFile);
        this.outputA.addHooks(hookFile);
        this.outputB.addHooks(hookFile);
        this.control.addHooks(hookFile);
    }

    @Override
    public void observe() {
        this.observed_input = input.getWord();
        this.observed_outputA = outputA.getWord();
        this.observed_outputB = outputB.getWord();
        this.observed_control = control.getWord();
        this.observed_controlAction
                = observed_control < ControlWord.values().length
                ? ControlWord.values()[(int) observed_control] : null;
    }

    @Override
    public void update(long deltaNanoseconds) {
        this.outputA.removeCharge();
        this.outputB.removeCharge();
        if (observed_controlAction != null
                && observed_controlAction != ControlWord.NOTHING) {
            if (observed_controlAction == ControlWord.READ_INPUT) {
                this.word = observed_input;
            } else if (observed_controlAction == ControlWord.WRITE_OUTPUT_A) {
                this.outputA.charge(word);
            } else if (observed_controlAction == ControlWord.WRITE_OUTPUT_B) {
                this.outputB.charge(word);
            }
        }
    }

    @Override
    public Map<String, Object> getProperties() {
        return properties;
    }

    @Override
    public void updateProperties() {
        this.input.updateProperties();
        this.outputA.updateProperties();
        this.outputB.updateProperties();
        this.control.updateProperties();
        this.prop_control.set(String.valueOf(observed_control));
        this.prop_controlAction.set(observed_controlAction == null
                ? "[UNKNOWN]" : String.valueOf(observed_controlAction));
        this.prop_word.set(String.valueOf(word));
    }

    @Override
    public String getInspectableName() {
        return "Register \"" + name + "\"";
    }

    public void setWord(long word) {
        this.word = word;
    }
}
