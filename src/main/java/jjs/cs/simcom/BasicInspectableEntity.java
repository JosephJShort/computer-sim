/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.util.Map;
import java.util.TreeMap;
import jjs.cs.control.EntityProcessor;
import jjs.cs.control.PickingProcessor;
import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.RenderHook;
import jjs.cs.simulation.data.Inspectable;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public abstract class BasicInspectableEntity
        extends BasicEntity implements Inspectable {

    protected final String inspectableName;
    protected final Map<String, Object> properties;

    public BasicInspectableEntity(
            float posX,
            float posY,
            float posZ,
            float width,
            float height,
            float depth,
            String textureName) {
        super(posX, posY, posZ, width, height, depth, textureName);
        this.inspectableName
                = this.getClass().getSimpleName() + "-" + this.hashCode();
        this.properties = new TreeMap<>();
    }

    @Override
    public void addHooks(HookFile hookFile) {
        this.entityHook = new EntityProcessor.EntityHook(
                hookFile.newId(), this);
        this.renderHook = new RenderHook(
                hookFile.newId(), worldPosition, model);
        this.pickingHook = new PickingProcessor.H(
                hookFile.newId(), this, worldPosition, this);
        hookFile.addHook(entityHook, EntityProcessor.EntityHook.class);
        hookFile.addHook(renderHook, RenderHook.class);
        hookFile.addHook(pickingHook, PickingProcessor.H.class);
    }

    @Override
    public Map<String, Object> getProperties() {
        return properties;
    }

    @Override
    public String getInspectableName() {
        return inspectableName;
    }

}
