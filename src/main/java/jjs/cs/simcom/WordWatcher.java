/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 * @param <T>
 */
public class WordWatcher<T extends HasWord> implements HasWord {

    private final String name;
    private final T t;
    private final int channels;

    private long observed_word;
    private long word;

    public WordWatcher(String name, T t) {
        this.name = name;
        this.t = t;
        this.channels = t.channels();

    }

    public void observe() {
        observed_word = t.word();
    }

    public void update() {
        word = observed_word;
    }

    public String getName() {
        return name;
    }

    @Override
    public long word() {
        return word;
    }

    @Override
    public int channels() {
        return channels;
    }
}
