/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import jjs.cs.utils.WordUtils;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class Executable {

    private int[] words;
    private String[] comments;
    private final int size;
    private int sizeInBytes;

    private Executable(int size) {
        if (size < 1) {
            throw new IllegalArgumentException("size must positive. size: " + size);
        }
        this.size = size;
        this.sizeInBytes = size >> 2;
        this.words = new int[sizeInBytes];
        this.comments = new String[sizeInBytes];
    }

    public void setWord(int index, int value) {
        this.words[index] = value;
    }

    public int getWord(int index) {
        return this.words[index];
    }

    public int getSizeInBytes() {
        return this.sizeInBytes;
    }

    public int getSize() {
        return this.size;
    }

    public static Executable load(File file) {

        Executable instance = new Executable(4096);
        int wordsStored = 0;
        try {
            Scanner fileScanner = new Scanner(file);
            while (fileScanner.hasNextLine()) {
                String line = fileScanner.nextLine();
                // Trim comments (after ';').
                int ignoreAfter = line.indexOf(';');
                line = ignoreAfter > 0 ? line.substring(0, ignoreAfter).trim() : line.trim();
                int value = Long.valueOf(line, 16).intValue();
                int current = value;
                int firstOpcode = WordUtils.intField(current, 26, 31);
                int secondOpcode = WordUtils.intField(current, 0, 5);
                // Adjust branch statements to accomodate for PC incrementing.
                if (firstOpcode >= 0x04 && firstOpcode <= 0x07) {
                    instance.setWord(wordsStored, value - 1);
                } else {
                    instance.setWord(wordsStored, value);
                }
                wordsStored++;
            }
        } catch (FileNotFoundException ex) {
            throw new RuntimeException(ex);
        }
        return instance;
    }
}
