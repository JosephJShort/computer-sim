/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import jjs.cs.control.EntityProcessor;
import jjs.cs.control.PickingProcessor;
import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.RenderHook;
import jjs.cs.simulation.data.BoxRenderModel;
import jjs.cs.simulation.data.Entity;
import jjs.cs.simulation.data.RenderModel;
import jjs.cs.simulation.data.WorldPosition;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public abstract class BasicEntity implements Entity {

    protected final WorldPosition worldPosition;
    protected final RenderModel model;

    protected EntityProcessor.EntityHook entityHook;
    protected RenderHook renderHook;
    protected PickingProcessor.H pickingHook;

    public BasicEntity(
            float posX,
            float posY,
            float posZ,
            float width,
            float height,
            float depth,
            String textureName) {
        this.worldPosition = new WorldPosition(
                new Vector3f(posX, posY, posZ),
                new Vector3f(0, 0, 0),
                new Vector3f(width, height, depth));
        if (width < 0 || height < 0 || depth < 0) {
            throw new IllegalArgumentException(
                    "Neither width, depth nor height can be negative."
                    + " Given width, depth and height: "
                    + width + ", " + "," + depth + ", " + height + ".");
        }
        this.model = new BoxRenderModel(textureName, false);
    }

    @Override
    public void addHooks(HookFile hookFile) {
        this.entityHook = new EntityProcessor.EntityHook(
                hookFile.newId(), this);
        this.renderHook = new RenderHook(
                hookFile.newId(), worldPosition, model);
        this.pickingHook = new PickingProcessor.H(
                hookFile.newId(), this, worldPosition);
        hookFile.addHook(entityHook, EntityProcessor.EntityHook.class);
        hookFile.addHook(renderHook, RenderHook.class);
        hookFile.addHook(pickingHook, PickingProcessor.H.class);
    }
}
