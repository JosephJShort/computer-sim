/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.control.EntityProcessor;
import jjs.cs.simulation.HookFile;
import jjs.cs.simulation.RenderHook;
import jjs.cs.simulation.data.WorldPosition;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class PbwConnector extends BasicInspectableEntity {

    private final int channels;
    private ParallelBusWire.Terminus busTerminus;
    private boolean charged;
    private long word;

    // Observations.
    private long observed_word;

    // Properties.
    private final AtomicReference<String> prop_channels;
    private final AtomicReference<String> prop_connected;
    private final AtomicReference<String> prop_charged;
    private final AtomicReference<String> prop_word;

    private final String name;

    public PbwConnector(
            final float posX,
            final float posY,
            final float posZ,
            final float width,
            final float height,
            final float depth,
            final int channels,
            String name) {
        super(posX, posY, posZ, width, height, depth, "simcom_connector.png");

        if (channels < 1 || channels > 64) {
            throw new IllegalArgumentException(
                    "Must have 1 <= C <= 64 channels. C = " + channels);
        }

        // Initial state.
        this.channels = channels;
        this.busTerminus = null;
        this.charged = false;
        this.word = 0l;
        this.observed_word = 0l;

        // Properties.
        this.prop_channels = new AtomicReference<>(String.valueOf(channels));
        this.prop_connected = new AtomicReference<>();
        this.prop_charged = new AtomicReference<>();
        this.prop_word = new AtomicReference<>();
        this.properties.put(name + " - channels", this.prop_channels);
        this.properties.put(name + " - connected", this.prop_connected);
        this.properties.put(name + " - charged", this.prop_charged);
        this.properties.put(name + " - word", this.prop_word);

        this.name = name;
    }

    public static PbwConnector createAttatched(
            WorldPosition attatchedPosition,
            boolean offsetX,
            boolean offsetPositive,
            int faceDivisions,
            int divisionIndex,
            int channels,
            String name) {
        if (faceDivisions < 2
                || divisionIndex < 1
                || divisionIndex >= faceDivisions) {
            throw new IllegalArgumentException();
        }
        final PbwConnector instance;

        float scaleX = ParallelBusWire.SINGLE_CHANNEL_WIDTH * 1.1f * (float) channels;
        float scaleOther = SimCom.GRID_STEP / 4f;

        float posX = offsetX
                // If offset X.
                ? ((offsetPositive ? 0.5f : -0.5f)
                * attatchedPosition.scale.x)
                + ((offsetPositive ? 0.5f : -0.5f) * scaleOther)
                + attatchedPosition.position.x
                // If offset Y.
                : attatchedPosition.scale.x / faceDivisions
                * (float) divisionIndex
                + (attatchedPosition.position.x
                - attatchedPosition.scale.x / 2f);
        float posY = !offsetX
                // If offset Y.
                ? ((offsetPositive ? 0.5f : -0.5f)
                * attatchedPosition.scale.y)
                + ((offsetPositive ? 0.5f : -0.5f) * scaleOther)
                + attatchedPosition.position.y
                // If offset X.
                : attatchedPosition.scale.y / faceDivisions
                * (float) divisionIndex
                + (attatchedPosition.position.y
                - attatchedPosition.scale.y / 2f);
        float angle = (offsetX ? 90f : 0f) + (offsetPositive ? 0f : 180f);

        instance = new PbwConnector(
                posX, posY, attatchedPosition.position.z - attatchedPosition.scale.z / 4f,
                scaleX, scaleOther, attatchedPosition.scale.z / 2f,
                channels, name);
        instance.worldPosition.angle.z = angle;

        return instance;
    }

    @Override
    public void addHooks(HookFile hookFile) {

        // Is an entity.
        this.entityHook
                = new EntityProcessor.EntityHook(hookFile.newId(), this);
        hookFile.addHook(this.entityHook, EntityProcessor.EntityHook.class);

        // Is rendered.
        this.renderHook = new RenderHook(
                hookFile.newId(), this.worldPosition, this.model);
        hookFile.addHook(this.renderHook, RenderHook.class);
    }

    public boolean canConnect(ParallelBusWire.Terminus terminus) {
        return terminus.getChannels() == channels;
    }

    public void connect(ParallelBusWire.Terminus terminus) {
        if (terminus == null) {
            throw new NullPointerException("terminus: " + terminus);
        }
        if (this.busTerminus != null) {
            throw new IllegalStateException("Already connected to a terminus.");
        }
        if (!canConnect(terminus)) {
            throw new IllegalArgumentException("Can't connect. channels: "
                    + channels + ", terminus channels: "
                    + terminus.getChannels());
        }
        this.busTerminus = terminus;
        this.busTerminus.connect(this);
    }

    public void charge(long word) {
        this.charged = true;
        this.word = word;
    }

    public void removeCharge() {
        this.charged = false;
        this.word = 0l;
    }

    @Override
    public void observe() {
        observed_word = busTerminus == null ? 0 : busTerminus.getWord();
    }

    @Override
    public void update(long deltaNanoseconds) {
        if (busTerminus != null) {
            if (!charged) {
                word = observed_word;
            }
        }
    }

    public long getWord() {
        return word;
    }

    public int channels() {
        return channels;
    }

    @Override
    public String getInspectableName() {
        return inspectableName;
    }

    @Override
    public Map<String, Object> getProperties() {
        return properties;
    }

    @Override
    public void updateProperties() {
        this.prop_connected.set(String.valueOf(busTerminus != null));
        this.prop_charged.set(String.valueOf(charged));
        this.prop_word.set(String.valueOf(word));
    }

    public Observation makeObservation() {
        return new Observation(charged, word);
    }

    public static class Observation {

        public final boolean charged;
        public final long word;

        private Observation(boolean charged, long word) {
            this.charged = charged;
            this.word = word;
        }
    }

    public boolean isCharged() {
        return charged;
    }

    public ParallelBusWire.Terminus getBusTerminus() {
        return busTerminus;
    }

    public WorldPosition getWorldPosition() {
        return worldPosition;
    }
}
