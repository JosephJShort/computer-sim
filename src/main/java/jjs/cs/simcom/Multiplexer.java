/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.simulation.HookFile;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class Multiplexer extends BasicInspectableEntity {

    public static final int CONTROL_CHANNELS = 1;

    public static enum ControlWord {

        USE_INPUT_A(0b00),
        USE_INPUT_B(0b01);
        final long word;

        private ControlWord(long word) {
            this.word = word;
        }

    }

    private final PbwConnector inputA;
    private final PbwConnector inputB;
    private final PbwConnector output;
    private final PbwConnector control;

    private long obs_inputA;
    private long obs_inputB;
    private long obs_control;
    private ControlWord obs_controlAction;

    private final AtomicReference<String> property_controlSignal;
    private final AtomicReference<String> property_controlAction;

    public Multiplexer(
            float posX,
            float posY) {
        super(
                posX,
                posY,
                SimCom.COMPONENT_POS_Z,
                SimCom.GRID_STEP * 2f,
                SimCom.GRID_STEP * 3f,
                SimCom.COMPONENT_DEPTH,
                "simcom_tall.png");

        this.inputA = PbwConnector.createAttatched(
                worldPosition, true, true, 4, 1, 32, "Input A");
        this.inputB = PbwConnector.createAttatched(
                worldPosition, true, true, 4, 3, 32, "Input B");
        this.output = PbwConnector.createAttatched(
                worldPosition, true, false, 2, 1, 32, "Output");
        this.control = PbwConnector.createAttatched(
                worldPosition, false, true, 2, 1, CONTROL_CHANNELS, "Control");

        this.property_controlSignal = new AtomicReference<>();
        this.property_controlAction = new AtomicReference<>();
        this.properties.put("1. Control signal", this.property_controlSignal);
        this.properties.put("2. Control action", this.property_controlAction);
        this.properties.putAll(this.inputA.getProperties());
        this.properties.putAll(this.inputB.getProperties());
        this.properties.putAll(this.output.getProperties());

        this.obs_control = 0l;
        this.obs_controlAction = ControlWord.USE_INPUT_A;
    }

    public void connect(
            ParallelBusWire.Terminus inputA,
            ParallelBusWire.Terminus inputB,
            ParallelBusWire.Terminus output,
            ParallelBusWire.Terminus control) {

        this.inputA.connect(inputA);
        this.inputB.connect(inputB);
        this.output.connect(output);
        this.control.connect(control);
    }

    @Override
    public void addHooks(HookFile hookFile) {
        super.addHooks(hookFile);
        inputA.addHooks(hookFile);
        inputB.addHooks(hookFile);
        output.addHooks(hookFile);
        control.addHooks(hookFile);
    }

    @Override
    public void observe() {
        obs_inputA = inputA.getWord();
        obs_inputB = inputB.getWord();
        obs_control = control.getWord();
        obs_controlAction = obs_control < ControlWord.values().length
                ? ControlWord.values()[(int) obs_control] : null;
    }

    @Override
    public void updateProperties() {
        this.inputA.updateProperties();
        this.inputB.updateProperties();
        this.output.updateProperties();
        this.property_controlSignal.set(String.valueOf(obs_control));
        this.property_controlAction.set(obs_controlAction == null ? "[UNKNOWN]"
                : String.valueOf(obs_controlAction));
    }

    @Override
    public void update(long deltaNanoseconds) {
        if (obs_controlAction == ControlWord.USE_INPUT_A) {
            output.charge(obs_inputA);
        } else if (obs_controlAction == ControlWord.USE_INPUT_B) {
            output.charge(obs_inputB);
        }
    }

}
