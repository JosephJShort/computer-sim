/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.simulation.HookFile;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class RegisterFile extends BasicInspectableEntity {

    public static final int CONTROL_RW_CHANNELS = 2;
    public static final int CONTROL_SEL_CHANNELS = 5;

    public static enum ControlWord {

        NOTHING(0b00),
        READ_INPUT(0b01),
        OUTPUT_A(0b10),
        OUTPUT_B(0b11);

        public final long word;

        private ControlWord(long word) {
            this.word = word;
        }

    }

    public static final int WORD_CAPACITY = 32;

    // Connections to other entities.
    private final PbwConnector input;
    private final PbwConnector outputA;
    private final PbwConnector outputB;
    private final PbwConnector controlRw;
    private final PbwConnector controlSelect;

    // State.
    private final long[] words;

    // Observation variables.
    private long observed_input = 0;
    private long observed_outputA = 0;
    private long observed_outputB = 0;
    private long observed_controlRw = 0;
    private ControlWord observed_controlRwAction = null;
    private long observed_controlSel = 0;

    private final ArrayList<AtomicReference<String>> prop_words;

    public RegisterFile(
            float posX,
            float posY) {
        super(posX, posY, SimCom.COMPONENT_POS_Z,
                12f, 30f, SimCom.COMPONENT_DEPTH,
                "simcom_tall.png");

        this.input = PbwConnector.createAttatched(
                worldPosition, true, true, 2, 1, 32, "Input");
        this.outputA = PbwConnector.createAttatched(
                worldPosition, true, false, 3, 2, 32, "Output A");
        this.outputB = PbwConnector.createAttatched(
                worldPosition, true, false, 3, 1, 32, "Output B");
        this.controlRw = PbwConnector.createAttatched(
                worldPosition, false, true, 4, 1, CONTROL_RW_CHANNELS, "Control RW");
        this.controlSelect = PbwConnector.createAttatched(
                worldPosition, false, true, 4, 3, CONTROL_SEL_CHANNELS, "Control Select");

        // Default state.
        this.words = new long[WORD_CAPACITY];

        // Properties.
        this.prop_words = new ArrayList<>(words.length);
        for (int i = 0; i < words.length; i++) {
            final AtomicReference<String> prop = new AtomicReference<>();
            this.prop_words.add(prop);
            this.properties.put(String.format("zWord %02d", i), prop);
        }
        this.properties.putAll(this.input.getProperties());
        this.properties.putAll(this.outputA.getProperties());
        this.properties.putAll(this.outputB.getProperties());
        this.properties.putAll(this.controlRw.getProperties());
        this.properties.putAll(this.controlSelect.getProperties());
    }

    @Override
    public void addHooks(HookFile hookFile) {
        super.addHooks(hookFile);
        this.input.addHooks(hookFile);
        this.outputA.addHooks(hookFile);
        this.outputB.addHooks(hookFile);
        this.controlRw.addHooks(hookFile);
        this.controlSelect.addHooks(hookFile);
    }

    public void connect(
            ParallelBusWire.Terminus t_input,
            ParallelBusWire.Terminus t_outputA,
            ParallelBusWire.Terminus t_outputB,
            ParallelBusWire.Terminus t_controlRw,
            ParallelBusWire.Terminus t_controlSel
    ) {
        this.input.connect(t_input);
        this.outputA.connect(t_outputA);
        if (t_outputB != null) {
            this.outputB.connect(t_outputB);
        }
        this.controlRw.connect(t_controlRw);
        this.controlSelect.connect(t_controlSel);
    }

    @Override
    public void observe() {
        this.observed_input = input.getWord();
        this.observed_outputA = outputA.getWord();
        this.observed_outputB = outputB.getWord();
        this.observed_controlRw = controlRw.getWord();
        this.observed_controlRwAction
                = this.observed_controlRw < ControlWord.values().length
                ? ControlWord.values()[(int) this.observed_controlRw] : null;
        this.observed_controlSel = controlSelect.getWord();
    }

    @Override
    public void update(long deltaNanoseconds) {
        final boolean validSelection = observed_controlSel < words.length;
        final int castSel = (int) observed_controlSel;

        this.outputA.removeCharge();
        this.outputB.removeCharge();
        if (observed_controlRwAction != null
                || observed_controlRwAction != ControlWord.NOTHING
                && validSelection) {
            if (observed_controlRwAction == ControlWord.READ_INPUT) {
                this.words[castSel] = observed_input;
            } else if (observed_controlRwAction == ControlWord.OUTPUT_A) {
                this.outputA.charge(this.words[castSel]);
            } else if (observed_controlRwAction == ControlWord.OUTPUT_B) {
                this.outputB.charge(this.words[castSel]);
            }
        }
    }

    @Override
    public void updateProperties() {
        this.input.updateProperties();
        this.outputA.updateProperties();
        this.outputB.updateProperties();
        this.controlRw.updateProperties();
        this.controlSelect.updateProperties();
        for (int i = 0; i < words.length; i++) {
            this.prop_words.get(i).set(String.valueOf(words[i]));
        }
    }

    public void wipe() {
        for (int i = 0; i < words.length; i++) {
            words[i] = 0;
        }
    }
}
