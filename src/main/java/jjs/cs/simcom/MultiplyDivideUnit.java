/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simcom;

import java.util.concurrent.atomic.AtomicReference;
import jjs.cs.simulation.HookFile;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class MultiplyDivideUnit extends BasicInspectableEntity {

    public static final int CONTROL_SIGNAL_CHANNELS = 10;

    private final PbwConnector inputA;
    private final PbwConnector inputB;
    private final PbwConnector outputHi;
    private final PbwConnector outputLo;
    private final PbwConnector control;

    private long obs_inputA;
    private long obs_inputB;
    private long obs_control;
    private ControlWord obs_controlAction;
    private final AtomicReference<String> property_controlSignal;
    private final AtomicReference<String> property_controlAction;

    public MultiplyDivideUnit(
            float posX,
            float posY) {
        super(
                posX,
                posY,
                SimCom.COMPONENT_POS_Z,
                4f * SimCom.GRID_STEP,
                4f * SimCom.GRID_STEP,
                SimCom.COMPONENT_DEPTH, "simcom_mdu.png");

        this.inputA = PbwConnector.createAttatched(
                worldPosition, true, false, 4, 1, 32, "Input A");
        this.inputB = PbwConnector.createAttatched(
                worldPosition, true, false, 4, 3, 32, "Input B");
        this.outputHi = PbwConnector.createAttatched(
                worldPosition, true, true, 4, 3, 32, "Output Hi");
        this.outputLo = PbwConnector.createAttatched(
                worldPosition, true, true, 4, 1, 32, "Output Lo");
        this.control = PbwConnector.createAttatched(
                worldPosition, false, true, 2, 1, CONTROL_SIGNAL_CHANNELS, "Control");

        this.property_controlSignal = new AtomicReference<>();
        this.property_controlAction = new AtomicReference<>();
        this.properties.put("1. Control signal", this.property_controlSignal);
        this.properties.put("2. Control action", this.property_controlAction);
        this.properties.putAll(this.inputA.getProperties());
        this.properties.putAll(this.inputB.getProperties());
        this.properties.putAll(this.outputHi.getProperties());
        this.properties.putAll(this.outputLo.getProperties());

        this.obs_control = 0l;
        this.obs_controlAction = ControlWord.NOTHING;
    }

    public void connect(
            ParallelBusWire.Terminus inA,
            ParallelBusWire.Terminus inB,
            ParallelBusWire.Terminus outHi,
            ParallelBusWire.Terminus outLo,
            ParallelBusWire.Terminus cont) {
        this.inputA.connect(inA);
        this.inputB.connect(inB);
        this.outputHi.connect(outHi);
        this.outputLo.connect(outLo);
        this.control.connect(cont);
    }

    @Override
    public void addHooks(HookFile hookFile) {
        super.addHooks(hookFile);
        this.inputA.addHooks(hookFile);
        this.inputB.addHooks(hookFile);
        this.outputHi.addHooks(hookFile);
        this.outputLo.addHooks(hookFile);
        this.control.addHooks(hookFile);
    }

    @Override
    public void observe() {
        obs_inputA = inputA.getWord();
        obs_inputB = inputB.getWord();
        obs_control = control.getWord();
        obs_controlAction = obs_control < ControlWord.values().length
                ? ControlWord.values()[(int) obs_control] : null;
    }

    @Override
    public void update(long deltaNanoseconds) {
        if (obs_controlAction != null
                && obs_controlAction != ControlWord.NOTHING) {
            int[] results = obs_controlAction.performOperation(
                    (int) obs_inputA, (int) obs_inputB);

            outputHi.charge(results[0]);
            outputLo.charge(results[1]);
        } else {
            this.outputHi.removeCharge();
            this.outputLo.removeCharge();
        }
    }

    @Override
    public void updateProperties() {
        this.inputA.updateProperties();
        this.inputB.updateProperties();
        this.outputHi.updateProperties();
        this.outputLo.updateProperties();
        this.property_controlSignal.set(String.valueOf(obs_control));
        this.property_controlAction.set(obs_controlAction == null ? "[UNKNOWN]"
                : String.valueOf(obs_controlAction));
    }

    public enum ControlWord {

        NOTHING {
                    @Override
                    protected int[] performOperation(int operandA, int operandB) {
                        return null;
                    }
                },
        MULTIPLY {

                    @Override
                    protected int[] performOperation(int operandA, int operandB) {
                        operandA <<= 32;
                        operandB <<= 32;
                        operandA >>>= 32;
                        operandB >>>= 32;
                        long result = operandA * operandB;
                        return new int[]{((int) (result >> 32)), ((int) result)};
                    }
                },
        DIVIDE {

                    @Override
                    protected int[] performOperation(int operandA, int operandB) {
                        operandA <<= 32;
                        operandB <<= 32;
                        operandA >>>= 32;
                        operandB >>>= 32;
                        // todo fix
                        long remainder = operandB == 0 ? 0 : operandA % operandB;
                        long quotient = operandB == 0 ? 0 : operandA / operandB;
                        return new int[]{(int) remainder, (int) quotient};
                    }
                };

        protected abstract int[] performOperation(int operandA, int operandB);

        public long word() {
            return ordinal();
        }
    }

}
