/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simulation.data;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class ModelTest {

    @Test
    public void testModel() {

        // Null parameters test.
        {
            boolean nullPointerExceptionThrown = false;
            try {
                final RenderModel instance = new RenderModel(null, null, null, true);
            } catch (NullPointerException ex) {
                nullPointerExceptionThrown = true;
            }
            if (!nullPointerExceptionThrown) {
                Assert.fail("Instance constructed with null parameters.");
            }
        }
    }

}
