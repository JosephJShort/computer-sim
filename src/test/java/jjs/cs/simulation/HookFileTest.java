/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.simulation;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class HookFileTest {

    private static HookFile instance;

    public HookFileTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new HookFile();
    }

    @AfterClass
    public static void tearDownClass() {
        instance = null;
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddRemoveHook() {
        {
            boolean caught = false;
            try {
                instance.addHook(null, null);
            } catch (NullPointerException ex) {
                caught = true;
            }
            if (!caught) {
                fail("Did not throw a NullPointerException when given a null"
                        + " parameter.");
            }
        }

        // Create hooks for testing.
        final int hookCount = 200;
        final ArrayList<Hook> hooks = new ArrayList<>();
        for (int i = 0; i < hookCount; i++) {
            hooks.add(new Hook(instance.newId()));
        }

        // Test sequential add/remove.
        for (int trials = 0; trials < 3; trials++) {
            for (Hook hook : hooks) {
                assertEquals(true, instance.addHook(hook, Hook.class));
                assertEquals(false, instance.addHook(hook, Hook.class));
                assertEquals(true, instance.removeHook(hook, Hook.class));
                assertEquals(false, instance.removeHook(hook, Hook.class));
            }
        }

        // Test batch add/remove.
        for (int trials = 0; trials < 3; trials++) {
            for (Hook hook : hooks) {
                assertEquals(true, instance.addHook(hook, Hook.class));
                assertEquals(false, instance.addHook(hook, Hook.class));
            }
            for (Hook hook : hooks) {
                assertEquals(true, instance.removeHook(hook, Hook.class));
                assertEquals(false, instance.removeHook(hook, Hook.class));
            }
        }
    }

    @Test
    public void testListeners() {
        final int hookCount = 20;
        final int listenerCount = 100;

        // Create hooks for testing.
        final ArrayList<Hook> hooks = new ArrayList<>();
        for (int i = 0; i < hookCount; i++) {
            hooks.add(new Hook(instance.newId()));
        }

        // Create listeners for testing.
        final ArrayList<TestListener> listeners = new ArrayList<>();
        for (int i = 0; i < listenerCount; i++) {
            listeners.add(new TestListener());
        }

        // Test adding and removing listeners to file.
        for (int trial = 0; trial < 3; trial++) {
            for (TestListener listener : listeners) {
                assertEquals(true, instance
                        .addHookFileListener(listener, Hook.class));
                assertEquals(false, instance
                        .addHookFileListener(listener, Hook.class));
                assertEquals(true, instance
                        .removeHookFileListener(listener, Hook.class));
                assertEquals(false, instance
                        .removeHookFileListener(listener, Hook.class));
            }
        }

        // Test listener triggering.
        for (TestListener testListener : listeners) {
            instance.addHookFileListener(testListener, Hook.class);
        }
        for (int trials = 0; trials < 3; trials++) {

            // Test hookAdded.
            for (Hook hook : hooks) {
                for (TestListener testListener : listeners) {
                    testListener.flag = false;
                }
                instance.addHook(hook, Hook.class);
                for (TestListener listener : listeners) {
                    assertEquals("hookAdded(Hook hook) not triggered on"
                            + " listener. hook: " + hook
                            + ", listener: " + listener + ".",
                            true, listener.flag);
                }
            }

            // Test hookRemoved.
            for (Hook hook : hooks) {
                for (TestListener testListener : listeners) {
                    testListener.flag = false;
                }
                instance.removeHook(hook, Hook.class);
                for (TestListener listener : listeners) {
                    assertEquals("hookAdded(Hook hook) not triggered on"
                            + " listener. hook: " + hook
                            + ", listener: " + listener + ".",
                            true, listener.flag);
                }
            }
        }

        // Remove listeners and test that they are not triggered.
        for (TestListener testListener : listeners) {
            instance.removeHookFileListener(testListener, Hook.class);
        }
        for (Hook hook : hooks) {
            for (TestListener testListener : listeners) {
                testListener.flag = false;
            }
            instance.addHook(hook, Hook.class);
            instance.removeHook(hook, Hook.class);
            for (TestListener listener : listeners) {
                assertEquals("hookAdded(Hook hook) or hookRemoved(Hook hook)"
                        + " triggered after listerner was removed. Listener: "
                        + listener + ".",
                        false, listener.flag);
            }
        }
    }

    private static class TestListener implements HookFileListener<Hook> {

        private boolean flag = false;

        @Override
        public void hookAdded(Hook hook) {
            flag = true;
        }

        @Override
        public void hookRemoved(Hook hook) {
            flag = true;
        }
    }

}
