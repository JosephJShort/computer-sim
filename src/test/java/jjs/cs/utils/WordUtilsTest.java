/*
 * Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jjs.cs.utils;

import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Joseph J Short <Joseph.J.Short@gmail.com>
 */
public class WordUtilsTest {

    public WordUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of bytes method, of class WordUtils.
     */
    @Test
    public void testBytes() {

        final Random random = new Random();
        for (int trial = 0; trial < 100; trial++) {

            // Roll a word.
            final long testWord = random.nextLong();

            // Convert to bytes.
            final byte[] bytes = WordUtils.toBytes(testWord);
            assertEquals(8, bytes.length);

            // Construct long from bytes.
            long bytesSum = 0;
            for (int i = 0; i < bytes.length; i++) {
                bytesSum <<= 8;
                bytesSum += bytes[i] & 0xff;
            }
            assertEquals(bytesSum, testWord);

            // Convert bytes to long.
            final long retrievedWord = WordUtils.fromBytes(bytes);
            assertEquals(retrievedWord, testWord);
        }

    }

    @Test
    public void testFields() {
    }
}
