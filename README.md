Computer Sim
============

Welcome. This was a little project I worked on at university to simulate a RISC
computer in Java. I made a 3D simulation of a board with an HCU, ALU, MDU,
memory, registers, buses and multiplexers. The simulated computer can run
programs written in assembler.

Screen capture
--------------

![cs.png](https://bitbucket.org/repo/dAn8Lx/images/3817820004-cs.png)

Notice
------

The "Computer Sim" project is released under the Apache License, Version 2.0.

> Copyright 2014 Joseph J Short <Joseph.J.Short@gmail.com>
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
>     http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.
